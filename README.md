# Epomis Mobile

 1. [Wprowadzanie zmian w repozytorium](#1-wprowadzanie-zmian-w-repozytorium)
 2. [Praca z kodem](#2-praca-z-kodem)
 3. [UML Diagrams](other/docs/uml/README.md)

# 1. Wstępne wymagania
### Git
* Podstawy gita: https://guides.github.com/introduction/git-handbook/
* Workflow (jak wykorzystać praktycznie gita?): https://guides.github.com/introduction/flow/index.html
* Nazywanie commitów: https://chris.beams.io/posts/git-commit/

Gdy skończysz implementować zadanie, przypisz MRa do mentora projektu. Gdy będzie trzeba wprowadzić jakieś poprawki, odbije on MRa z komentarzami, a proces się powtórzy. Jeżeli wszystko będzie w porządku, MR zostanie przypisany do @blank, który zmerguje branch lub doda jeszcze jakieś komentarze.

### Narzędzia
* Git: https://git-scm.com/
* Android Studio: https://developer.android.com/studio/install
* Android SDK 29 (pobierane podczas pierwszego uruchomienia Android Studio)

# 2. Praca z kodem
##### Git + Android Studio: import projektu, commitowanie/pushowanie zmian
https://www.youtube.com/watch?v=k4xfd1iVKMo
##### Poruszanie się po android studio
https://developer.android.com/studio/intro
##### Uruchamianie aplikacji
https://developer.android.com/training/basics/firstapp/running-app
##### Częste problemy z Android Studio
https://developer.android.com/studio/known-issues
##### Coding conventions
https://kotlinlang.org/docs/reference/coding-conventions.html