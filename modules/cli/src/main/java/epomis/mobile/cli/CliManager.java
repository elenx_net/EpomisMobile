package epomis.mobile.cli;

import epomis.mobile.manager.ApplicantManager;
import epomis.mobile.manager.ProviderManager;
import kotlin.NotImplementedError;

class CliManager
{
    private final ProviderManager providerManager;
    private final ApplicantManager applicantManager;

    CliManager(ProviderManager providerManager, ApplicantManager applicantManager)
    {
        this.providerManager = providerManager;
        this.applicantManager = applicantManager;
    }

    void run()
    {
        throw new NotImplementedError();
    }
}
