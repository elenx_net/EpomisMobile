package epomis.mobile.cli

import epomis.mobile.applicantModule
import epomis.mobile.manager.ApplicantManager
import epomis.mobile.manager.ProviderManager
import epomis.mobile.providerModule
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

val kodein = Kodein {
    importOnce(providerModule, allowOverride = true)
    importOnce(applicantModule, allowOverride = true)
}

fun main()
{
    val providerManager by kodein.instance<ProviderManager>()
    val applicantManager by kodein.instance<ApplicantManager>()

    CliManager(providerManager, applicantManager)
        .run()
}