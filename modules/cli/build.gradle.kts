plugins {
    kotlin
    kodein
}

dependencies {

    implementation(project(":modules:model"))
    implementation(project(":modules:utils"))
    implementation(project(":modules:provider"))
    implementation(project(":modules:applicant"))

    implementation("io.vavr:vavr:0.9.3")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}
