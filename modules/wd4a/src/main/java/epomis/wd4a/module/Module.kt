package epomis.wd4a.module

open class Module(
    val name: String,
    val jsFile: String,
    val dependsOn: List<Module> = listOf())