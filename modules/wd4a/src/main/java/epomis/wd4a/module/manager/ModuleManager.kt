package epomis.wd4a.module.manager

import epomis.wd4a.module.CallableModule

interface ModuleManager
{
    fun <I> register(module: CallableModule<I>): I
}