package epomis.wd4a.module.manager

import epomis.wd4a.context.JsExecutionContextProvider
import epomis.wd4a.module.CallableModule
import epomis.wd4a.module.Module
import kotlin.reflect.full.createInstance

class ModuleManagerImpl(
    private val jsExecutionContextProviderFactory: (Module) -> JsExecutionContextProvider
) : ModuleManager {
    override fun <I> register(module: CallableModule<I>) =
        module.interfaceFactory.createInstance().create(jsExecutionContextProviderFactory(module))
}