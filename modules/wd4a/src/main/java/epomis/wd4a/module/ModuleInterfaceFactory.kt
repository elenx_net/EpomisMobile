package epomis.wd4a.module

import epomis.wd4a.context.JsExecutionContextProvider

interface ModuleInterfaceFactory<I>
{
    fun create(provider: JsExecutionContextProvider): I
}