package epomis.wd4a.module

import kotlin.reflect.KClass

class CallableModule<I>(
    name: String,
    jsFile: String,
    dependsOn: List<Module> = listOf(),
    val interfaceFactory: KClass<ModuleInterfaceFactory<I>>
) : Module(name, jsFile, dependsOn)