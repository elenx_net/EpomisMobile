package epomis.wd4a.utils

import android.webkit.WebView
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

suspend fun WebView.evaluateJavascript(code: String) = suspendCancellableCoroutine<String> { evaluateJavascript(code, it) }

private fun WebView.evaluateJavascript(code: String, continuation: CancellableContinuation<String>) =
    evaluateJavascript(code) { continuation.resume(it) }

fun WebView.safeReload() = loadUrl(url)
