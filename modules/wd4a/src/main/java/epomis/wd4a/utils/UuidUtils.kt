package epomis.wd4a.utils

import java.util.*

fun generateId() = UUID.randomUUID().toString()