package epomis.wd4a.utils

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel

suspend fun <R> receiveFrom(block: suspend (receiver: SendChannel<R>) -> Unit): R {
    val channel = Channel<R>(Channel.RENDEZVOUS)
    block.invoke(channel)
    return channel.receive()
}