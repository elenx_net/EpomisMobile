package epomis.wd4a

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import epomis.wd4a.communicator.JsCommandsExecutor
import epomis.wd4a.communicator.JsCommunicator
import epomis.wd4a.communicator.JsCommunicatorInjector
import epomis.wd4a.communicator.JsMethodInvocationCodeBuilder
import epomis.wd4a.context.JsExecutionContextProvider
import epomis.wd4a.module.Module
import epomis.wd4a.module.manager.ModuleManager
import epomis.wd4a.module.manager.ModuleManagerImpl
import epomis.wd4a.service.webview.WebViewService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

fun setupWd4aModule(context: Context, webViewService: WebViewService) = Kodein.Module("wd4a")
{
    bind<Context>() with instance(context)
    bind<WebViewService>() with instance(webViewService)
    bind<JsCommunicatorInjector>() with singleton { JsCommunicatorInjector(instance(), instance()) }
    bind<ModuleManager>() with singleton { ModuleManagerImpl(factory()) }
    bind<ObjectMapper>() with singleton { ObjectMapper() }
    bind<JsMethodInvocationCodeBuilder>() with singleton { JsMethodInvocationCodeBuilder() }
    bind<JsCommandsExecutor>() with singleton { JsCommandsExecutor(instance(), instance(), instance()) }
    bind<JsCommunicator>() with singleton { JsCommunicator(instance(), instance(), instance()) }
    bind<JsExecutionContextProvider>() with factory { module: Module -> JsExecutionContextProvider(module, instance()) }
}