package epomis.wd4a.context

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import epomis.wd4a.communicator.CreateCallback
import epomis.wd4a.communicator.CreateFunction
import epomis.wd4a.communicator.InvokeFunction
import epomis.wd4a.communicator.JsCommunicator
import epomis.wd4a.scope.JsScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.map
import java.util.*

class JsExecutionContext(
    private val communicator: JsCommunicator,
    private val scope: JsScope,
    private val objectMapper: ObjectMapper)
{
    suspend fun invoke(name: String, vararg args: Any?) =
        invokeWithSuppliedReceiver(name, args, null)

    suspend fun <R> invokeWithResultTyped(name: String, returnType: Class<R>, vararg args: Any?): R
    {
        val receiver = Channel<JsonNode?>()
        invokeWithSuppliedReceiver(name, args, receiver)
        val returnJson = receiver.receive()
        return objectMapper.treeToValue(returnJson, returnType)
    }

    suspend inline fun <reified R> invokeWithResult(name: String, vararg args: Any?) =
        invokeWithResultTyped(name, R::class.java, *args)

    suspend fun <R> invokeWithCallbackTyped(name: String, returnType: Class<R>, vararg args: Any): R
    {
        val callbackSymbol = name + UUID.randomUUID().toString()
        val callbackChannel = createCallbackTyped(callbackSymbol, returnType)
        invoke(name, callbackSymbol, *args)
        return callbackChannel.receive()
    }

    suspend fun createFunction(name: String, js: String) =
        communicator.commands.send(CreateFunction(scope, name, js))

    suspend fun <P> createCallbackTyped(name: String, argType: Class<P>): ReceiveChannel<P>
    {
        val callbackReceiver = Channel<List<JsonNode?>>(Channel.UNLIMITED)
        communicator.commands.send(CreateCallback(scope, name, callbackReceiver))
        return callbackReceiver.map { objectMapper.treeToValue(it.first(), argType) }
    }

    suspend inline fun <reified P> createCallback(name: String) =
        createCallbackTyped(name, P::class.java)

    suspend fun invokeWithSuppliedReceiver(name: String, args: Array<out Any?>, receiver: SendChannel<JsonNode?>?)
    {
        val argsJson = args.map { objectMapper.convertValue(it, JsonNode::class.java) }
        communicator.commands.send(InvokeFunction(scope, name, argsJson, receiver))
    }
}