package epomis.wd4a.context

import epomis.wd4a.module.Module
import epomis.wd4a.scope.JsScopeManager

class JsExecutionContextProvider(
    private val module: Module,
    private val jsScopeManager: JsScopeManager
) {
    suspend fun <R> run(block: JsExecutionContext.() -> R): R = TODO()
    suspend fun <R> runRetried(maxRetryCount: Int, block: JsExecutionContext.() -> R): R = TODO()
}