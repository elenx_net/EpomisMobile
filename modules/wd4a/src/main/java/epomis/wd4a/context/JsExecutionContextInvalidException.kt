package epomis.wd4a.context

class JsExecutionContextInvalidException(message: String) : RuntimeException(message)