package epomis.wd4a.service.webview.window

import android.graphics.PixelFormat.TRANSLUCENT
import android.os.Build
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.WindowManager
import android.view.WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
import android.view.WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE

private val LAYOUT_TYPE_FLAG =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
    {
        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
    }
    else
    {
        WindowManager.LayoutParams.TYPE_PHONE
    }

val INVISIBLE_LAYOUT_PARAMS = WindowManager.LayoutParams(
    WRAP_CONTENT,
    WRAP_CONTENT,
    LAYOUT_TYPE_FLAG,
    FLAG_NOT_FOCUSABLE or FLAG_NOT_TOUCHABLE,
    TRANSLUCENT
).apply {
    gravity = Gravity.TOP
    x = 0
    y = 0
    width = 0
    height = 0
}
