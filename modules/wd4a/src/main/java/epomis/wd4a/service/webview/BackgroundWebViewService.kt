package epomis.wd4a.service.webview

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.webkit.WebView
import epomis.wd4a.service.bindServiceInterface
import epomis.wd4a.service.webview.window.HeadlessWindow
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import mu.KotlinLogging

private val logger = KotlinLogging.logger("BackgroundWebViewService")
private const val EXCEPTION_MESSAGE = "Exception in service context: "

class BackgroundWebViewService : WebViewService, Service()
{
    private val supervisorJob = SupervisorJob()
    private val coroutineExceptionHandler = CoroutineExceptionHandler { context, throwable ->
        logger.error("$EXCEPTION_MESSAGE $context", throwable)
    }
    override val coroutineContext = supervisorJob + coroutineExceptionHandler + Dispatchers.Main
    private val interfaceBinder by lazy { WebViewServiceBinder(this) }
    private lateinit var internalWebView: WebView
    private lateinit var headlessWindow: HeadlessWindow

    override val webView: WebView
        get() = internalWebView

    override fun onBind(intent: Intent?) = interfaceBinder

    override fun onCreate()
    {
        internalWebView = WebView(this)
        headlessWindow = HeadlessWindow(this)

        headlessWindow.assignView(webView)
    }

    override fun onDestroy()
    {
        headlessWindow.removeView(webView)
        supervisorJob.cancel()
    }

    companion object
    {
        fun start(context: Context) = context.startService(Intent(context, BackgroundWebViewService::class.java))
        fun bind(context: Context,
                 onUnbind: () -> Unit = {},
                 onBind: (WebViewService, ServiceConnection) -> Unit
        ) = bindServiceInterface<BackgroundWebViewService, WebViewService>(context, onUnbind, onBind)
    }
}
