package epomis.wd4a.service.webview.window

import android.content.Context
import android.view.View
import android.view.WindowManager

class HeadlessWindow(context: Context)
{
    private val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

    fun assignView(view: View) = windowManager.addView(view, INVISIBLE_LAYOUT_PARAMS)

    fun removeView(view: View) = windowManager.removeView(view)
}
