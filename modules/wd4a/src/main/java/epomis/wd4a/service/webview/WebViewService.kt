package epomis.wd4a.service.webview

import android.webkit.WebView
import kotlinx.coroutines.CoroutineScope

interface WebViewService : CoroutineScope
{
    val webView: WebView
}