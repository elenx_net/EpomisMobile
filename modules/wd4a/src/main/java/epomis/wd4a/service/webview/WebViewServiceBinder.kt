package epomis.wd4a.service.webview

import android.os.Binder

class WebViewServiceBinder(private val backgroundWebViewService: BackgroundWebViewService)
    : Binder(), WebViewService by backgroundWebViewService
