package epomis.wd4a.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder

inline fun <reified SERVICE, INTERFACE> bindServiceInterface(
    context: Context,
    noinline onUnbind: () -> Unit = {},
    noinline onBind: (INTERFACE, ServiceConnection) -> Unit
) = context.bindService(
    Intent(context, SERVICE::class.java),
    createServiceConnection(onUnbind, onBind),
    Context.BIND_ABOVE_CLIENT
)

fun <INTERFACE> createServiceConnection(
    onUnbind: () -> Unit,
    onBind: (INTERFACE, ServiceConnection) -> Unit
): ServiceConnection = ServiceConnectionProxy(onUnbind, onBind)

private class ServiceConnectionProxy<INTERFACE>(
    private val onUnbind: () -> Unit,
    private val onBind: (INTERFACE, ServiceConnection) -> Unit) : ServiceConnection
{
    @Suppress("UNCHECKED_CAST")
    override fun onServiceConnected(name: ComponentName, binder: IBinder
    ) = onBind(binder as INTERFACE, this)

    override fun onServiceDisconnected(name: ComponentName?) = onUnbind()
}