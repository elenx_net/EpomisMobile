package epomis.wd4a.communicator

import com.fasterxml.jackson.databind.JsonNode
import epomis.wd4a.scope.JsScope
import epomis.wd4a.utils.receiveFrom
import kotlinx.coroutines.channels.SendChannel

suspend fun JsCommunicator.createScope(
    code: String,
    importScopes: Map<String, JsScope> = emptyMap()
) = receiveFrom<JsScope> { commands.send(CreateScope(code, importScopes, it)) }

suspend fun JsCommunicator.invokeFunctionWithResult(
    scope: JsScope,
    name: String,
    argumentsJson: List<JsonNode> = emptyList()
) = receiveFrom<JsonNode?> { commands.send(InvokeFunction(scope, name, argumentsJson, it)) }

suspend fun JsCommunicator.createFunction(
    scope: JsScope,
    name: String,
    code: String
) = commands.send(CreateFunction(scope, name, code))

suspend fun JsCommunicator.createCallback(
    scope: JsScope,
    name: String,
    callReceiver: SendChannel<List<JsonNode>>? = null
) = commands.send(CreateCallback(scope, name, callReceiver))
