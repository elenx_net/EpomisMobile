package epomis.wd4a.communicator

class JsMethodInvocationCodeBuilder
{
    fun build(objectName: String, methodName: String, vararg params: Any): String
    {
        val argumentsSegment = adaptArguments(*params)
        return "$objectName.$methodName($argumentsSegment)"
    }

    private fun adaptArguments(vararg arguments: Any) = arguments
        .map { adaptArgument(it) }
        .joinToString(",")

    private fun adaptArgument(argument: Any) =
        if (argument is String)
            "'$argument'"
        else
            argument
}