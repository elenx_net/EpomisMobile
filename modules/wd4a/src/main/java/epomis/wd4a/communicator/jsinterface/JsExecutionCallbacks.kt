package epomis.wd4a.communicator.jsinterface

import android.webkit.JavascriptInterface
import mu.KotlinLogging

private val logger = KotlinLogging.logger("JsExecutionCallbacks")

class JsExecutionCallbacks
{
    @JavascriptInterface
    fun onCallback(callbackId: String, argsJson: String)
    {
        logger.info("onCallback $callbackId: $argsJson")
    }
}