package epomis.wd4a.communicator

sealed class JsEvent

object JsPageReloadedEvent : JsEvent()