package epomis.wd4a.communicator

import com.fasterxml.jackson.databind.ObjectMapper
import epomis.wd4a.service.webview.WebViewService
import epomis.wd4a.utils.evaluateJavascript
import epomis.wd4a.utils.generateId

private const val JS_COMMUNICATOR_OBJECT = "jsCommunicator"

class JsCommandsExecutor(
    private val webViewService: WebViewService,
    private val jsMethodInvocationCodeBuilder: JsMethodInvocationCodeBuilder,
    private val objectMapper: ObjectMapper
)
{
    suspend fun createScope(command: CreateScope): String
    {
        val importScopes = command.importScopes.map { (key, value) -> mapOf("name" to key, "id" to value.id) }
        val importScopesJson = objectMapper.writeValueAsString(importScopes)
        return invokeJsMethod("createScope", command.code, importScopesJson)
    }

    suspend fun invokeFunction(command: InvokeFunction): String
    {
        val argumentsJson = objectMapper.writeValueAsString(command.argumentsJson)
        return invokeJsMethod("invokeFunctionFromScope", command.scope.id, command.name, argumentsJson)
    }

    suspend fun createFunction(command: CreateFunction) =
        invokeJsMethod("createFunctionInScope", command.scope.id, command.name, command.code)

    suspend fun createCallback(command: CreateCallback) =
        invokeJsMethod("createCallback", generateId(), command.scope.id, command.name)

    private suspend fun invokeJsMethod(methodName: String, vararg params: Any): String
    {
        val methodInvocationCode = jsMethodInvocationCodeBuilder.build(JS_COMMUNICATOR_OBJECT, methodName, *params)
        return webViewService.webView.evaluateJavascript(methodInvocationCode)
    }
}