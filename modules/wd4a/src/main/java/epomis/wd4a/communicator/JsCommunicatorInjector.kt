package epomis.wd4a.communicator

import android.content.Context
import epomis.wd4a.service.webview.WebViewService
import epomis.wd4a.utils.evaluateJavascript
import kotlinx.coroutines.withContext

private const val PATH_TO_COMMUNICATOR_CODE = "js/communicator/communicator.js"

class JsCommunicatorInjector(
    context: Context,
    private val webViewService: WebViewService
)
{
    private val communicatorCode = context
        .assets
        .open(PATH_TO_COMMUNICATOR_CODE)
        .bufferedReader()
        .use { it.readText() }

    suspend fun inject() = withContext(webViewService.coroutineContext) {
        webViewService.webView.evaluateJavascript(communicatorCode)
    }
}