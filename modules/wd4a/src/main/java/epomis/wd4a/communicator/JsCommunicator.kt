package epomis.wd4a.communicator

import android.annotation.SuppressLint
import android.webkit.WebView
import android.webkit.WebViewClient

import epomis.wd4a.communicator.jsinterface.JsExecutionCallbacks
import epomis.wd4a.service.webview.WebViewService
import epomis.wd4a.utils.evaluateJavascript
import epomis.wd4a.utils.safeReload

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.launch

private const val GET_JSCOMMUNICATOR_JS_FUNCTION = "window.jsCommunicator"
private const val UNDEFINED = "undefined"
private const val NULL = "null"
private const val WD4A = "wd4a"

class JsCommunicator(
    private val webViewService: WebViewService,
    private val jsCommunicatorInjector: JsCommunicatorInjector,
    private val jsCommandsExecutor: JsCommandsExecutor
) : CoroutineScope by webViewService
{
    val commands = actor<JsCommand> { processCommands(channel) }
    private val events = BroadcastChannel<JsEvent>(capacity = 1)

    init
    {
        setupWebView()
    }

    private suspend fun processCommands(channel: Channel<JsCommand>)
    {
        for (command in channel)
        {
            injectIfNeeded()
            processCommand(command)
        }
    }

    private suspend fun processCommand(command: JsCommand) =
        when (command)
        {
            is CreateScope -> jsCommandsExecutor.createScope(command)
            is InvokeFunction -> jsCommandsExecutor.invokeFunction(command)
            is CreateFunction -> jsCommandsExecutor.createFunction(command)
            is CreateCallback -> jsCommandsExecutor.createCallback(command)
        }

    private suspend fun injectIfNeeded() =
        jsCommunicatorInjector.takeUnless { isInjected() }?.apply { inject() }

    private suspend fun isInjected(): Boolean
    {

        val jsCommunicator = webViewService.webView.evaluateJavascript(GET_JSCOMMUNICATOR_JS_FUNCTION)
        return jsCommunicator != NULL && jsCommunicator != UNDEFINED
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView()
    {
        webViewService.webView.settings.javaScriptEnabled = true
        webViewService.webView.safeReload()
        webViewService.webView.addJavascriptInterface(JsExecutionCallbacks(), WD4A)
        webViewService.webView.webViewClient = Wd4aWebViewClient()
    }

    private inner class Wd4aWebViewClient : WebViewClient()
    {
        override fun onPageFinished(view: WebView?, url: String?)
        {
            launch { onPageFinished() }
        }

        private suspend fun onPageFinished()
        {
            if (!isInjected())
            {
                jsCommunicatorInjector.inject()
                events.send(JsPageReloadedEvent)
            }
        }
    }

    fun subscribeToEvents() = events.openSubscription()
}
