package epomis.wd4a.communicator

import com.fasterxml.jackson.databind.JsonNode
import epomis.wd4a.scope.JsScope
import kotlinx.coroutines.channels.SendChannel

sealed class JsCommand

class CreateScope(
    val code: String,
    val importScopes: Map<String, JsScope> = emptyMap(),
    val scopeReceiver: SendChannel<JsScope>? = null
) : JsCommand()

class InvokeFunction(
    val scope: JsScope,
    val name: String,
    val argumentsJson: List<JsonNode> = emptyList(),
    val resultReceiver: SendChannel<JsonNode?>? = null
) : JsCommand()

class CreateFunction(
    val scope: JsScope,
    val name: String,
    val code: String
) : JsCommand()

class CreateCallback(
    val scope: JsScope,
    val name: String,
    val callReceiver: SendChannel<List<JsonNode>>? = null
) : JsCommand()