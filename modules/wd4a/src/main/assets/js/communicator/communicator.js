(function() {
    window.jsCommunicator = {
        scopes: [],
        createScope: function(scopeCode, importScopesJson) {
             return this.wrapExecution(function() {
                var scope = this.createNewScope(scopeCode);
                this.importScopesToScope(scope, importScopesJson);
                return this.scopes.length;
            }.bind(this));
        },
        createFunctionInScope: function(scopeId, functionName, functionCode) {
            return this.wrapExecution(function() {
                this.scopes[scopeId][functionName] = this.createJsCodeFromString(functionCode)
                return functionName;
            }.bind(this));
        },
        invokeFunctionFromScope: function(scopeId, functionName, argumentsJson) {
            return this.wrapExecution(function() {
                var scope = this.scopes[scopeId];
                return this.invokeFunction(scope, functionName, argumentsJson);
            }.bind(this));
        },
        createCallback: function(callbackId, scopeId, name) {
            return this.wrapExecution(function() {
                var scope = this.scopes[scopeId]
                this.createCallbackFunction(callbackId, scope, name);
                return callbackId
            }.bind(this));
        },
        wrapExecution: function(executor) {
            try {
                return JSON.stringify(executor());
            } catch (error) {
                return JSON.stringify(error, Object.getOwnPropertyNames(error));
            }
        },
        createNewScope: function(scopeCode) {
            this.scopes[this.scopes.length] = this.createJsCodeFromString(scopeCode);
            return this.scopes[this.scopes.length - 1]
        },
        createJsCodeFromString: function(jsCode) {
            return new Function("return " + jsCode)();
        },
        importScopesToScope: function(scope, importScopesJson) {
            if (importScopesJson !== undefined && !this.isEmptyJson(importScopesJson)) {
                var importScopes = JSON.parse(importScopesJson);
                importScopes.forEach(function(element) {
                    scope[element.name] = this.scopes[element.id];
                }.bind(this));
            }
        },
        invokeFunction: function(scope, functionName, argumentsJson) {
            if (argumentsJson === undefined || this.isEmptyJson(argumentsJson)) {
                return scope[functionName].call(scope);
            } else {
                var parsedArgumentsJson = JSON.parse(argumentsJson);
                return scope[functionName].apply(scope, parsedArgumentsJson);
            }
        },
        createCallbackFunction: function(callbackId, scope, name) {
            scope[name] = function() {
                var argumentsJson = JSON.stringify(arguments);
                window.wd4a.onCallback(callbackId, argumentsJson);
            }
        },
        isEmptyJson: function(json) {
            return json === "{}" || json === "[]";
        }
    };
})();