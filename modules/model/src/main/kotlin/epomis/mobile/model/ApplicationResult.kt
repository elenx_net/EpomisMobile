package epomis.mobile.model

import org.joda.time.DateTime

data class ApplicationResult(
    val success: Boolean,
    val sent: DateTime = DateTime.now()
)