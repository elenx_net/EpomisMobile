package epomis.mobile.model

data class AdFilter(
    val keyword: String,
    val location: String
)
