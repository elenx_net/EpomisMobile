package epomis.mobile.model

class UserData(
    val firstName: String,
    val lastName: String,
    val email: String,
    val resumeFilename: String,
    val resumeContent: ByteArray
)