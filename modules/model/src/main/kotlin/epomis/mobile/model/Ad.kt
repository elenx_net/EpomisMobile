package epomis.mobile.model

data class Ad(
    val href: String,
    val title: String,
    val company: String,
    val location: String,
    val provider: String,
    val latestApplication: ApplicationResult? = null,

    val id: Long? = null
)