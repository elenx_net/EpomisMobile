package epomis.mobile.model

import org.joda.time.DateTime

data class Job(
    val type: JobType,
    val status: JobStatus,
    val adFilter: AdFilter,
    val started: DateTime = DateTime.now(),
    val finished: DateTime = DateTime.now(),

    val id: Long? = null
)

enum class JobType(val simpleName: String)
{
    CRAWLING("Crawling"),
    APPLYING("Applying")
}

enum class JobStatus
{
    IN_PROGRESS,
    SCHEDULED,
    FAILED,
    SUCCESSFUL
}
