package epomis.service.connection6.http

import epomis.service.connection6.request.ConnectionRequest

import org.mockito.Mockito

import spock.lang.Specification

class HeadersBuilderTest extends Specification
{
    void "test building headers"()
    {
        given:

        ConnectionRequest request = Mockito.mock(ConnectionRequest)
        HeadersMerger merger = Mockito.mock(HeadersMerger)

        Mockito.when(request.headers).thenReturn(Collections.EMPTY_MAP)
        Mockito.when(request.cookies).thenReturn(["cookieKeyA": "cookieValueA", "cookieKeyB": "cookieValueB"])

        HeadersBuilder headersBuilder = new HeadersBuilderImpl(merger)

        when:

        def headers = headersBuilder.fromConnectionRequest(request)

        then:

        headers.get("Cookie")[0] == "cookieKeyA=cookieValueA; cookieKeyB=cookieValueB"
    }
}
