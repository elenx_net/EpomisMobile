package epomis.service.connection6.http

import com.google.api.client.http.LowLevelHttpResponse
import com.google.api.client.http.HttpTransport
import com.google.api.client.http.HttpRequest
import com.google.api.client.http.HttpResponse
import com.google.api.client.http.HttpHeaders

import org.mockito.Mockito

import epomis.service.connection6.CookieExtractorImpl

import spock.lang.Specification

class CookieExtractorTest extends Specification
{
    void "test extracting cookies from http response"()
    {
        given:

        LowLevelHttpResponse lowLevelHttpResponse = Mockito.mock(LowLevelHttpResponse)
        HttpTransport transport = Mockito.mock(HttpTransport)
        HttpHeaders httpHeaders = Mockito.mock(HttpHeaders)

        def httpRequest = new HttpRequest(transport, "GET")
        def httpResponse = new HttpResponse(httpRequest, lowLevelHttpResponse)
        def headers = ["SomeHeader": ["SomeValue"], "Set-Cookie": ["cookieKeyA=cookieValueA; cookieKeyB=cookieValueB"]].entrySet()

        Mockito.when(httpHeaders.entrySet()).thenReturn(headers)
        httpRequest.setResponseHeaders(httpHeaders)

        def cookieExtractor = new CookieExtractorImpl()

        when:

        def cookies = cookieExtractor.extractFromHttpResponse(httpResponse)

        then:

        cookies.get("cookieKeyA") == "cookieValueA"
        cookies.get("cookieKeyB") == "cookieValueB"
    }
}
