package epomis.service.connection6.http

import epomis.service.connection6.request.DataEntry

import org.mockito.Mockito

import spock.lang.Specification

class InputStreamEntryBuilderTest extends Specification
{
    InputStream inputStream = Mock()
    DataEntry dataEntry = Mockito.mock(DataEntry)

    void "test building byte array with file > 50MB throws RuntimeException"()
    {
        given:

            inputStream.available() >> 50000001
            inputStream.read(_) >> 25000000

            Mockito.when(dataEntry.getInputStream()).thenReturn(inputStream)

            def inputStreamEntryBuilder = new InputStreamEntryBuilder()

        when:

            inputStreamEntryBuilder.build(dataEntry, false)

        then:

            final FileTooBigException exception = thrown()
            exception.getMessage() == "File is too big (50 MB) to be sent as a ByteArray. Send it as InputStream"
    }

    void "test building byte array with file == 50MB"()
    {
        given:

            1 * inputStream.available() >> 50000000
            1 * inputStream.available() >> 25000000
            2 * inputStream.read(_) >> 25000000

            inputStream.available() >> 0
            inputStream.read(_) >> 0

            Mockito.when(dataEntry.getInputStream()).thenReturn(inputStream)

            def inputStreamEntryBuilder = new InputStreamEntryBuilder()

        when:

            inputStreamEntryBuilder.build(dataEntry, false)

        then:

            noExceptionThrown()
    }

    void "test building byte array with file < 50MB"()
    {
        given:

            1 * inputStream.available() >> 49999999
            1 * inputStream.available() >> 25000000
            2 * inputStream.read(_) >> 24999999

            inputStream.available() >> 0
            inputStream.read(_) >> 0

            Mockito.when(dataEntry.getInputStream()).thenReturn(inputStream)

            def inputStreamEntryBuilder = new InputStreamEntryBuilder()

        when:

            inputStreamEntryBuilder.build(dataEntry, false)

        then:

            noExceptionThrown()
    }
}
