package epomis.service.connection6.http

import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.DataEntry

import org.mockito.Mockito

import spock.lang.Specification

class GetParametersBuilderTest extends Specification
{
    void "test building url parameters"()
    {
        given:

        ConnectionRequest connectionRequest = Mockito.mock(ConnectionRequest)
        DataEntry dataEntry1 = Mockito.mock(DataEntry)
        DataEntry dataEntry2 = Mockito.mock(DataEntry)
        def data = [dataEntry1, dataEntry2]

        Mockito.when(dataEntry1.key).thenReturn("key1")
        Mockito.when(dataEntry2.key).thenReturn("key2")

        Mockito.when(dataEntry1.value).thenReturn("value1")
        Mockito.when(dataEntry2.value).thenReturn("value2")

        Mockito.when(connectionRequest.data).thenReturn(data)
        Mockito.when(connectionRequest.url).thenReturn("http://www.test.pl")

        GetParametersBuilder builder = new GetParametersBuilder()

        when:

        def resultUrl = builder.buildUrlWithGetParameters(connectionRequest)

        then:

        resultUrl.toString() == "http://www.test.pl?key1=value1&key2=value2"
    }
}
