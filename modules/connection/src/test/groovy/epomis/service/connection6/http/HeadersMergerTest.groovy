package epomis.service.connection6.http

import com.google.api.client.http.HttpHeaders

import spock.lang.Specification

class HeadersMergerTest extends Specification
{
    void "test merging headers"()
    {
        given:

        def userHeaders = ["user-header": "user-value", "Accept-Encoding": "compress"]
        def httpHeaders = new HttpHeaders()

        httpHeaders.setAcceptEncoding("gzip")

        HeadersMerger merger = new HeadersMerger()

        when:

        merger.mergeHeaders(userHeaders, httpHeaders)

        def encoding = httpHeaders.get("accept-encoding")
        def userHeader = httpHeaders.get("user-header")

        then:

        encoding[0] == "gzip"
        encoding[1] == "compress"
        userHeader[0] == "user-value"
    }
}
