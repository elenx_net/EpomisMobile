package epomis.service.connection6.request

import org.apache.commons.lang3.StringUtils

data class ConnectionRequest(
    val url: String = StringUtils.EMPTY,
    val jsonObject: Any? = null,
    val referrer: String = StringUtils.EMPTY,
    val data: List<DataEntry> = emptyList(),
    val headers: Map<String, String> = emptyMap(),
    val cookies: Map<String, String> = emptyMap(),
    val requestArgs: Map<String, String> = emptyMap(),
    val shouldRedirect: Boolean = true,
    val isChunked: Boolean = false,
    val contentType: RequestContentType = RequestContentType.MULTIPART,
    val transportProvider: TransportProvider = TransportProvider.NET
)
