package epomis.service.connection6.http

import com.google.api.client.http.GenericUrl

import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.DataEntry

import org.apache.http.client.utils.URIBuilder

internal class GetParametersBuilder
{
    fun buildUrlWithGetParameters(connectionRequest: ConnectionRequest): GenericUrl
    {
        val uriBuilder = URIBuilder(connectionRequest.url)

        connectionRequest
            .data
            .forEach { appendToUrl(it, uriBuilder) }

        return GenericUrl(uriBuilder.build().toString())
    }

    private fun appendToUrl(dataEntry: DataEntry, uriBuilder: URIBuilder) = uriBuilder.addParameter(dataEntry.key, dataEntry.value)
}
