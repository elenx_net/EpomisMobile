package epomis.service.connection6.http

import com.google.api.client.http.HttpHeaders
import epomis.service.connection6.request.ConnectionRequest

interface HeadersBuilder
{
    fun fromConnectionRequest(request: ConnectionRequest): HttpHeaders
}
