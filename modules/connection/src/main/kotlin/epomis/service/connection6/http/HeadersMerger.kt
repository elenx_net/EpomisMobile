package epomis.service.connection6.http

import com.google.api.client.http.HttpHeaders

import org.apache.commons.collections4.ListUtils

import java.util.Collections

class HeadersMerger
{
    fun mergeHeaders(userHeaders: Map<String, String>, resultHeaders: HttpHeaders)
        = userHeaders
            .entries
            .forEach{ mergeHeader(it, resultHeaders) }


    private fun mergeHeader(userHeader: Map.Entry<String, String>, resultHeaders: HttpHeaders)
    {
        val headerName = userHeader.key
        val currentValues = resultHeaders[headerName]
        val userValues = Collections.singletonList(userHeader.value)

        if(currentValues == null)
        {
            resultHeaders.set(headerName, userValues)
        }
        else
        {
            resultHeaders.set(headerName, ListUtils.union(currentValues as ArrayList<*>, userValues))
        }
    }
}
