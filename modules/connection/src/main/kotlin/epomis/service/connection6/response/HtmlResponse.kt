package epomis.service.connection6.response

import com.google.api.client.http.HttpResponse
import org.jsoup.nodes.Document

class HtmlResponse(val connectionResponse: ConnectionResponse,
                   val document: Document)
{
    val httpResponse: HttpResponse
        get() = connectionResponse.httpResponse

    val cookies: Map<String, String>
        get() = connectionResponse.cookies

    fun isOk() = httpResponse.statusCode == 200

    fun isMovedPermanently() = httpResponse.statusCode == 301

    fun isFound() = httpResponse.statusCode == 302
}
