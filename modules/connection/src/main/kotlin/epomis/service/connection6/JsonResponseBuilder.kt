package epomis.service.connection6

import com.fasterxml.jackson.databind.ObjectMapper

import com.google.api.client.http.HttpResponse

import epomis.service.connection6.response.JsonResponse

internal class JsonResponseBuilder(private val connectionResponseBuilder: ConnectionResponseBuilder,
                                   private val objectMapper: ObjectMapper)
{
    fun <T> fromHttpResponse(httpResponse: HttpResponse, responseType: Class<T>) = JsonResponse(
            connectionResponse = connectionResponseBuilder.fromHttpResponse(httpResponse),
            json = objectMapper.readValue(httpResponse.content, responseType)!!
    )
}
