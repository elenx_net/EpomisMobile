package epomis.service.connection6.http

class WrongMethodException(message: String) : RuntimeException(message)
