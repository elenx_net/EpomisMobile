package epomis.service.connection6.request

enum class RequestContentType
{
    MULTIPART,
    URL_ENCODED
}
