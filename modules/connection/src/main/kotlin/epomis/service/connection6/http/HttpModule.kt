package epomis.service.connection6.http

import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson.JacksonFactory
import epomis.configuration.internet.internetModule

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val httpModule = Kodein.Module("connection.http")
{
    /** INTERNET **/
    importOnce(internetModule, allowOverride = true)

    /** HTTP REQUEST BUILDER **/
    bind<HttpRequestBuilder>() with singleton { HttpRequestBuilder(instance(), instance(), instance(), instance(), instance()) }
        bind<RequestFactory>() with singleton { RequestFactory(instance(), instance(), instance(), instance()) }
            bind<RequestInitializer>() with singleton { RequestInitializer(instance()) }
            bind<HeadersBuilder>() with singleton { HeadersBuilderImpl(instance()) }
                bind<HeadersMerger>() with singleton { HeadersMerger() }
        bind<JsonContentBuilder>() with singleton { JsonContentBuilder(instance()) }
        bind<MultipartContentBuilder>() with singleton { MultipartContentBuilder(instance(), instance()) }
            bind<MultipartPartFactory>() with singleton { MultipartPartFactory() }
            bind<InputStreamEntryBuilder>() with singleton { InputStreamEntryBuilder() }
        bind<UrlEncodedContentBuilder>() with singleton { UrlEncodedContentBuilder() }
        bind<GetParametersBuilder>() with singleton { GetParametersBuilder() }
}