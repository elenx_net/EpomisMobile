package epomis.service.connection6

import com.google.api.client.http.HttpResponse

internal interface CookieExtractor
{
    fun extractFromHttpResponse(httpResponse: HttpResponse): Map<String, String>
}