package epomis.service.connection6.http

enum class HttpMethod
{
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;

    companion object
    {
        private val mappings = HashMap<String, HttpMethod>(16)

        init
        {
            for (httpMethod in values())
            {
                mappings[httpMethod.name] = httpMethod
            }
        }

        fun resolve(method: String?): HttpMethod? = if(method == null) null else mappings[method]
    }

    fun matches(method: String): Boolean = this === resolve(method)
}