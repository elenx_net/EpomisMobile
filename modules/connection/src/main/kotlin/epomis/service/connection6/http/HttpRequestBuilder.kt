package epomis.service.connection6.http

import com.google.api.client.http.HttpRequest

import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.RequestContentType

internal class HttpRequestBuilder(private val requestFactory: RequestFactory,
                                  private val jsonContentBuilder: JsonContentBuilder,
                                  private val multipartBodyBuilder: MultipartContentBuilder,
                                  private val urlEncodedContentBuilder: UrlEncodedContentBuilder,
                                  private val getParametersBuilder: GetParametersBuilder)
{
    fun requestWithBody(method: HttpMethod, connectionRequest: ConnectionRequest): HttpRequest
    {
        if(connectionRequest.jsonObject != null)
        {
            return requestFactory.buildRequest(method, connectionRequest, jsonContentBuilder)
        }

        if(RequestContentType.MULTIPART == connectionRequest.contentType)
        {
            return requestFactory.buildRequest(method, connectionRequest, multipartBodyBuilder)
        }

        return requestFactory.buildRequest(method, connectionRequest, urlEncodedContentBuilder)
    }

    fun requestWithUrlParameters(method: HttpMethod, connectionRequest: ConnectionRequest): HttpRequest
    {
        val urlWithGetParameters = getParametersBuilder.buildUrlWithGetParameters(connectionRequest)
        val httpRequest = requestFactory.buildRequest(method, connectionRequest, UrlEncodedContentBuilder())

        httpRequest.url = urlWithGetParameters

        return httpRequest
    }
}
