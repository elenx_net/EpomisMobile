package epomis.service.connection6

import com.google.api.client.http.HttpRequest
import com.google.api.client.http.HttpResponse

import epomis.service.connection6.response.HtmlResponse

import org.jsoup.Jsoup

internal class HtmlResponseBuilder(private val connectionResponseBuilder: ConnectionResponseBuilder)
{
    fun fromHttpResponse(httpResponse: HttpResponse, httpRequest: HttpRequest): HtmlResponse
    {
        val contentTypeFromRequest = httpRequest.headers.contentType
        val contentCharsetFromResponse = httpResponse.contentCharset
        val charsetName = when
        {
            contentCharsetFromResponse.isRegistered -> contentCharsetFromResponse.toString()
            contentTypeFromRequest.contains("charset") -> obtainCharsetNameFromRequest(httpRequest)
            else -> "UTF-8"
        }
        return HtmlResponse(
        connectionResponse = connectionResponseBuilder.fromHttpResponse(httpResponse),
        document = parseHtml(httpResponse, charsetName)
    )
    }

    private fun parseHtml(httpResponse: HttpResponse, charsetName: String)
        = Jsoup.parse(
            httpResponse.content, charsetName,
            httpResponse.request.url.buildAuthority()
        )

    private fun obtainCharsetNameFromRequest(httpRequest: HttpRequest) = httpRequest.headers.contentType.substringAfter("=").trim()
}
