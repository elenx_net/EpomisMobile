package epomis.service.connection6

import com.google.api.client.http.HttpResponse
import com.google.common.net.HttpHeaders

import org.apache.commons.lang3.StringUtils

import java.util.ArrayList

internal class CookieExtractorImpl : CookieExtractor
{
    override fun extractFromHttpResponse(httpResponse: HttpResponse): Map<String, String> = httpResponse
        .headers
        .entries
        .asSequence()
        .filter { isSetCookieHeader(it) }
        .flatMap { extractCookiesFromHeader(it) }
        .toMap()

    private fun isSetCookieHeader(httpHeader: Map.Entry<String, Any>): Boolean = HttpHeaders.SET_COOKIE.equals(httpHeader.key, true)

    private fun extractCookiesFromHeader(cookies: Map.Entry<String, Any>):Sequence<Pair<String, String>>
    {
        val cookiesHeaders = cookies.value as ArrayList<String>

        return cookiesHeaders
            .asSequence()
            .flatMap { toCookieArray(it) }
            .map { cookieAsMapEntry(it) }
    }

    private fun toCookieArray(cookiesString: String) = cookiesString.split(";").asSequence()

    private fun cookieAsMapEntry(cookieString: String): Pair<String, String>
    {
        val cookieArray = cookieString.split("=")

        val key = cookieArray[0]
        val value = if (cookieArray.size > 1) cookieArray[1] else StringUtils.EMPTY

        return key.trim() to value.trim()
    }
}
