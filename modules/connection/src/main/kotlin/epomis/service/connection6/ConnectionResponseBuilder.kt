package epomis.service.connection6

import com.google.api.client.http.HttpResponse

import epomis.service.connection6.response.ConnectionResponse

internal class ConnectionResponseBuilder(private val cookieExtractor: CookieExtractor)
{
    fun fromHttpResponse(httpResponse: HttpResponse): ConnectionResponse
    {
        val url = httpResponse.request.url

        return ConnectionResponse(
            cookies = cookieExtractor.extractFromHttpResponse(httpResponse),
            referrer = url.buildAuthority() + url.rawPath,
            httpResponse = httpResponse
        )
    }
}
