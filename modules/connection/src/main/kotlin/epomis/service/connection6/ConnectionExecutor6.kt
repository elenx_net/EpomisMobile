package epomis.service.connection6

import com.google.api.client.http.HttpRequest

import epomis.service.connection6.http.HttpMethod
import epomis.service.connection6.http.HttpRequestBuilder
import epomis.service.connection6.request.ConnectionRequest

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.rx2.rxFlowable
import kotlinx.coroutines.withTimeoutOrNull
import java.util.concurrent.Future
import kotlin.coroutines.CoroutineContext

@ExperimentalCoroutinesApi
internal class ConnectionExecutor6(private val httpRequestBuilder: HttpRequestBuilder,
                                   private val htmlResponseBuilder: HtmlResponseBuilder,
                                   private val jsonResponseBuilder: JsonResponseBuilder) : ConnectionService6, CoroutineScope
{
    override val coroutineContext: CoroutineContext
        = Dispatchers.IO + SupervisorJob()

    override fun reactivePostForHtml(request: ConnectionRequest) = rxFlowable { send(postForHtml(request)) }
    override fun reactiveGetForHtml(request: ConnectionRequest) = rxFlowable { send(getForHtml(request)) }
    override fun <T> reactivePostForJson(request: ConnectionRequest, responseType: Class<T>) = rxFlowable { send(postForJson(request, responseType)) }
    override fun <T> reactiveGetForJson(request: ConnectionRequest, responseType: Class<T>) =  rxFlowable { send(getForJson(request, responseType)) }
    override fun <T> reactivePutForJson(request: ConnectionRequest, responseType: Class<T>) = rxFlowable { send(putForJson(request, responseType)) }
    override fun <T> reactiveDeleteForJson(request: ConnectionRequest, responseType: Class<T>) = rxFlowable { send(deleteForJson(request, responseType)) }

    override suspend fun getForHtml(request: ConnectionRequest) = requestHtml(buildRequestWithUrlParameters(HttpMethod.GET, request))
    override suspend fun postForHtml(request: ConnectionRequest) = requestHtml(buildRequestWithBody(HttpMethod.POST, request))
    override suspend fun <T> getForJson(request: ConnectionRequest, responseType: Class<T>) = requestJson(buildRequestWithUrlParameters(HttpMethod.GET, request), responseType)
    override suspend fun <T> postForJson(request: ConnectionRequest, responseType: Class<T>) = requestJson(buildRequestWithBody(HttpMethod.POST, request), responseType)
    override suspend fun <T> putForJson(request: ConnectionRequest, responseType: Class<T>) = requestJson(buildRequestWithBody(HttpMethod.PUT, request), responseType)
    override suspend fun <T> deleteForJson(request: ConnectionRequest, responseType: Class<T>) = requestJson(buildRequestWithBody(HttpMethod.DELETE, request), responseType)

    private suspend fun <T> requestJson(httpRequest: HttpRequest, responseType: Class<T>) = jsonResponseBuilder.fromHttpResponse(executeRequest(httpRequest), responseType)
    private suspend fun requestHtml(httpRequest: HttpRequest) = htmlResponseBuilder.fromHttpResponse(executeRequest(httpRequest), httpRequest)
    private suspend fun executeRequest(httpRequest: HttpRequest) = httpRequest.execute() ?: throw RuntimeException("Http request failed")

    private fun buildRequestWithUrlParameters(method: HttpMethod, request: ConnectionRequest) = httpRequestBuilder.requestWithUrlParameters(method, request)
    private fun buildRequestWithBody(method: HttpMethod, request: ConnectionRequest) = httpRequestBuilder.requestWithBody(method, request)
}

private suspend fun <T> Future<T>.awaitOrNull(pollDelayMillis: Long = 5): T?
{
    while(!isDone && !isCancelled)
        delay(pollDelayMillis)
    return if(isCancelled)
        null
    else
        get()
}
