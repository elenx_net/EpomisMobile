package epomis.service.connection6.http

import com.google.api.client.http.ByteArrayContent
import com.google.api.client.http.HttpHeaders
import com.google.api.client.http.MultipartContent

import epomis.service.connection6.request.DataEntry

import org.apache.commons.lang3.StringUtils

private const val CONTENT_DISPOSITION_TEXT_FORMAT = "form-data; name=\"%s\""
private const val NO_TYPE = StringUtils.EMPTY

class MultipartPartFactory
{
    fun build(dataEntry: DataEntry): MultipartContent.Part
    {
        val part = MultipartContent.Part()
        val headers = HttpHeaders()

        part.content = ByteArrayContent(NO_TYPE, dataEntry.value.toByteArray())

        headers.set(
            com.google.common.net.HttpHeaders.CONTENT_DISPOSITION,
            String.format(CONTENT_DISPOSITION_TEXT_FORMAT, dataEntry.key)
        )
        part.headers = headers

        return part
    }
}