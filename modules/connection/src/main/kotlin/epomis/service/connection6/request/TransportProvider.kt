package epomis.service.connection6.request

enum class TransportProvider
{
    APACHE,
    NET
}