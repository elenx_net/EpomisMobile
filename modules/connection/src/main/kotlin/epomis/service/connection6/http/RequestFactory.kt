package epomis.service.connection6.http

import com.google.api.client.http.GenericUrl
import com.google.api.client.http.HttpContent
import com.google.api.client.http.HttpRequest
import com.google.api.client.http.HttpRequestFactory
import com.google.api.client.http.apache.ApacheHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport

import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.TransportProvider

import org.apache.commons.lang3.StringUtils

import org.apache.http.protocol.HTTP

private const val ENCODING = "gzip, deflate"

internal class RequestFactory(netHttpTransport: NetHttpTransport,
                              apacheHttpTransport: ApacheHttpTransport,
                              initializer: RequestInitializer,
                              private val headersBuilder: HeadersBuilder)
{
    private val apacheBasedFactory: HttpRequestFactory = apacheHttpTransport.createRequestFactory(initializer)
    private val netBasedFactory: HttpRequestFactory = netHttpTransport.createRequestFactory(initializer)

    fun buildRequest(method: HttpMethod, request: ConnectionRequest, contentBuilder: HttpContentBuilder): HttpRequest
    {
        val content = contentBuilder.build(method, request)

        val httpRequest = buildRequestWithSpecifiedTransport(method, request, content)
        val httpHeaders = headersBuilder.fromConnectionRequest(request)
        httpHeaders.apply {
            set(HTTP.CONN_DIRECTIVE, HTTP.CONN_KEEP_ALIVE)
            acceptEncoding = ENCODING
        }

        if (request.referrer != StringUtils.EMPTY)
        {
            httpHeaders[org.apache.http.HttpHeaders.REFERER] = request.referrer
        }

        httpRequest.apply {
            suppressUserAgentSuffix = isUsingCustomUserAgent(request)
            headers = httpHeaders
            followRedirects = request.shouldRedirect
            throwExceptionOnExecuteError = false
        }

        return httpRequest
    }

    private fun isUsingCustomUserAgent(request: ConnectionRequest)
        = request
            .headers
            .containsKey("User-Agent")

    private fun buildRequestWithSpecifiedTransport(method: HttpMethod, request: ConnectionRequest, content: HttpContent): HttpRequest
    {
        val httpRequestFactory = if (TransportProvider.NET == request.transportProvider) netBasedFactory else apacheBasedFactory

        return httpRequestFactory.buildRequest(method.name, GenericUrl(request.url), content)
    }
}