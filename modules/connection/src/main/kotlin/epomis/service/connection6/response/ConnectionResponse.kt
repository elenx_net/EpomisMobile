package epomis.service.connection6.response

import com.google.api.client.http.HttpResponse

import epomis.service.connection6.request.ConnectionRequest

import java.io.Serializable

class ConnectionResponse(@Transient val httpResponse: HttpResponse,
                         val referrer: String,
                         @Transient val cookies: Map<String, String>) : Serializable
{

    fun nextRequest()
        = ConnectionRequest(
            cookies = cookies,
            referrer = referrer
    )

    data class ConnectionResponseBuilder(var cookies: Map<String, String> = emptyMap())
}
