package epomis.service.connection6.http

import com.google.api.client.http.HttpContent
import com.google.api.client.http.HttpMediaType
import com.google.api.client.http.UrlEncodedContent

import epomis.service.connection6.request.ConnectionRequest

private val urlEncodedMediaType = HttpMediaType("application/x-www-form-urlencoded")

class UrlEncodedContentBuilder : HttpContentBuilder
{
    override fun build(method: HttpMethod, request: ConnectionRequest): HttpContent
    {
        val requestData = dataAsMap(request)

        val urlEncodedContent = UrlEncodedContent(requestData)
        urlEncodedContent.mediaType = urlEncodedMediaType

        return urlEncodedContent
    }

    private fun dataAsMap(request: ConnectionRequest): Map<String, String>
        = request
            .data
            .asSequence()
            .map { it.key to it.value }
            .toMap()
}
