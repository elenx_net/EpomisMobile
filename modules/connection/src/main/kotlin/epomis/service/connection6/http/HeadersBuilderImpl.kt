package epomis.service.connection6.http

import epomis.service.connection6.request.ConnectionRequest

import com.google.api.client.http.HttpHeaders

import java.util.Collections

class HeadersBuilderImpl(private val headersMerger: HeadersMerger) : HeadersBuilder
{
    override fun fromConnectionRequest(request: ConnectionRequest): HttpHeaders
    {
        val cookies = extractCookiesStringFromRequest(request)
        val headers = asHttpHeaders(cookies)

        headersMerger.mergeHeaders(request.headers, headers)

        return headers
    }

    private fun extractCookiesStringFromRequest(request: ConnectionRequest)
        = request
            .cookies
            .entries
            .asSequence()
            .map { it.toString() }
            .joinToString(separator = "; ")

    private fun asHttpHeaders(cookies: String): HttpHeaders
    {
        val headers = HttpHeaders()
        headers.set("Cookie", Collections.singletonList(cookies))

        return headers
    }
}
