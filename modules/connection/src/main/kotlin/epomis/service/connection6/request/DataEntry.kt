package epomis.service.connection6.request

import java.io.InputStream

data class DataEntry(var key: String,
                     var value: String,
                     var inputStream: InputStream? = null)
{
    constructor(key: String, value: String): this(key, value, null)

    fun hasInputStream() = inputStream != null
}