package epomis.service.connection6.http

import com.google.api.client.http.HttpRequest
import com.google.api.client.http.HttpRequestInitializer
import com.google.api.client.json.JsonObjectParser

class RequestInitializer(private val jsonObjectParser: JsonObjectParser) : HttpRequestInitializer
{
    override fun initialize(request: HttpRequest)
    {
        request.parser = jsonObjectParser
    }
}
