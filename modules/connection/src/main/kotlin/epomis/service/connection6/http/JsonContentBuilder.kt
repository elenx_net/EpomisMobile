package epomis.service.connection6.http

import epomis.service.connection6.request.ConnectionRequest

import com.google.api.client.http.HttpContent
import com.google.api.client.http.json.JsonHttpContent
import com.google.api.client.json.JsonFactory

private const val WRONG_METHOD_EXCEPTION = "You can't send files and JSON with use of GET method! (If you have to send JSON with GET, just use the ObjectMapper and send it with data())"

class JsonContentBuilder(private val jsonFactory: JsonFactory) : HttpContentBuilder
{
    override fun build(method: HttpMethod, request: ConnectionRequest): HttpContent
    {
        if(HttpMethod.POST.matches(method.name))
        {
            return JsonHttpContent(jsonFactory, request.jsonObject)
        }
        else
        {
            throw WrongMethodException(WRONG_METHOD_EXCEPTION)
        }
    }
}
