package epomis.service.connection6.http

import com.google.api.client.http.HttpHeaders
import com.google.api.client.http.MultipartContent

import epomis.service.connection6.request.DataEntry

import java.io.InputStream
import java.nio.ByteBuffer

import com.google.api.client.http.ByteArrayContent
import com.google.api.client.http.InputStreamContent
import com.google.common.net.HttpHeaders.CONTENT_DISPOSITION

private const val CONTENT_DISPOSITION_ATTACHMENT_FORMAT = "form-data; name=\"%s\"; filename=\"%s\""
private const val FILE_TOO_BIG = "File is too big (50 MB) to be sent as a ByteArray. Send it as InputStream"
private const val MAX_FILE_SIZE = 50 * 1024 * 1024

class InputStreamEntryBuilder
{
    fun build(dataEntry: DataEntry, isRequestChunked: Boolean): MultipartContent.Part
    {
        val headers = HttpHeaders()

        val part = if (isRequestChunked) dataAsInputStream(dataEntry, headers) else dataAsByteArray(dataEntry, headers)

        headers.set(CONTENT_DISPOSITION, buildContentDisposition(dataEntry))
        part.headers = headers

        return part
    }

    private fun dataAsInputStream(dataEntry: DataEntry, headers: HttpHeaders)
        = MultipartContent.Part(InputStreamContent(headers.contentType, dataEntry.inputStream))

    private fun dataAsByteArray(dataEntry: DataEntry, headers: HttpHeaders): MultipartContent.Part
    {
        val inputStream = dataEntry.inputStream!!
        val byteBuffer = ByteBuffer.allocate(MAX_FILE_SIZE)

        var totalBytesRead = 0

        while (totalBytesRead < MAX_FILE_SIZE && moreBytesToRead(inputStream))
        {
            val read = inputStream.read(byteBuffer.array())
            totalBytesRead += read
        }

        if (moreBytesToRead(inputStream))
        {
            throw FileTooBigException(FILE_TOO_BIG)
        }

        val resultBytes = ByteArray(totalBytesRead)
        byteBuffer.get(resultBytes, 0, totalBytesRead)

        return MultipartContent.Part(ByteArrayContent(headers.contentType, resultBytes))
    }

    private fun buildContentDisposition(dataEntry: DataEntry)
        = String.format(CONTENT_DISPOSITION_ATTACHMENT_FORMAT, dataEntry.key, dataEntry.value)

    private fun moreBytesToRead(inputStream: InputStream) = inputStream.available() > 0
}