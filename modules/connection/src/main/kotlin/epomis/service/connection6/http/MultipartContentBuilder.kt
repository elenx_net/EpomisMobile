package epomis.service.connection6.http

import com.google.api.client.http.HttpContent
import com.google.api.client.http.HttpMediaType
import com.google.api.client.http.MultipartContent

import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.DataEntry

private val MULTIPART_MEDIA_TYPE = HttpMediaType("multipart/form-data")

internal class MultipartContentBuilder(
    private val multipartPartBuilder: MultipartPartFactory,
    private val inputStreamEntryBuilder: InputStreamEntryBuilder
) : HttpContentBuilder
{

    override fun build(method: HttpMethod, request: ConnectionRequest): HttpContent
    {
        val multipartContent = MultipartContent()
        multipartContent.mediaType = MULTIPART_MEDIA_TYPE
        setupContentBoundary(request, multipartContent)

        request
            .data
            .asSequence()
            .map { toMultipartPart(it, request.isChunked) }
            .forEach { multipartContent.addPart(it) }

        return multipartContent
    }

    private fun setupContentBoundary(request: ConnectionRequest, multipartContent: MultipartContent)
    {
        val boundary: String? by request.requestArgs.withDefault { "__END_OF_PART__" }
        multipartContent.boundary = boundary
    }

    private fun toMultipartPart(dataEntry: DataEntry, isRequestChunked: Boolean): MultipartContent.Part =
        if (dataEntry.hasInputStream())
        {
            inputStreamEntryBuilder.build(dataEntry, isRequestChunked)
        } else
        {
            multipartPartBuilder.build(dataEntry)
        }
}
