package epomis.service.connection6.http

import epomis.service.connection6.request.ConnectionRequest

import com.google.api.client.http.HttpContent

internal interface HttpContentBuilder
{
    fun build(method: HttpMethod, request: ConnectionRequest): HttpContent
}
