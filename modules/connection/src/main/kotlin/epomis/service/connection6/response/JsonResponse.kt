package epomis.service.connection6.response

import com.google.api.client.http.HttpResponse

class JsonResponse<T>(val connectionResponse: ConnectionResponse,
                      val json: T)
{
    val httpResponse: HttpResponse
        get() = connectionResponse.httpResponse

    val cookies: Map<String, String>
        get() = connectionResponse.cookies
}
