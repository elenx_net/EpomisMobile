package epomis.service.connection6.http

class FileTooBigException(message: String) : Exception(message)