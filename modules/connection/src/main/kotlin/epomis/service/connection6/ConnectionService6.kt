package epomis.service.connection6

import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.HtmlResponse
import epomis.service.connection6.response.JsonResponse

import org.reactivestreams.Publisher

interface ConnectionService6
{
    suspend fun postForHtml(request: ConnectionRequest): HtmlResponse
    suspend fun getForHtml(request: ConnectionRequest): HtmlResponse
    suspend fun <T> postForJson(request: ConnectionRequest, responseType: Class<T>): JsonResponse<T>
    suspend fun <T> getForJson(request: ConnectionRequest, responseType: Class<T>): JsonResponse<T>
    suspend fun <T> putForJson(request: ConnectionRequest, responseType: Class<T>): JsonResponse<T>
    suspend fun <T> deleteForJson(request: ConnectionRequest, responseType: Class<T>): JsonResponse<T>

    fun reactivePostForHtml(request: ConnectionRequest): Publisher<HtmlResponse>
    fun reactiveGetForHtml(request: ConnectionRequest): Publisher<HtmlResponse>
    fun <T> reactivePostForJson(request: ConnectionRequest, responseType: Class<T>): Publisher<JsonResponse<T>>
    fun <T> reactiveGetForJson(request: ConnectionRequest, responseType: Class<T>): Publisher<JsonResponse<T>>
    fun <T> reactivePutForJson(request: ConnectionRequest, responseType: Class<T>): Publisher<JsonResponse<T>>
    fun <T> reactiveDeleteForJson(request: ConnectionRequest, responseType: Class<T>): Publisher<JsonResponse<T>>
}

suspend inline fun <reified T> ConnectionService6.postForJson(request: ConnectionRequest) = postForJson(request, T::class.java)
suspend inline fun <reified T> ConnectionService6.getForJson(request: ConnectionRequest) = getForJson(request, T::class.java)
suspend inline fun <reified T> ConnectionService6.putForJson(request: ConnectionRequest) = putForJson(request, T::class.java)
suspend inline fun <reified T> ConnectionService6.deleteForJson(request: ConnectionRequest) = deleteForJson(request, T::class.java)
