package epomis.connection

import kotlin.reflect.KClass

sealed class RequestBody<out T>(val content: T)
object NoBody : RequestBody<Nothing?>(null)
class ByteArrayBody(body: ByteArray) : RequestBody<ByteArray>(body)

inline fun <reified T : Any> JsonBody(body: T) = JsonBody(body, T::class)
class JsonBody<T : Any>(body: T, val bodyClass: KClass<T>) : RequestBody<T>(body)

class MultipartFormBody(body: Map<String, MultipartFormEntryValue<*>>) :
    RequestBody<Map<String, MultipartFormEntryValue<*>>>(body)

sealed class MultipartFormEntryValue<T>(val content: T)
class ByteArrayValue(content: ByteArray) : MultipartFormEntryValue<ByteArray>(content)
class StringValue(content: String) : MultipartFormEntryValue<String>(content)

