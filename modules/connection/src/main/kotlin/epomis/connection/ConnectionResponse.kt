package epomis.connection

import org.jsoup.nodes.Document
import kotlin.reflect.KClass

interface ConnectionResponse
{
    val bodyString: String
    val bodyDocument: Document
    fun <T : Any> body(bodyClass: KClass<T>): T

    val statusCode: Int
    val headers: Map<String, String>
    val cookies: Map<String, String>
}

inline fun <reified T : Any> ConnectionResponse.body() = body(T::class)
