package epomis.connection

import org.reactivestreams.Publisher

interface ConnectionService7
{
    fun send(request: ConnectionRequest): ConnectionResponse
    fun sendAsync(request: ConnectionRequest): Publisher<ConnectionResponse>
}