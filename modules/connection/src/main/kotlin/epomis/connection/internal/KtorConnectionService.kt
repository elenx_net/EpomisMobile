package epomis.connection.internal

import com.fasterxml.jackson.databind.ObjectMapper
import epomis.connection.ByteArrayBody
import epomis.connection.ByteArrayValue
import epomis.connection.ConnectionRequest
import epomis.connection.ConnectionResponse
import epomis.connection.ConnectionService7
import epomis.connection.GetRequest
import epomis.connection.JsonBody
import epomis.connection.MultipartFormBody
import epomis.connection.NoBody
import epomis.connection.PostRequest
import epomis.connection.StringValue
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.cookies.AcceptAllCookiesStorage
import io.ktor.client.features.cookies.HttpCookies
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.client.request.forms.formData
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.invoke
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.utils.EmptyContent
import io.ktor.content.ByteArrayContent
import io.ktor.http.Cookie
import io.ktor.http.Url
import io.ktor.http.content.PartData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.reactive.publish
import kotlinx.coroutines.runBlocking
import org.reactivestreams.Publisher
import java.net.URL

internal class KtorConnectionService(private val objectMapper: ObjectMapper) : ConnectionService7
{
    private val cookieStorage = AcceptAllCookiesStorage()
    private val client = HttpClient(OkHttp) {
        install(HttpCookies) {
            storage = cookieStorage
        }
    }

    @ExperimentalCoroutinesApi
    override fun sendAsync(request: ConnectionRequest): Publisher<ConnectionResponse> = publish {
        runCatching { suspendSend(request) }
            .onSuccess { this.send(it) }
            .onFailure { this.close(it) }
    }

    override fun send(request: ConnectionRequest): ConnectionResponse = runBlocking {
        suspendSend(request)
    }

    private suspend fun suspendSend(request: ConnectionRequest): ConnectionResponse
    {
        val requestBuilder = HttpRequestBuilder(URL(request.url)).apply {
            this.body = requestBodyFor(request)
            request.headers.forEach(this::header)
        }

        return when (request)
        {
            is GetRequest -> client.get<HttpResponse>(requestBuilder)
            is PostRequest -> client.post<HttpResponse>(requestBuilder)
        }.let { KtorConnectionResponse(it, objectMapper, cookiesFor(request.url)) }
    }

    private suspend fun cookiesFor(url: String): List<Cookie> = cookieStorage.get(Url(url))

    private fun requestBodyFor(request: ConnectionRequest): Any = when (request.body)
    {
        NoBody -> EmptyContent
        is JsonBody -> objectMapper.writeValueAsString(request.body.content)
        is ByteArrayBody -> ByteArrayContent(request.body.content)
        is MultipartFormBody -> MultiPartFormDataContent(formDataFor(request.body))
    }

    private fun formDataFor(requestBody: MultipartFormBody): List<PartData> = formData {
        requestBody.content.forEach {
            when (val value = it.value)
            {
                is ByteArrayValue -> append(it.key, value.content)
                is StringValue -> append(it.key, value.content)
            }
        }
    }
}