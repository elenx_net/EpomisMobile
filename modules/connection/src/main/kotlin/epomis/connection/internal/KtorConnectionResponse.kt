package epomis.connection.internal

import com.fasterxml.jackson.databind.ObjectMapper
import epomis.connection.ConnectionResponse
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.Cookie
import io.ktor.util.toMap
import kotlinx.coroutines.runBlocking
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import kotlin.reflect.KClass

internal class KtorConnectionResponse(
    private val httpResponse: HttpResponse,
    private val objectMapper: ObjectMapper,
    private val cookieList: List<Cookie>
) : ConnectionResponse
{
    private lateinit var cachedParsedBody: Any

    override val bodyString: String by lazy { runBlocking { httpResponse.readText() } }
    override val bodyDocument: Document by lazy { Jsoup.parse(bodyString) }

    override fun <T : Any> body(bodyClass: KClass<T>): T =
        if (this::cachedParsedBody.isInitialized)
        {
            cachedParsedBody as T
        } else
        {
            objectMapper.readValue(bodyString, bodyClass.java)
                .also { cachedParsedBody = it }
        }

    override val statusCode: Int by lazy { httpResponse.status.value }
    override val headers: Map<String, String> by lazy {
        httpResponse.headers.toMap()
            .map { it.key to it.value.first() }
            .toMap()
    }

    override val cookies: Map<String, String> by lazy {
        cookieList
            .map { it.name to it.value }
            .toMap()
    }
}