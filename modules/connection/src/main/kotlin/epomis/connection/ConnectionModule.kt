package epomis.connection

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import epomis.connection.internal.KtorConnectionService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val connectionModule = Kodein.Module("connection")
{
    bind<ObjectMapper>(overrides = true) with singleton {
        ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .registerKotlinModule()
    }

    bind<ConnectionService7>() with singleton {
        KtorConnectionService(instance())
    }
}