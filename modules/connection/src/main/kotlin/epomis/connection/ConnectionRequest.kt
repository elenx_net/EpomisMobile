package epomis.connection

sealed class ConnectionRequest(
    val url: String,
    val body: RequestBody<*>,
    val headers: Map<String, String>,
    val cookies: Map<String, String>
)

class GetRequest(
    url: String,
    body: RequestBody<*> = NoBody,
    headers: Map<String, String> = emptyMap(),
    cookies: Map<String, String> = emptyMap()
) : ConnectionRequest(url, body, headers, cookies)

class PostRequest(
    url: String,
    body: RequestBody<*> = NoBody,
    headers: Map<String, String> = emptyMap(),
    cookies: Map<String, String> = emptyMap()
) : ConnectionRequest(url, body, headers, cookies)