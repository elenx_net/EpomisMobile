package epomis.configuration.internet

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

import com.google.api.client.http.apache.ApacheHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.JsonObjectParser
import com.google.api.client.json.jackson.JacksonFactory

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val internetModule = Kodein.Module("connection.internet") {
    bind<ObjectMapper>(overrides = true) with singleton { ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).registerKotlinModule() }
    bind<NetHttpTransport>() with singleton { NetHttpTransport() }
    bind<ApacheHttpTransport>() with singleton { ApacheHttpTransport() }
    bind<JsonFactory>() with singleton { JacksonFactory() }
    bind<JsonObjectParser>() with singleton { JsonObjectParser(instance()) }
}