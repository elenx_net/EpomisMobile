package epomis.configuration.internet

import kotlinx.coroutines.asCoroutineDispatcher

import org.apache.commons.lang3.concurrent.BasicThreadFactory

import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.ThreadFactory

fun threadFactory(prefix: String): ThreadFactory = BasicThreadFactory.Builder().daemon(true).namingPattern("$prefix-%d").build()
val HttpClientThreadFactory = ScheduledThreadPoolExecutor(25, threadFactory("http-client-pool"))
val HttpClientPool = HttpClientThreadFactory.asCoroutineDispatcher()