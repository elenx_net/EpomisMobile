package epomis

import epomis.configuration.internet.internetModule
import epomis.service.connection6.ConnectionExecutor6
import epomis.service.connection6.ConnectionResponseBuilder
import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.CookieExtractor
import epomis.service.connection6.CookieExtractorImpl
import epomis.service.connection6.HtmlResponseBuilder
import epomis.service.connection6.JsonResponseBuilder
import epomis.service.connection6.http.HeadersBuilder
import epomis.service.connection6.http.HeadersBuilderImpl
import epomis.service.connection6.http.HeadersMerger
import epomis.service.connection6.http.HttpRequestBuilder
import epomis.service.connection6.http.RequestFactory
import epomis.service.connection6.http.RequestInitializer
import epomis.service.connection6.http.httpModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val connectionModule = Kodein.Module("connection")
{
    /** INTERNET **/
    importOnce(internetModule, allowOverride = true)

    /** HTTP **/
    importOnce(httpModule)

    /** CONNECTION SERVICE **/
    bind<ConnectionService6>() with singleton { ConnectionExecutor6(instance(), instance(), instance()) }
        bind<HtmlResponseBuilder>() with singleton { HtmlResponseBuilder(instance()) }
            bind<ConnectionResponseBuilder>() with singleton { ConnectionResponseBuilder(instance()) }
                bind<CookieExtractor>() with singleton { CookieExtractorImpl() }
        bind<JsonResponseBuilder>() with singleton { JsonResponseBuilder(instance(), instance()) }
}