plugins {
    kotlin
    kodein
    rx
    coroutines
    test
}

dependencies {
    implementation(project(":modules:utils"))
    implementation("io.ktor:ktor-client-android:1.3.1")
    implementation("io.ktor:ktor-client-json-jvm:1.3.1")
    implementation("io.ktor:ktor-client-okhttp:1.3.1")

    api("com.google.api-client:google-api-client:1.24.1")
    api("com.google.http-client:google-http-client:1.24.1")
    api("com.google.http-client:google-http-client-jackson:1.24.1")
    implementation("com.google.http-client:google-http-client-xml:1.24.1")

    implementation("org.apache.httpcomponents:httpclient-android:4.3.5.1")
    implementation("org.apache.commons:commons-lang3:3.7")
    implementation("org.apache.commons:commons-collections4:4.1")

    api("org.jsoup:jsoup:1.13.1")
}