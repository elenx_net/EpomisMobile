Closes #211

# UI GUIDE EPOMIS v0.2
<br>


> Annotation<br>
>
Czym jest UI? Jest to most o konkretnej długości, szerokości, kolorach i poręczach. Czym jest UX? Jest to doświadczenie jakie odczuwasz przechodząc na drugą stonę.
Poniższy Guide został napisany z myślą o nowych projektantach, chcących pomóc przy projektowaniu aplikacji Epomis.
Z początku Guide jest ubogi, ale z czasem będzie nabierał pomocnych informacji i materiałów koniecznych do przyswojenia.
Drugie proste założenie to uczymy się od siebie, wymieniamy doświadczeniem i rozwijamy nasze abstrakcyjne myślenie :)

**Gdybym spytał klientów czego pragną, powiedzieliby, że szybszego konia. - Henry Ford**
<br><br>





Proces realizacji prototypu
![alt text][logo]

[logo]: https://i.ibb.co/sKrK63m/Plan2.jpg "Logo Title Text 2"

***

#### Spis treści<br>
**1.** Tool Box<br>
**2.** Material Design<br>
**3.** Typography & Text Style<br>
**4.** Pattern Library<br>
**5.** Images and Icons<br>
**6.** X Podstawowych Heurystyk UX<br>
**7.** Links<br>
**8.** TODO<br>
<br>
### 1. Tool Box
<br>
Praca nad dużym projektem graficznym przewidzianym na x ilość projektantów wymaga wspólnych zasobów oraz narzędzi.
Można to potraktować jako bibliotekę startową dla newbie :) na której pracuje każdy i każdy ją posiada, jest to konieczne dla zachowania wspólnego stylu i szybszej
organizacji pracy.


#### **Resources**
<br>

![alt text](https://i.ibb.co/yNSSJXz/reso.jpg  "Logo Title Text 2")

<br>

Na gicie znajdziesz wszystkie potrzebne materiały do zaczęcia swojej przygody, jeśli czegoś nie rozumiesz to zawsze pytaj. W ,,Resources" trzymamy tzw. pakiet startowy. W krótce znajdziesz tam
ikony z których wszyscy korzystamy, kolory, images z ilustracjami i logotyp - jeszcze nad tym pracujemy :). W miarę postępów zaczniemy wydawać versje naszego dziecka w folderze Version, nad częstotliwością pomyślimy.
Ważne byśmy mogli zawsze się cofnąć i coś poprawić :)

#### **Design**
- **Papier i ołówek** <br>najpotężniejsze narzędzie grafika, najlepsze projekty wychodzą z pod kartki i papieru a później cyfrowo :)
- **Design software:
Photoshop, Illustrator, Affinity Designer, Affinity Photo.** <br> wypisałem te które ja preferuję, wybierz swój ulubiony, najlepiej coś do edycji zdjęć i coś do projektowania lepszych wektorów


#### **Mockup and prototype**
- **Adobe XD** <- bez tego ani rusz, darmowa wersja



### 2. Material Design
<br>

**https://www.material.io**

Ten link powinen być znany każdemu projektantowi UI/UX. Material Design to Design Language System stworzony przez Google na potrzebę ujednolicenia identyfikacji graficznej swoich produktów.
Pod linkiem znajdziesz treściwy przewodnik po MD. Jeśli jeszcze nie wiesz na czym polegają takie założenia jak elevator i surfcase to musisz koniecznie nadrobić.


### 3. Typography & Text Style
<br>
Na ten moment narzucanie głównego fontu jest zbędne, za to lepszym rozwiązanie jest operowanie na trzech fontach, które pasują do siebie w każdej konfiguracji.
Jednak max użytych fontów ograniczajmy do dwóch ze względu estetycznych.

1. **OpenSans**<br>
https://fonts.google.com/specimen/Open+Sans
2. **Montserrat**<br>
https://fonts.google.com/specimen/Montserrat
3. **Roboto**<br>
https://fonts.google.com/specimen/Roboto

**Czarny Font - #000000** <br>
**Tekst główny:** <br>87%<br>
**Tekst dodatkowy:** <br>54%<br>
**Tekst marginalny (icony, przypisy itp):** <br>38% <br>

**Biały Font - #FFFFFF** <br>
**Tekst główny:** <br>100% <br>
**Tekst dodatkowy:** <br>70% <br>
**Tekst marginalny (icony, przypisy itp):** <br>50% <br>


### 4. Pattern Library

<br>

- **Color Palette**


![alt text](https://i.ibb.co/zZwtv9h/color3.jpg "Logo Title Text 2")
![alt text](https://i.ibb.co/NC3WX61/paleta.jpg "Logo Title Text 2")


- Logotype
- Form Elements
- Card
- Typography
- Text Form
- Progress Bars
- Notification
- Navbar
<br>


### 5. Images and Icons
<br>


Wstępny pakiet icon.
https://www.flaticon.com/packs/material-design
<br>

### 6. X Podstawowych Heurystyk UX
<br>

**I Pokazuj status systemu.**<br>
Na bieżąco informuj użytkownika co się dzieje. Odpowiednia informacja zwrotna w rozsądnym czasie.

**II Zachowaj zgodność pomiędzy systemem a rzeczywistością.**<br>
Mów naturalnym językiem, prostym i zrozumiałym, trudne terminy tłumacz.

**III Daj użytkownikowi pełną kontrolę.**<br>
Zapewnij użytkownikowi możliwość cofnięcia, powtórzenia i przerwania wykonywanej akcji.

**IV Trzymaj się standardów i zachowaj spójność.**<br>
Pewne ikony, elementy układu są konieczne. Wyobraź sobie, że chcesz zmienić coś w ustawieniach, a nie masz menu :))

**V Zapobiegaj błędom.**<br>
Czytelny komunikat o błędzie jest dobry, ale jeszcze lepiej zapobiegać ich powstawaniu.

**VI Pozwalaj wybierać zamiast zmuszać do zapamiętywania.**<br>
Nie powinniśmy wymagać od użytkownika zapamiętywania funkcji elementów. Zamiast tego zawsze powinniśmy dostarczać niezbędne info. Użytkownik powinien dokonywać
wyboru spośród jasno opisanych opcji.

**VII Zapewnij elastyczność i efektywność.**<br>
Zastanów się nad system zaprojektowanym w dyskretne wspomaganie integracji, umożliwiającym użytkownikom przyśpieszenie często powtarzalnych operacji.

**VIII Dbaj o estetykę i umiar.**<br>
Pozbądź się tendencji do wyposażania okien dialogowych interfejsów  w elementy nie komunikujące adekwatne i potrzebne informacje. Odciąganie użytkownika uwagę od
 istotnego przekazu i pogarszają jego estetykę.

**IX Zapewnij skuteczną obsługę błędów.**<br>
Komunikaty o błędach wyróżniaj językiem naturalnym, bez zamieszczania kodów systemowych. Precyzyjnie wskazuj problem i sugeruj rozwiązanie.

**X Zadbaj o pomoc i dokumentację.**<br>
Najlepszy system to taki który nie potrzebuje dokumentacji. Rozbudowane FAQ jest dużo lepsze niż skąpe.


<br>

### 7. Links
<br>

https://www.figma.com/proto/05u1McJsghJUf77cQd2zzN5s/Epomis-Mobile?node-id=9%3A30&viewport=460%2C497%2C0.232108&scaling=min-zoom - Prototyp Epomis w Figmie<br><br>
https://elenx.net/blank/tabula/wikis/1_Development/2.-Opis-projektow - Opis projektu Epomis w Tabuli<br>
https://lawsofux.com - Kodeks UX<br>
http://uxmovement.com - Blog o UX<br>
https://uxplanet.org - Blog o UX 2<br>
http://nietylko.design/020-prosty-jezyk/ - podcast o prostym języku
<br><br>

### 8. TODO

- **Information Architecture**

Użytkownicy nie przychodzą oglądać obrazki i piękny projekt, tylko treści, konkrete informacji i otrzymać feedback. Naszą rolą jest przygotowanie treści, obrobić, zagospodarować, sformatować
teksty, żeby były łatwe do znalezienia, czytelne i po prostu przyjemne. UX writing jest cholernie ważne :). Nie możemy pracować na lorem ipsum. Powinniśmy opracować obraz do treści.
Brakuje dokumentu z całym układem treści Epomis. Zebranie i przeanalizowanie treści na podstawie
interview i researchu może okazać się czasochłonnym zadaniem.


To zadanie dla osoby która dobrze czuje się w ux writingu, pisaniu tekstów marketingowych.<br>

- **Images and Icons**

Potrzebujemy wstępny pakiet dobrych ikon. Na razie bardziej rozchodzi się o dobranie stylu niż rodzaju :)

- **Logotype**

Jedno z trudniejszych wyzwań projektowych.  Dobry logotyp to prosty logotyp, który łatwo zapamiętać, a taki nie jest prosto zaprojektować. Task na późniejszy okres, może więcej osób będzie chciało się "popisać" swoimi umiejętnościami :)

- **Interview**

Najbliższy z etapów kanału ui_ux. Interview z Epomis Dev jest pierwszym etapem w wykonaniu architektury informacji oraz jakichkolwiek badań ux. Z opisu Epomis (#links) możemy się dowiedzieć
mniej więcej jakie są plany wobec tego projektu. Jednak DEV od tego czasu zrobił kawał dobrej i sporej roboty. Musimy nadgonić i być na bierząco, nie tylko by zrobić adekwatny projekt dla już gotowych rozwiązań, ale też
by zrozumieć działanie Epomis.

- **Badania UX**

Przyczyny odinstalowania app.<br>
75% - brak zainteresowania<br>
69% - nie przydatność<br>
11% - wygląd<br><br>


![alt text](https://i.ibb.co/KzDkgSR/piramida.jpg  "Logo Title Text 2")



Podstawowym celem badań UX jest bowiem lepsze zrozumienie ludzi i kontekstu użycia danego produktu, przede wszystkim na podstawie obserwacji i faktów, a nie samej deklaracji użytkowników.
Drugim przeznaczeniem badań jest weryfikacja rozwiązań, tak aby projekt zderzyć z rzeczywistością jeszcze przed uruchomieniem całej machiny marketingowej, unikając tym upadek.
Jest to tak samo pracochłonny proces jak prototypowanie, dlatego często w firmach zespoły dzielą się na 3 sekcje - Badaczy UX , Projektantów, DEV. Jest to złożony proces, który wymaga też odpowiedniej próby badawczej.
Należy uświadomić sobie, iż projektowanie na pod. badań nie polega na ślepym wdrażaniu zachcianek użytkownika, ale szukaniu wzorców zachowań, przyczyn problemu, nisz do zagospodarowań.

- **Responsive web design**

Z tego co mi wiadomo, Epomis jest tylko aplikacją mobile. Jednak są różne wielkości telefonów. Przydałoby się trochę rozwinąć temat RWD.


- **Animation and Illustration**

Wzbogacają walory identyfikacji i doświadczenia użytkowników. Wywołują przyjemne integracje, pokazują status sytemu i mają jeszcze parę innych zastosowań.
Daleki temat, dla doświadczonych grafików.

- **Grid System**

Brak spójnych marginesów zaobserwujemy w momencie poruszania się między ekranami aplikacji. Trzeba znaleźć gotowy grid system, albo go opracować. Kolejny task :)
<br><br>