package epomis.mobile.notification

import epomis.mobile.model.Job
import io.reactivex.Flowable

interface NotificationService
{
    fun sendNotificationsFor(jobs: Flowable<Job>)
}