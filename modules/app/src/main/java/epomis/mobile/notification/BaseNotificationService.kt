package epomis.mobile.notification

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import epomis.mobile.EpomisMobileApplication
import epomis.mobile.R
import epomis.mobile.model.Job
import epomis.mobile.model.JobStatus
import epomis.utils.addOrReplace
import io.reactivex.Flowable

private const val FETCH_ADS_NOTIFICATION_ID = 42
private const val NOTIFICATION_CHANNEL_ID = "service_channel_id"
private const val NOTIFICATION_CHANNEL_NAME = "Job info"

class BaseNotificationService(
    private val context: Context
) : NotificationService
{
    private val notificationManager = NotificationManagerCompat.from(context)
    private val notificationBuilder = createNotificationBuilder()

    private fun createNotificationBuilder(): NotificationCompat.Builder = NotificationCompat
        .Builder(context, NOTIFICATION_CHANNEL_ID)
        .apply { configureNotificationBuilder(this) }

    private fun configureNotificationBuilder(notificationBuilder: NotificationCompat.Builder)
    {
        val title = context.getString(R.string.app_name)
        val intent = Intent(context, EpomisMobileApplication::class.java)
            .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK }
        val contentIntent = PendingIntent.getActivity(context, 0, intent, 0)

        notificationBuilder.setContentIntent(contentIntent)
        notificationBuilder.setContentTitle(title)
        notificationBuilder.setSmallIcon(R.drawable.notification_icon)
        notificationBuilder.setProgress(0, 0, false)
        notificationBuilder.setNotificationChannel(context)
        notificationBuilder.priority = NotificationCompat.PRIORITY_LOW
    }

    private fun NotificationCompat.Builder.setNotificationChannel(context: Context)
    {
        val networkManager = context.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager

        this.setChannelId(NOTIFICATION_CHANNEL_ID)
        networkManager.setNotificationChannelIfNecessary(
            NOTIFICATION_CHANNEL_ID,
            NOTIFICATION_CHANNEL_NAME,
            NotificationManager.IMPORTANCE_LOW
        )
    }

    @TargetApi(26)
    private fun NotificationManager.setNotificationChannelIfNecessary(id: String, channelName: String, importance: Int)
    {
        if (isNotificationChannelNeeded())
        {
            createNotificationChannel(NotificationChannel(id, channelName, importance))
        }
    }

    private fun isNotificationChannelNeeded() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

    override fun sendNotificationsFor(jobs: Flowable<Job>)
    {
        jobs.collect({ mutableListOf() }, this::collectJobs)
            .subscribe()
    }

    private fun collectJobs(jobAccumulator: MutableList<Job>, job: Job)
    {
        jobAccumulator.addOrReplace(job)

        val inProgressJobs = obtainInProgressJobs(jobAccumulator)
        jobAccumulator.clear()
        jobAccumulator.addAll(inProgressJobs)

        sendNotificationFor(jobAccumulator)
    }

    private fun obtainInProgressJobs(list: MutableList<Job>) = list
        .filter { it.status == JobStatus.IN_PROGRESS }

    private fun sendNotificationFor(jobAccumulator: MutableList<Job>) =
        if (jobAccumulator.isEmpty())
        {
            send("All jobs finished", false)
        } else
        {
            send("Jobs running: ${jobAccumulator.size}", true)
        }

    private fun send(content: String, showProgressBar: Boolean)
    {
        notificationBuilder.setContentText(content)
        setupProgressFor(showProgressBar)
        notificationManager.notify(FETCH_ADS_NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun setupProgressFor(showProgressBar: Boolean) =
        if (showProgressBar)
        {
            notificationBuilder.setProgress(1, 0, true)
        } else
        {
            notificationBuilder.setProgress(0, 0, false)
        }
}