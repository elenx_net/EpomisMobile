package epomis.mobile.notification

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val notificationModule = Kodein.Module("notificationService")
{
    bind<NotificationService>() with singleton { BaseNotificationService(instance()) }
}