package epomis.mobile.android.plugins

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.Disposable

interface DisposablePlugin : LifecycleOwner
{
    fun Disposable.disposeOnDestroy()
    {
        lifecycle.addObserver(OnDestroyListener { this.dispose() })
    }

    class OnDestroyListener(private val block: () -> Unit) : LifecycleObserver
    {
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() = block()
    }
}