package epomis.mobile.handler

import io.reactivex.plugins.RxJavaPlugins

import mu.KotlinLogging

private const val EXCEPTION_MESSAGE = "An uncaught exception has occurred"

private val logger = KotlinLogging.logger(RxJavaGlobalErrorHandler::class.java.name)

object RxJavaGlobalErrorHandler
{
    fun setup()
    {
        RxJavaPlugins.setErrorHandler { e ->
            logger.warn(EXCEPTION_MESSAGE, e)
        }
    }
}