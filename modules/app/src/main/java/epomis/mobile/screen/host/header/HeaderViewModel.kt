package epomis.mobile.screen.host.header

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import epomis.mobile.persistence.ad.AdStorageFacade

class HeaderViewModel(
    private val adStorageFacade: AdStorageFacade
) : ViewModel()
{
    val crawledAdsSize: LiveData<Int> by lazy { LiveDataReactiveStreams.fromPublisher(adStorageFacade.observeSize()) }
}