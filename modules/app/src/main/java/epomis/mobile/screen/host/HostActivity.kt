package epomis.mobile.screen.host

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.util.AttributeSet
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import epomis.mobile.R
import epomis.mobile.databinding.DrawerHeaderBinding
import epomis.mobile.databinding.HostActivityBinding
import epomis.mobile.persistence.ad.AdStorageFacade
import epomis.mobile.screen.host.header.HeaderViewModel
import epomis.mobile.screen.host.header.HeaderViewModelFactory
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance

const val EMAIL_FOR_FEEDBACK = "elenx.net@gmail.com"
const val EMAIL_CONTENT_TYPE_MIME = "message/rfc822"
const val GITLAB_ADDRESS = "https://gitlab.com/elenx_net"

class HostActivity : AppCompatActivity(), KodeinAware
{
    override val kodein by closestKodein()
    private lateinit var viewModel: HeaderViewModel
    private val adStorageFacade: AdStorageFacade by instance()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setupBindings()

        val navController = findNavController(R.id.navHost)
        val appBarConfiguration = AppBarConfiguration(navController.graph, findViewById(R.id.app_drawer))

        findViewById<Toolbar>(R.id.toolbar)
            .setupWithNavController(navController, appBarConfiguration)

        setupActionBarForegroundColor()

        val navMenu = findViewById<NavigationView>(R.id.navigationView)
        setupDrawer(navMenu)
    }

    private fun setupActionBarForegroundColor()
    {
        findViewById<Toolbar>(R.id.toolbar)
            .navigationIcon?.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
    }

    private fun setupBindings()
    {
        val hostActivityBinding = DataBindingUtil.setContentView<HostActivityBinding>(this, R.layout.host_activity)
        val drawerHeaderBinding = DrawerHeaderBinding.bind(hostActivityBinding.navigationView.getHeaderView(0))
        drawerHeaderBinding.viewModel = viewModel
        drawerHeaderBinding.lifecycleOwner = this
        setContentView(hostActivityBinding.root)
    }

    private fun setupDrawer(navMenu: NavigationView)
    {
        val drawerLayout = findViewById<DrawerLayout>(R.id.app_drawer)
        navMenu.setNavigationItemSelectedListener {
            drawerLayout.closeDrawers()
            this.onOptionsItemSelected(it)
        }

        setupDrawerFooter()
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View?
    {
        viewModel = ViewModelProviders.of(this, HeaderViewModelFactory(adStorageFacade))
            .get(HeaderViewModel::class.java)
        return super.onCreateView(name, context, attrs)
    }

    private fun setupDrawerFooter()
    {
        val footerTextView = findViewById<TextView>(R.id.drawerFooterText)
        footerTextView.setOnClickListener {
            val uriGitLab = GITLAB_ADDRESS.toUri()
            val openGitLabIntent = Intent(Intent.ACTION_VIEW, uriGitLab)
            startActivity(openGitLabIntent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId)
        {
            R.id.navSchedules ->
            {
                displaySnackbarWithMessage(getString(R.string.scheduling))
                false
            }
            R.id.navUserData ->
            {
                findNavController(R.id.navHost).navigate(R.id.userDataScreen)
                false
            }
            R.id.navFeedback ->
            {
                sendFeedbackEmail()
                false
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun displaySnackbarWithMessage(message: String) =

        Snackbar.make(
            findViewById(R.id.app_drawer),
            message,
            Snackbar.LENGTH_SHORT
        ).show()

    private fun sendFeedbackEmail()
    {
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent
            .setType(EMAIL_CONTENT_TYPE_MIME)
            .setData(Uri.parse("mailto:${EMAIL_FOR_FEEDBACK}"))
        startActivity(emailIntent)
    }

    companion object
    {
        fun start(context: Context) = context.startActivity(Intent(context, HostActivity::class.java))
    }
}