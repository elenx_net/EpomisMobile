package epomis.mobile.screen.filters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import epomis.mobile.R
import epomis.mobile.model.AdFilter
import epomis.utils.nullIfBlank

class FiltersScreenAdapter(
    private val dataset: List<AdFilter>,
    private val onClickCallback: (AdFilter) -> Unit
) : RecyclerView.Adapter<FiltersScreenAdapter.ViewHolder>()
{
    var tracker: SelectionTracker<Long>? = null

   init
    {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = LayoutInflater
        .from(parent.context)
        .inflate(R.layout.filters_filter, parent, false)
        .let { ViewHolder(it) }

    override fun getItemCount(): Int = dataset.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val item = dataset[position]

        setupTextValueOf(holder.keyword, item.keyword)
        setupTextValueOf(holder.location, item.location)

        holder.rootView.setOnLongClickListener { true }
        holder.rootView.setOnClickListener { onClickCallback(item) }

        tracker?.let {
            holder.bind(it.isSelected(position.toLong()))
        }
    }

    private fun setupTextValueOf(textView: TextView, value: String?)
    {
        value.nullIfBlank()
            ?.let { textView.text = it }
            ?: run {
                textView.text = textView.context.getText(R.string.filtersScreenEmptyText)
                textView.alpha = 0.3f
            }
    }

    class ViewHolder(val rootView: View) : RecyclerView.ViewHolder(rootView)
    {
        val keyword = rootView.findViewById<TextView>(R.id.filtersItemKeyword)
        val location = rootView.findViewById<TextView>(R.id.filtersItemLocation)
        fun computeFilterDetails(): ItemDetailsLookup.ItemDetails<Long> = FilterDetails()

        inner class FilterDetails : ItemDetailsLookup.ItemDetails<Long>()
        {
            override fun getPosition(): Int = adapterPosition
            override fun getSelectionKey(): Long? = itemId
        }

        fun bind(isActivated: Boolean = false)
        {
            itemView.isActivated = isActivated
        }
    }

}
