package epomis.mobile.screen.dashboard

import epomis.mobile.model.JobType

interface DashboardNavigator {
    fun goToFilters(jobType: JobType)
}