package epomis.mobile.screen.userdata

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val userDataScreenModule = Kodein.Module("userDataScreen") {
    bind<UserDataScreenViewModel>() with singleton { UserDataScreenViewModel(instance()) }
}