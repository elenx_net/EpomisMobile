package epomis.mobile.screen.filters

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StableIdKeyProvider
import androidx.recyclerview.selection.StorageStrategy
import epomis.mobile.R
import epomis.mobile.job.JobFacade
import epomis.mobile.model.AdFilter
import epomis.mobile.model.JobType
import kotlinx.android.synthetic.main.filters.filterList
import kotlinx.android.synthetic.main.filters.goToAddFilterButton
import org.kodein.di.KodeinAware
import org.kodein.di.android.support.closestKodein
import org.kodein.di.generic.instance

private const val SELECTION_ID = "filtersSelection"

class FiltersScreen : Fragment(), KodeinAware
{
    private val filters = mutableListOf<AdFilter>()

    override val kodein by closestKodein()
    private val jobFacade: JobFacade by instance()
    private val viewModel: FiltersScreenViewModel by instance()
    private val filtersScreenAdapter = FiltersScreenAdapter(filters, this::onFilterClick)
    private lateinit var tracker: SelectionTracker<Long>
    private var actionMode: ActionMode? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        viewModel.filters.observe(viewLifecycleOwner) {
            filters.clear()
            filters.addAll(it)
            filtersScreenAdapter.notifyDataSetChanged()
        }

        return inflater.inflate(R.layout.filters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        goToAddFilterButton.setOnClickListener { goToAddNewFilterScreen() }
        filterList.adapter = filtersScreenAdapter
        viewModel.refreshFilters()
        setupTracker()
    }

    private fun setupTracker()
    {
        tracker = setupTrackerBuilder()
            .withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()
            .also { it.addObserver(LongSelectionObserver()) }

        filtersScreenAdapter.tracker = tracker
    }

    private fun setupTrackerBuilder(): SelectionTracker.Builder<Long> = SelectionTracker
        .Builder(
            SELECTION_ID,
            filterList,
            StableIdKeyProvider(filterList),
            FilterItemDetailsLookup(filterList),
            StorageStrategy.createLongStorage()
        )

    override fun onDestroy()
    {
        super.onDestroy()
        actionMode?.finish()
    }

    private fun onFilterClick(adFilter: AdFilter)
    {
        val jobType = (arguments?.get("taskType") ?: TODO()) as JobType

        jobFacade.startCrawling(adFilter, jobType)
        goToRecentActivitiesScreen()
    }

    private fun goToRecentActivitiesScreen()
    {
        findNavController().popBackStack(R.id.recentJobs, false)
    }

    private fun goToAddNewFilterScreen()
    {
        findNavController().navigate(R.id.addFilterScreen)
    }

    inner class LongSelectionObserver : SelectionTracker.SelectionObserver<Long>()
    {
        override fun onSelectionChanged()
        {
            super.onSelectionChanged()
            when (tracker.selection.size())
            {
                0 -> actionMode?.finish()
                1 -> actionMode = actionMode ?: requireActivity().startActionMode(SelectionCallback())
                else -> Unit
            }
        }
    }

    inner class SelectionCallback : ActionMode.Callback
    {
        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean
        {
            if (item?.itemId == R.id.action_remove)
            {
                showAlertDialog(tracker.selection.toList())
                actionMode?.finish()
            }
            return false
        }

        private fun showAlertDialog(selection: List<Long>) = AlertDialog
            .Builder(requireContext())
            .setMessage(R.string.dialog_remove_filters)
            .setPositiveButton(R.string.confirm) { _, _ -> viewModel.removeFilters(selection) }
            .setNegativeButton(R.string.cancel) { _, _ -> tracker.clearSelection() }
            .show()

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean
        {
            mode?.menuInflater?.inflate(R.menu.remove_menu, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean = false

        override fun onDestroyActionMode(mode: ActionMode?)
        {
            tracker.clearSelection()
            actionMode = null
        }
    }
}