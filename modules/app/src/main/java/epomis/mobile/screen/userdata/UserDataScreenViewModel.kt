package epomis.mobile.screen.userdata

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import epomis.mobile.persistence.userdata.UserDataFacade
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class UserDataScreenViewModel(
    private val userDataFacade: UserDataFacade
) : ViewModel()
{
    private var findUserDataDisposable: Disposable? = null

    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val resumeFilename = MutableLiveData<String>()
    val resumeContent = MutableLiveData<ByteArray>()

    val updateNotifier = MediatorLiveData<Unit>()
        .apply {
            addSource(firstName) { this.postValue(Unit) }
            addSource(lastName) { this.postValue(Unit) }
            addSource(email) { this.postValue(Unit) }
            addSource(resumeFilename) { this.postValue(Unit) }
        }

    init
    {
        findUserDataDisposable?.dispose()
        findUserDataDisposable = userDataFacade
            .maybeFindUserData()
            .subscribeOn(Schedulers.io())
            .subscribe { userData ->
                firstName.postValue(userData.firstName)
                lastName.postValue(userData.lastName)
                email.postValue(userData.email)
                resumeFilename.postValue(userData.resumeFilename)
                resumeContent.postValue(userData.resumeContent)
            }
    }

    override fun onCleared()
    {
        findUserDataDisposable?.dispose()
    }

    fun saveUserData(): Completable = userDataFacade
        .saveUserData(
            firstName.value!!,
            lastName.value!!,
            email.value!!,
            resumeFilename.value!!,
            resumeContent.value!!
        ).subscribeOn(Schedulers.io())
}