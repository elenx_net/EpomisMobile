package epomis.mobile.screen.filters.domain

import epomis.mobile.model.AdFilter

interface FiltersRepository
{
    fun getFilters(): List<AdFilter>
    fun addFilter(adFilter: AdFilter)
    fun removeFilters(adFilters: List<AdFilter>)
}