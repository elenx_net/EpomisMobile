package epomis.mobile.screen.filters

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.widget.RecyclerView

class FilterItemDetailsLookup(
    private val recyclerView: RecyclerView
) : ItemDetailsLookup<Long>()
{
    override fun getItemDetails(event: MotionEvent): ItemDetails<Long>? = recyclerView
        .findChildViewUnder(event.x, event.y)
        ?.let {
            (recyclerView.getChildViewHolder(it) as FiltersScreenAdapter.ViewHolder)
                .computeFilterDetails()
        }
}