package epomis.mobile.screen.recentjobs

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val recentActivitiesScreenModule = Kodein.Module("recentActivities") {
    bind<RecentJobsScreenViewModel>() with singleton { RecentJobsScreenViewModel(instance(), instance()) }
}