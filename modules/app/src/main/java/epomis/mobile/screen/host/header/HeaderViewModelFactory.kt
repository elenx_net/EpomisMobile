package epomis.mobile.screen.host.header

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import epomis.mobile.persistence.ad.AdStorageFacade

class HeaderViewModelFactory(
    private val adStorageFacade: AdStorageFacade
) : ViewModelProvider.Factory
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return if (modelClass.isAssignableFrom(HeaderViewModel::class.java))
        {
            HeaderViewModel(adStorageFacade) as T
        } else
        {
            throw IllegalArgumentException("ViewModel Not Found!")
        }
    }
}