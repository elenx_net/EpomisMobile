package epomis.mobile.screen.recentjobs

import android.os.Bundle
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StableIdKeyProvider
import androidx.recyclerview.selection.StorageStrategy
import epomis.mobile.R
import epomis.mobile.job.JobFacade
import epomis.mobile.model.Job
import epomis.mobile.model.JobStatus
import epomis.utils.ifNotEmpty
import kotlinx.android.synthetic.main.recent_jobs.createTaskBtn
import kotlinx.android.synthetic.main.recent_jobs.noRecentJobsPlaceholder
import kotlinx.android.synthetic.main.recent_jobs.recentJobsList
import org.kodein.di.KodeinAware
import org.kodein.di.android.support.closestKodein
import org.kodein.di.generic.instance

private const val SELECTION_ID = "jobsSelection"

class RecentJobsScreen : Fragment(), RecentJobsNavigator, KodeinAware
{
    private val recentActivities = mutableListOf<Job>()

    override val kodein by closestKodein()
    private val viewModel: RecentJobsScreenViewModel by instance()
    private val jobFacade: JobFacade by instance()
    private val adapter = RecentJobsScreenAdapter(recentActivities, this::onJobClick)
    private lateinit var tracker: SelectionTracker<Long>
    private var actionMode: ActionMode? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        viewModel.jobsLiveData.observe(viewLifecycleOwner) {
            it.ifNotEmpty { noRecentJobsPlaceholder.visibility = View.GONE }
            recentActivities.clear()
            recentActivities.addAll(it)
            adapter.notifyDataSetChanged()
        }
        return inflater.inflate(R.layout.recent_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        recentJobsList.adapter = adapter
        createTaskBtn.setOnClickListener { createTaskButtonClicked() }
        setupTracker()
    }

    private fun setupTracker()
    {
        tracker = setupTrackerBuilder()
            .withSelectionPredicate(SelectionPredicate())
            .build()
            .also { it.addObserver(LongSelectionObserver()) }

        adapter.tracker = tracker
    }

    private fun setupTrackerBuilder(): SelectionTracker.Builder<Long> = SelectionTracker
        .Builder(
            SELECTION_ID,
            recentJobsList,
            StableIdKeyProvider(recentJobsList),
            RecentJobItemDetailsLookup(recentJobsList),
            StorageStrategy.createLongStorage()
        )

    private fun createTaskButtonClicked()
    {
        viewModel.isUserDataFilled
            .subscribe { isDataFilled ->
                if (isDataFilled)
                {
                    goToDashboard()
                } else
                {
                    goToUserData()
                }
            }
    }

    override fun onDestroy()
    {
        super.onDestroy()
        actionMode?.finish()
    }

    private fun onJobClick(job: Job) = when (job.status)
    {
        JobStatus.SCHEDULED -> applyForJob(job)
        else -> goToAdsList(job.id!!)
    }

    private fun applyForJob(job: Job)
    {
        AlertDialog.Builder(requireContext())
            .setMessage(R.string.applyForAllAdsAlert)
            .setPositiveButton("APPLY") { _, _ -> jobFacade.startApplying(job) }
            .setNegativeButton("CANCEL") { _, _ -> }
            .show()
    }

    override fun goToDashboard()
    {
        findNavController().navigate(R.id.dashboard)
    }

    override fun goToAdsList(jobId: Long)
    {
        findNavController().navigate(R.id.adsListScreen, bundleOf("jobId" to jobId))
    }

    override fun goToUserData()
    {
        findNavController().navigate(R.id.userDataScreen)
    }

    inner class LongSelectionObserver : SelectionTracker.SelectionObserver<Long>()
    {
        override fun onSelectionChanged()
        {
            super.onSelectionChanged()
            when (tracker.selection.size())
            {
                0 -> actionMode?.finish()
                1 -> actionMode = actionMode ?: requireActivity().startActionMode(SelectionCallback())
                else -> Unit
            }
        }
    }

    inner class SelectionPredicate : SelectionTracker.SelectionPredicate<Long>()
    {
        override fun canSetStateForKey(key: Long, nextState: Boolean): Boolean
        {
            return true
        }

        override fun canSetStateAtPosition(position: Int, nextState: Boolean): Boolean
        {
            return recentActivities[position].status != JobStatus.IN_PROGRESS
        }

        override fun canSelectMultiple(): Boolean
        {
            return true
        }
    }

    inner class SelectionCallback : ActionMode.Callback
    {
        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean
        {
            if (item?.itemId == R.id.action_remove)
            {
                showAlertDialog(tracker.selection.toList())
                actionMode?.finish()
            }
            return false
        }

        private fun showAlertDialog(selection: List<Long>) = AlertDialog
            .Builder(requireContext())
            .setMessage(R.string.dialog_remove_tasks)
            .setPositiveButton(R.string.confirm) { _, _ ->
                jobFacade.deleteTasks(selection.map {
                    recentActivities[it.toInt()]
                })
            }
            .setNegativeButton(R.string.cancel) { _, _ -> tracker.clearSelection() }
            .show()

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean
        {
            mode?.menuInflater?.inflate(R.menu.remove_menu, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean = false

        override fun onDestroyActionMode(mode: ActionMode?)
        {
            tracker.clearSelection()
            actionMode = null
        }
    }
}