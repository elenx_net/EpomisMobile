package epomis.mobile.screen.filters.domain

import android.content.SharedPreferences
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import epomis.mobile.model.AdFilter

private const val FILTERS_KEY = "FILTERS_LIST"

class PreferencesFiltersRepository(
    private val manager: SharedPreferences,
    private val objectMapper: ObjectMapper
) : FiltersRepository
{
    private val data: MutableList<AdFilter> by lazy {
        manager.getString(FILTERS_KEY, null)?.let {
            objectMapper.readValue<MutableList<AdFilter>>(it)
        } ?: mutableListOf(
            AdFilter("Java", "Warszawa"),
            AdFilter("Kotlin", "Gdańsk"),
            AdFilter("Kotlin", "Poznań"),
            AdFilter("Javascript", "Kraków")
        )
    }

    override fun getFilters(): List<AdFilter> = data

    override fun addFilter(adFilter: AdFilter)
    {
        if (!data.contains(adFilter))
        {
            data.add(adFilter)
            saveData()
        }
    }

    override fun removeFilters(adFilters: List<AdFilter>)
    {
        data.removeAll(adFilters)
        saveData()
    }

    private fun saveData()
    {
        val json = objectMapper.writeValueAsString(data)

        manager
            .edit()
            .putString(FILTERS_KEY, json)
            .apply()
    }
}
