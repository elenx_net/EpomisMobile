package epomis.mobile.screen.adslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.google.android.material.snackbar.Snackbar
import epomis.mobile.R
import epomis.mobile.job.JobFacade
import epomis.mobile.model.Ad
import epomis.mobile.model.ApplicationResult
import kotlinx.android.synthetic.main.ads.adsList
import org.kodein.di.KodeinAware
import org.kodein.di.android.support.closestKodein
import org.kodein.di.generic.instance

class AdsListScreen : Fragment(), KodeinAware
{
    private val entries = mutableListOf<Ad>()

    private var jobId: Long = 0

    override val kodein by closestKodein()
    private val viewModel: AdsListScreenViewModel by instance()
    private val jobFacade: JobFacade by instance()
    private val adapter = AdsListScreenAdapter(entries, this::onAdClick)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        jobId = arguments?.getLong("jobId") ?: TODO()
        viewModel.refreshAds(jobId)
        return inflater.inflate(R.layout.ads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        adsList.adapter = adapter

        viewModel.adsLiveData?.observe(viewLifecycleOwner) {
            entries.clear()
            entries.addAll(it)
            adapter.notifyDataSetChanged()
        }
    }

    private fun onAdClick(ad: Ad)
    {
        AlertDialog.Builder(requireContext())
            .setMessage("Apply for ${ad.title}?")
            .setPositiveButton("APPLY") { _, _ ->
                jobFacade.applyFor(ad)
                    .doOnSuccess { applySnackbarStatus(result = it.latestApplication!!, ad = ad) }
                    .subscribe()
            }
            .setNegativeButton("CANCEL") { _, _ -> }
            .show()
    }

    private fun applySnackbarStatus(result: ApplicationResult, ad: Ad)
    {
        when (result.success)
        {
            true -> Snackbar.make(
                requireView(),
                "Successfully applied for ${ad.title}",
                Snackbar.LENGTH_SHORT
            ).show()

            false -> Snackbar.make(
                requireView(),
                "Could not apply for ${ad.title}",
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }
}