package epomis.mobile.screen.recentjobs

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import epomis.mobile.model.Job
import epomis.mobile.persistence.job.JobStorageFacade
import epomis.mobile.persistence.userdata.UserDataFacade
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RecentJobsScreenViewModel(
    private val jobStorageFacade: JobStorageFacade,
    private val userDataFacade: UserDataFacade
) : ViewModel()
{
    val jobsLiveData: LiveData<List<Job>> by lazy {
        LiveDataReactiveStreams.fromPublisher(jobStorageFacade.observeAll())
    }

    val isUserDataFilled = userDataFacade.isUserDataPresent()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}