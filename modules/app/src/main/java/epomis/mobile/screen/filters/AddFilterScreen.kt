package epomis.mobile.screen.filters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import epomis.mobile.R
import epomis.mobile.model.AdFilter
import kotlinx.android.synthetic.main.filters_add.confirmAddFilterButton
import kotlinx.android.synthetic.main.filters_add.keywordInputEditText
import kotlinx.android.synthetic.main.filters_add.locationInputEditText
import org.kodein.di.KodeinAware
import org.kodein.di.android.support.closestKodein
import org.kodein.di.generic.instance

class AddFilterScreen : Fragment(), KodeinAware
{
    override val kodein by closestKodein()
    private val viewModel: FiltersScreenViewModel by instance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.filters_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        confirmAddFilterButton.setOnClickListener { addFilter() }
    }

    private fun addFilter()
    {
        val filter = AdFilter(
            keywordInputEditText.text.toString(),
            locationInputEditText.text.toString()
        )
        viewModel.addFilter(filter)

        findNavController().navigateUp()
    }
}
