package epomis.mobile.screen.filters

import epomis.mobile.screen.filters.domain.PreferencesFiltersRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val filtersScreenModule = Kodein.Module("filters") {

    bind<FiltersScreenViewModel>() with singleton {
        val filtersRepository = PreferencesFiltersRepository(instance(), instance())
        FiltersScreenViewModel(filtersRepository)
    }
}