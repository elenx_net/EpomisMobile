package epomis.mobile.screen.adslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import epomis.mobile.model.Ad
import epomis.mobile.persistence.ad.AdStorageFacade

class AdsListScreenViewModel(
    private val adEntryStorageFacade: AdStorageFacade
) : ViewModel()
{
    var adsLiveData: LiveData<List<Ad>>? = null

    fun refreshAds(jobId: Long)
    {
        adsLiveData = LiveDataReactiveStreams.fromPublisher(adEntryStorageFacade.observeAllByJobId(jobId))
    }

    override fun onCleared()
    {
        adsLiveData = null
    }
}