package epomis.mobile.screen.adslist

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val adsListScreenModule = Kodein.Module("adsList") {
    bind<AdsListScreenViewModel>() with singleton { AdsListScreenViewModel(instance()) }
}