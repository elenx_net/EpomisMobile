package epomis.mobile.screen.userdata

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore

internal const val PICK_RESUME_CODE = 0

private const val LOAD_FILE_CODE = 42

class ResumePicker : Activity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        startActivityForResult(createPickFileIntent(), LOAD_FILE_CODE)
    }

    private fun createPickFileIntent() = Intent(Intent.ACTION_OPEN_DOCUMENT)
        .apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "application/pdf"
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        val returnedData = Intent()

        if (requestCode == LOAD_FILE_CODE && resultCode == RESULT_OK)
        {
            data?.data?.let { putDataFromUri(returnedData, it) }
        }

        setResult(PICK_RESUME_CODE, returnedData)
        finish()
    }

    private fun putDataFromUri(returnedData: Intent, it: Uri): Intent?
    {
        returnedData.putExtra("resumeContent", contentResolver?.openInputStream(it)?.readBytes())

        val projection = arrayOf(MediaStore.MediaColumns.DISPLAY_NAME)
        return contentResolver
            ?.query(it, projection, null, null, null)
            ?.use { metaCursor ->
                metaCursor.moveToFirst()
                returnedData.putExtra("resumeFilename", metaCursor.getString(0))
            }
    }
}
