package epomis.mobile.screen.recentjobs

interface RecentJobsNavigator
{
    fun goToDashboard()
    fun goToAdsList(jobId: Long)
    fun goToUserData()
}