package epomis.mobile.screen.recentjobs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import epomis.mobile.R
import epomis.mobile.model.Job
import epomis.mobile.model.JobStatus
import kotlinx.android.synthetic.main.recent_jobs_item.view.decorator
import org.joda.time.LocalDateTime
import java.util.Locale

class RecentJobsScreenAdapter(
    val dataset: List<Job>,
    val onClickCallback: (Job) -> Unit
) : RecyclerView.Adapter<RecentJobsScreenAdapter.ViewHolder>()
{
    var tracker: SelectionTracker<Long>? = null
    private lateinit var context: Context

    init
    {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.recent_jobs_item, parent, false)

        context = itemView.context

        return ViewHolder(itemView)

    }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val item = dataset[position]

        val progress = item.status.name

        holder.title.text = item.type.simpleName
        holder.status.text = progress
        holder.started.text = LocalDateTime(item.started).toString("HH:mm")
        holder.filter.text = obtainFilterText(item)
        updateJobStatusColor(holder.rootView, item.status)
        holder.rootView.setOnClickListener {
            onClickCallback(item)
        }
        holder.rootView.setOnLongClickListener { true }
        tracker?.let {
            holder.bind(it.isSelected(position.toLong()))
        }
    }

    private fun obtainFilterText(item: Job): String
    {
        val keyword = item.adFilter.keyword
        val location = item.adFilter.location

        return when
        {
            keyword.isNotBlank() && location.isNotBlank() -> "${toUpperCaseField(keyword)}, ${toUpperCaseField(location)}"
            keyword.isNotBlank() && location.isBlank() -> toUpperCaseField(keyword)
            keyword.isBlank() && location.isNotBlank() -> toUpperCaseField(location)
            else -> context.getString(R.string.filtersScreenEmptyText)
        }
    }

    private fun toUpperCaseField(field: String) = field.toUpperCase(Locale.getDefault())

    class ViewHolder(val rootView: View) : RecyclerView.ViewHolder(rootView)
    {
        val title = rootView.findViewById<TextView>(R.id.jobName)
        val status = rootView.findViewById<TextView>(R.id.statusValue)
        val started = rootView.findViewById<TextView>(R.id.startedValue)
        val filter = rootView.findViewById<TextView>(R.id.filterValue)

        fun computeRecentJobDetails(): ItemDetailsLookup.ItemDetails<Long> = RecentJobDetails()

        inner class RecentJobDetails : ItemDetailsLookup.ItemDetails<Long>()
        {
            override fun getPosition(): Int = adapterPosition
            override fun getSelectionKey(): Long? = itemId
        }

        fun bind(isActivated: Boolean = false)
        {
            itemView.isActivated = isActivated
        }
    }

    private fun updateJobStatusColor(holder: View, status: JobStatus)
    {
        when (status)
        {
            JobStatus.SUCCESSFUL -> appointTintColor(holder, R.color.job_complete)
            JobStatus.IN_PROGRESS -> appointTintColor(holder, R.color.job_in_progress)
            JobStatus.SCHEDULED -> appointTintColor(holder, R.color.job_scheduled)
            else -> appointTintColor(holder, R.color.job_failed)
        }
    }

    private fun appointTintColor(holder: View, color: Int) =
        ImageViewCompat.setImageTintList(holder.decorator, AppCompatResources.getColorStateList(context, color))
}
