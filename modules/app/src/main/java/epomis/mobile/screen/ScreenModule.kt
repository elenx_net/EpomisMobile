package epomis.mobile.screen

import epomis.mobile.screen.adslist.adsListScreenModule
import epomis.mobile.screen.filters.filtersScreenModule
import epomis.mobile.screen.recentjobs.recentActivitiesScreenModule
import epomis.mobile.screen.userdata.userDataScreenModule
import org.kodein.di.Kodein

val screenModule = Kodein.Module("screen")
{
    importOnce(filtersScreenModule, allowOverride = true)
    importOnce(recentActivitiesScreenModule, allowOverride = true)
    importOnce(adsListScreenModule, allowOverride = true)
    importOnce(userDataScreenModule, allowOverride = true)
}