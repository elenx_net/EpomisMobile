package epomis.mobile.screen.userdata

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import epomis.mobile.persistence.userdata.UserDataFacade

class UserDataScreenViewModelFactory(private val userDataFacade: UserDataFacade) : ViewModelProvider.Factory
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        require(modelClass.isAssignableFrom(UserDataScreenViewModel::class.java)) {
            "ViewModel not found"
        }

        return UserDataScreenViewModel(this.userDataFacade) as T
    }
}