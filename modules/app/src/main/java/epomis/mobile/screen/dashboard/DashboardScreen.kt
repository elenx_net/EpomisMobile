package epomis.mobile.screen.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import epomis.mobile.R
import epomis.mobile.model.JobType
import kotlinx.android.synthetic.main.dashboard.tasksApplyingContainer
import kotlinx.android.synthetic.main.dashboard.tasksCrawlingContainer
import org.kodein.di.KodeinAware
import org.kodein.di.android.support.closestKodein

class DashboardScreen : Fragment(), DashboardNavigator, KodeinAware
{
    override val kodein by closestKodein()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        tasksCrawlingContainer.setOnClickListener { goToFilters(JobType.CRAWLING) }
        tasksApplyingContainer.setOnClickListener { goToFilters(JobType.APPLYING) }
    }

    override fun goToFilters(jobType: JobType)
    {
        findNavController().navigate(R.id.selectFilterScreen, bundleOf("taskType" to jobType))
    }
}