package epomis.mobile.screen.adslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import epomis.mobile.R
import epomis.mobile.databinding.AdsItemBinding
import epomis.mobile.model.Ad

class AdsListScreenAdapter(
    val dataset: List<Ad>, val onClickCallback: (Ad) -> Unit
) : RecyclerView.Adapter<AdsListScreenAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val itemView = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<AdsItemBinding>(itemView, R.layout.ads_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(dataset[position], onClickCallback)

    class ViewHolder(val binding: AdsItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind(item: Ad, onClickCallback: (Ad) -> Unit)
        {
            binding.ad = item
            itemView.setOnClickListener {
                binding.expandIndicator.scaleY *=-1
                binding.expandableLayout.visibility =
                    if (binding.expandableLayout.isVisible) View.GONE else View.VISIBLE
            }
            binding.adsButton.setOnClickListener { onClickCallback(item) }
        }
    }

}