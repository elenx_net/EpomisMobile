package epomis.mobile.screen.filters

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import epomis.mobile.model.AdFilter
import epomis.mobile.screen.filters.domain.FiltersRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FiltersScreenViewModel(
    private val filtersRepository: FiltersRepository
) : ViewModel()
{
    private val filtersLiveData = MutableLiveData<List<AdFilter>>()
    val filters: LiveData<List<AdFilter>> get() = filtersLiveData

    fun refreshFilters()
    {
        viewModelScope.launch(Dispatchers.IO) {
            filtersLiveData.postValue(filtersRepository.getFilters())
        }
    }

    fun addFilter(adFilter: AdFilter)
    {
        filtersRepository.addFilter(adFilter)
        refreshFilters()
    }

    fun removeFilters(filters: List<Long>) = filters
        .map { filtersLiveData.value!![it.toInt()] }
        .let { filtersRepository.removeFilters(it) }
        .also { refreshFilters() }
}