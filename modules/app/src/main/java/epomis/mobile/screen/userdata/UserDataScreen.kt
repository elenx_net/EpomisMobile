package epomis.mobile.screen.userdata

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.trimmedLength
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import epomis.mobile.android.plugins.DisposablePlugin
import epomis.mobile.databinding.UserDataBinding
import epomis.mobile.persistence.userdata.UserDataFacade
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.user_data.emailInputLayout
import kotlinx.android.synthetic.main.user_data.firstNameInputLayout
import kotlinx.android.synthetic.main.user_data.lastNameInputLayout
import kotlinx.android.synthetic.main.user_data.pickCvButton
import kotlinx.android.synthetic.main.user_data.setDataButton
import org.kodein.di.KodeinAware
import org.kodein.di.android.support.closestKodein
import org.kodein.di.generic.instance

class UserDataScreen : Fragment(), KodeinAware, DisposablePlugin
{
    override val kodein by closestKodein()
    private lateinit var viewModel: UserDataScreenViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UserDataBinding
            .inflate(inflater)
            .apply {
                this.viewModel = this@UserDataScreen.viewModel
                this.lifecycleOwner = this@UserDataScreen
            }.root

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        val userDataFacade: UserDataFacade by instance()
        viewModel = ViewModelProviders.of(this, UserDataScreenViewModelFactory(userDataFacade))
            .get(UserDataScreenViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        pickCvButton.setOnClickListener { pickFileFromStorage() }
        setDataButton.setOnClickListener { onSetDataButtonClick() }
        attachInputValidators()
    }

    private fun pickFileFromStorage()
    {
        startActivityForResult(Intent(context, ResumePicker::class.java), PICK_RESUME_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        val filename = data?.getStringExtra("resumeFilename")

        if (filename != null)
        {
            viewModel.resumeFilename.postValue(filename)
            viewModel.resumeContent.postValue(data.getByteArrayExtra("resumeContent"))
        }
    }

    private fun isNameValid(name: String) = name.trimmedLength() > 3

    private fun isEmailValid(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun areInputsValid() = !viewModel.resumeFilename.value.isNullOrBlank()
            && isNameValid(viewModel.firstName.value!!)
            && isNameValid(viewModel.lastName.value!!)
            && isEmailValid(viewModel.email.value!!)

    private fun attachInputValidators()
    {
        viewModel.updateNotifier.observe(viewLifecycleOwner) {
            setDataButton.alpha = if (areInputsValid()) 1f else .5f
        }

        viewModel.firstName.observe(viewLifecycleOwner) { name ->
            firstNameInputLayout.error = "too short".takeIf { !isNameValid(name) }
        }
        viewModel.lastName.observe(viewLifecycleOwner) { name ->
            lastNameInputLayout.error = "too short".takeIf { !isNameValid(name) }
        }
        viewModel.email.observe(viewLifecycleOwner) { email ->
            emailInputLayout.error = "invalid format".takeIf { !isEmailValid(email) }
        }
    }

    private fun onSetDataButtonClick()
    {
        if (areInputsValid())
        {
            viewModel.saveUserData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSetUserDataSuccessful, this::onSetUserDataError)
                .disposeOnDestroy()
        }
    }

    private fun onSetUserDataError(cause: Throwable)
    {
        Snackbar
            .make(requireView(), "Couldn't set data, please try again", Snackbar.LENGTH_SHORT)
            .show()
    }

    private fun onSetUserDataSuccessful()
    {
        Snackbar
            .make(requireView(), "Data successfully set", Snackbar.LENGTH_SHORT)
            .show()
        goToDashboardScreen()
    }

    private fun goToDashboardScreen()
    {
        findNavController().popBackStack()
    }
}