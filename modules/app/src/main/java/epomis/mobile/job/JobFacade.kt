package epomis.mobile.job

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter
import epomis.mobile.model.Job
import epomis.mobile.model.JobType
import io.reactivex.Single

interface JobFacade
{
    fun startCrawling(adFilter: AdFilter, jobType: JobType = JobType.CRAWLING)
    fun deleteTasks(jobs: List<Job>)
    fun startApplying(job: Job)
    fun applyFor(ad: Ad): Single<Ad>
}