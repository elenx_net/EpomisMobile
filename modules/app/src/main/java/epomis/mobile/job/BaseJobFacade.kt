package epomis.mobile.job

import android.app.Service
import android.content.Intent
import android.os.IBinder
import epomis.mobile.manager.ApplicantManager
import epomis.mobile.manager.ProviderManager
import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter
import epomis.mobile.model.Job
import epomis.mobile.model.JobStatus
import epomis.mobile.model.JobType
import epomis.mobile.model.UserData
import epomis.mobile.persistence.ad.AdStorageFacade
import epomis.mobile.persistence.job.JobStorageFacade
import epomis.mobile.persistence.userdata.UserDataFacade
import epomis.utils.asSingle
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.toFlowable
import io.reactivex.schedulers.Schedulers

class BaseJobFacade(
    private val userDataFacade: UserDataFacade,
    private val providerManager: ProviderManager,
    private val applicantManager: ApplicantManager,
    private val jobStorageFacade: JobStorageFacade,
    private val adStorageFacade: AdStorageFacade
) : JobFacade, Service()
{
    override fun startCrawling(adFilter: AdFilter, jobType: JobType)
    {
        Single.just(Job(jobType, JobStatus.IN_PROGRESS, adFilter))
            .map { jobStorageFacade.insert(it) }
            .subscribeOn(Schedulers.io())
            .subscribe { job, error ->
                crawl(adFilter, job)
            }
    }

    override fun deleteTasks(jobs: List<Job>)
    {
        Flowable.fromIterable(jobs)
            .doOnComplete { jobStorageFacade.delete(jobs) }
            .map {it.id!!}
            .toList()
            .subscribeOn(Schedulers.io())
            .subscribe{ ids -> adStorageFacade.delete(ids)}
    }

    private fun crawl(adFilter: AdFilter, job: Job)
    {
        providerManager
            .acquireAds(adFilter)
            .toFlowable()
            .flatMap { it }
            .map { adStorageFacade.insert(it, job) }
            .doOnComplete { jobStorageFacade.updateStatus(job, computeStatusFor(job)) }
            .doOnError { jobStorageFacade.updateStatus(job, JobStatus.FAILED) }
            .subscribe()
    }

    private fun computeStatusFor(job: Job): JobStatus = when (job.type)
    {
        JobType.CRAWLING -> JobStatus.SUCCESSFUL
        JobType.APPLYING -> JobStatus.SCHEDULED
    }

    override fun startApplying(job: Job)
    {
        Flowable.just(job)
            .doOnNext { jobStorageFacade.updateStatus(job, JobStatus.IN_PROGRESS) }
            .flatMapIterable { adStorageFacade.findAllByJob(job) }
            .flatMapSingle { apply(it, userDataFacade.findUserData()) }
            .doOnComplete { jobStorageFacade.updateStatus(job, JobStatus.SUCCESSFUL) }
            .doOnError { jobStorageFacade.updateStatus(job, JobStatus.FAILED) }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    override fun applyFor(ad: Ad): Single<Ad> = userDataFacade
        .maybeFindUserData()
        .flatMapSingle { apply(ad, it) }
        .subscribeOn(Schedulers.io())

    private fun apply(ad: Ad, userData: UserData): Single<Ad> = applicantManager
        .applyFor(ad, userData)
        .asSingle()
        .map { adStorageFacade.updateApplicationResult(ad, it) }

    override fun onBind(intent: Intent?): IBinder?
    {
        TODO("Not yet implemented")
    }
}