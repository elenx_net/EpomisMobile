package epomis.mobile.job

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val jobModule = Kodein.Module("jobFacade")
{
    bind<JobFacade>() with singleton {
        BaseJobFacade(instance(), instance(), instance(), instance(), instance())
    }
}