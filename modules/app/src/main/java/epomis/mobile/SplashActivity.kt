package epomis.mobile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import epomis.mobile.screen.host.HostActivity

class SplashActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        HostActivity.start(this)
        finish()
    }
}