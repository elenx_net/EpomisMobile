package epomis.mobile

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import epomis.mobile.handler.RxJavaGlobalErrorHandler
import epomis.mobile.job.jobModule
import epomis.mobile.notification.notificationModule
import epomis.mobile.persistence.persistenceModule
import epomis.mobile.screen.screenModule
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.androidCoreModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

@Suppress("UNUSED")
class EpomisMobileApplication : Application(), KodeinAware
{
    override val kodein = Kodein.lazy {
        importOnce(androidCoreModule(this@EpomisMobileApplication), allowOverride = true)

        importOnce(providerModule, allowOverride = true)
        importOnce(applicantModule, allowOverride = true)
        importOnce(persistenceModule(this@EpomisMobileApplication), allowOverride = true)

        importOnce(screenModule, allowOverride = true)

        bind<SharedPreferences>() with singleton { PreferenceManager.getDefaultSharedPreferences(this@EpomisMobileApplication) }

        importOnce(jobModule, allowOverride = true)
        importOnce(notificationModule, allowOverride = true)
    }

    override fun onCreate()
    {
        RxJavaGlobalErrorHandler.setup()
        super.onCreate()
    }
}