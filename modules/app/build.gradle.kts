plugins {
    android
    kodein
    rx
}

apply(plugin = "androidx.navigation.safeargs.kotlin")

android.defaultConfig.applicationId = "epomis.mobile"

dependencies {

    implementation(project(":modules:model"))
    implementation(project(":modules:utils"))
    implementation(project(":modules:provider"))
    implementation(project(":modules:persistence"))
    implementation(project(":modules:applicant"))

    implementation("android.arch.navigation:navigation-ui-ktx:1.0.0")
    implementation("androidx.preference:preference-ktx:1.1.0")
    implementation("androidx.fragment:fragment-ktx:1.2.2")
    implementation("androidx.lifecycle:lifecycle-extensions:2.0.0")
    implementation("androidx.lifecycle:lifecycle-reactivestreams:2.0.0")
    implementation("androidx.recyclerview:recyclerview-selection:1.1.0-rc01")
    implementation("androidx.constraintlayout:constraintlayout:2.0.0-beta7")

    implementation("io.github.microutils:kotlin-logging:1.7.9")

    implementation("android.arch.navigation:navigation-fragment-ktx:1.0.0") {
        exclude(group = "com.android.support")
    }
}
