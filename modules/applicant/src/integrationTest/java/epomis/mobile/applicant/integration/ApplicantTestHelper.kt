package epomis.mobile.applicant.integration

import epomis.mobile.applicant.resume.BasicUserResume
import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter
import epomis.provider.cv.CVProvider

private val randomEmail = "jan" + System.currentTimeMillis().toString() + "@kowalski.pl"
private const val firstName = "Jan"
private const val lastName = "Kowalski"

class ApplicantTestHelper(
    private val cvProvider: CVProvider,
    private val namedProviderManager: NamedProviderManager)
{
    fun adWithUserResumeFor(provider: String, adFilter: AdFilter) =
        namedProviderManager
            .acquireAdFromProviderNamed(provider, adFilter)
            .let { createAdWithUserResume(it) }

    private fun createAdWithUserResume(ad: Ad) =
        AdWithUserResume(ad, BasicUserResume(userData, cvProvider.asInputStream()))

    private val userData
        get() =
            mapOf(
                "email" to randomEmail,
                "firstName" to firstName,
                "lastName" to lastName
            )
}
