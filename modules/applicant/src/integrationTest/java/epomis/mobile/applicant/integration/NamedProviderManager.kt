package epomis.mobile.applicant.integration

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter
import epomis.mobile.provider.GenericAdProvider
import epomis.service.connection6.ConnectionService6

class NamedProviderManager(
    private val connectionService: ConnectionService6,
    private val providers: Set<GenericAdProvider<*>>)
{
    fun acquireAdFromProviderNamed(providerName: String, adFilter: AdFilter): Ad
    {
        val provider = providers.find { it.javaClass.name == providerName }
            ?: throw RuntimeException("Provider $providerName is not implemented!")
        return acquireAdFromProvider(provider, adFilter)
    }

    private fun acquireAdFromProvider(provider: GenericAdProvider<*>, adFilter: AdFilter): Ad
    {
        @Suppress("UNCHECKED_CAST")
        val castedProvider = provider as GenericAdProvider<Any>

        val (request, method) = createRequestAndMethodFor(provider, adFilter)

        val response = castedProvider.send(request, connectionService, method).blockingGet()
        return castedProvider.extractAdsFrom(response).toList().shuffled().first()
    }

    private fun createRequestAndMethodFor(provider: GenericAdProvider<*>, adFilter: AdFilter) =
        Pair(
            provider.requestFor(adFilter.keyword, adFilter.location, 1),
            provider.methodFor(adFilter.keyword, adFilter.location, 1)
        )

}
