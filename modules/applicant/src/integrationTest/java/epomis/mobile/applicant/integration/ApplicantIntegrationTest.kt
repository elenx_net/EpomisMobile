package epomis.mobile.applicant.integration

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import epomis.mobile.applicant.integration.ResultType.HARD_FAIL
import epomis.mobile.applicant.integration.ResultType.SOFT_FAIL
import epomis.mobile.applicant.integration.ResultType.SUCCESS
import epomis.mobile.model.AdFilter
import mu.KotlinLogging
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

private val logger = KotlinLogging.logger("ApplicantIntegrationTest")

@RunWith(JUnitPlatform::class)
class ApplicantIntegrationTest : Spek
({
    val kodein = Kodein.direct { importOnce(applicantIntegrationTestModule, allowOverride = true) }

    val objectMapper: ObjectMapper = kodein.instance()
    val testPerformer: ApplicantIntegrationTestPerformer = kodein.instance()

    val partitionedProvidersWithApplicants =
        loadTestData(objectMapper).partition { it.applicantClasses.isNotEmpty() }

    val adFilter = AdFilter("", "")
    val testResults = mutableMapOf<ApplicantTestDataEntry, TestResult>()

    partitionedProvidersWithApplicants
        .first
        .forEach { testResults[it] = testPerformer.performTest(it, adFilter) }

    afterGroup {
        logger.info { "*** APPLICANT INTEGRATION TEST RESULTS ***" }
        logger.info { "Providers without applicantClasses" }
        partitionedProvidersWithApplicants
            .second
            .forEach { logger.info { it.providerClass } }

        logger.info { "Successful applicant classes" }
        testResults
            .filterValues { it.resultType == SUCCESS }
            .keys
            .forEach { logger.info { it.providerClass } }

        logger.info { "Soft-failed applicant classes (missing cookies, apply url changed, etc.)" }
        testResults
            .filterValues { it.resultType == SOFT_FAIL }
            .keys
            .forEach { logger.info { it.providerClass } }

        logger.info { "Hard-failed applicant classes (thrown exception)" }
        testResults
            .filterValues { it.resultType == HARD_FAIL }
            .forEach { (testDataEntry, testResult) -> logger.info { "${testDataEntry.providerClass} with error: ${prepareErrorMessage(testResult)}" } }

    }
    describe("Applicant Integration Test") {
        testResults
            .forEach { (testDataEntry, testResult) ->
                it("should apply successfully for ${testDataEntry.providerClass}") {
                    assertEquals(SUCCESS, testResult.resultType)
                }
            }
    }
})

private fun loadTestData(objectMapper: ObjectMapper) =
    objectMapper
        .readValue<List<ApplicantTestDataEntry>>(
            ApplicantIntegrationTest::class.java.getResourceAsStream("applicant-integration-test-data.json"))

private fun prepareErrorMessage(testResult: TestResult) =
    testResult.throwable?.stackTrace?.joinToString { "\n$it " } ?: "no stack trace"
