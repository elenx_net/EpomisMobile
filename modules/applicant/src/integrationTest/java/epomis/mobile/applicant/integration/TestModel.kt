package epomis.mobile.applicant.integration

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.resume.UserResume
import epomis.mobile.model.Ad

data class ApplicantTestDataEntry(
    val providerClass: String,
    val applicantClasses: List<String> = emptyList()
)

data class AdWithUserResume(
    val ad: Ad,
    val userResume: UserResume
)

data class TestResult(
    val resultType: ResultType,
    val applicationForm: ApplicationForm<Any>? = null,
    val throwable: Throwable? = null
)

enum class ResultType
{
    SUCCESS, HARD_FAIL, SOFT_FAIL
}
