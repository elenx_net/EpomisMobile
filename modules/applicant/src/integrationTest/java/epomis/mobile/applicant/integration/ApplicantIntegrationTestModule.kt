package epomis.mobile.applicant.integration

import epomis.mobile.applicantModule
import epomis.mobile.providerModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val applicantIntegrationTestModule = Kodein.Module("applicant.integrationTest")
{
    importOnce(applicantModule, allowOverride = true)
    importOnce(providerModule, allowOverride = true)

    bind<NamedProviderManager>() with singleton { NamedProviderManager(instance(), instance()) }
    bind<ApplicantTestHelper>() with singleton { ApplicantTestHelper(instance(), instance()) }
    bind<ApplicantIntegrationTestPerformer>() with singleton { ApplicantIntegrationTestPerformer(instance(), instance(), instance(), instance()) }
}
