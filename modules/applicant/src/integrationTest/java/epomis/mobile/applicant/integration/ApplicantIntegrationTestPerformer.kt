package epomis.mobile.applicant.integration

import epomis.mobile.applicant.Applicant
import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.manager.ApplicantFactory
import epomis.mobile.manager.RxApplicantManager
import epomis.mobile.model.AdFilter
import epomis.service.connection6.ConnectionService6
import io.reactivex.Flowable

class ApplicantIntegrationTestPerformer(
    private val applicantTestHelper: ApplicantTestHelper,
    private val applicants: Set<Applicant<Any>>,
    private val connectionService: ConnectionService6,
    private val applicantFactory: ApplicantFactory
)
{
    fun performTest(testDataEntry: ApplicantTestDataEntry, adFilter: AdFilter) =
        runCatching {
            val adWithUserResume = applicantTestHelper.adWithUserResumeFor(testDataEntry.providerClass, adFilter)
            val applicantManager = RxApplicantManager(connectionService, selectApplicantsByName(testDataEntry.applicantClasses), applicantFactory)
            val applicationForm = Flowable.fromPublisher(applicantManager.applyFor(adWithUserResume.ad, adWithUserResume.userResume)).blockingFirst()
            TestResult(testResultFor(applicationForm), applicationForm)
        }
            .getOrElse { TestResult(ResultType.HARD_FAIL, throwable = it) }

    private fun selectApplicantsByName(applicantsName: List<String>) =
        applicants
            .filter { applicantsName.contains(it.javaClass.name) }
            .toSet()

    private fun testResultFor(applicationForm: ApplicationForm<Any>) =
        when
        {
            isHardFail(applicationForm) -> ResultType.HARD_FAIL
            isSoftFail(applicationForm) -> ResultType.SOFT_FAIL
            else -> ResultType.SUCCESS
        }

    private fun isHardFail(applicationForm: ApplicationForm<Any>) =
        applicationForm.status == ApplicationForm.Status.EXCEPTION

    private fun isSoftFail(applicationForm: ApplicationForm<Any>) =
        !isHardFail(applicationForm) && applicationForm.status == ApplicationForm.Status.FAILURE
}
