package epomis.mobile.resume

import epomis.mobile.model.UserData
import epomis.service.connection6.request.DataEntry
import java.io.InputStream

internal class UserFile(
    val inputStream: InputStream,
    val fileName: String
)

internal interface UserResume
{
    val cv: UserFile
    operator fun get(key: String): String
    fun userDataEntries(): List<DataEntry>

    companion object
    {
        fun from(userData: UserData) = BasicUserResume(
            mapOf(
                "firstName" to userData.firstName,
                "lastName" to userData.lastName,
                "email" to userData.email
            ),
            userData.resumeContent.inputStream(),
            userData.resumeFilename
        )
    }
}