package epomis.mobile.resume

import epomis.service.connection6.request.DataEntry
import java.io.InputStream

internal class BasicUserResume(
    private val userData: Map<String, String> = mapOf(),
    private val inputStream: InputStream,
    private val cvFilename: String = "cv.pdf"
) : UserResume
{
    override val cv: UserFile
        get() = UserFile(inputStream, cvFilename)

    override fun userDataEntries(): List<DataEntry> = userData
        .map { DataEntry(it.key, it.value) }
        .plus(DataEntry("cv", cv.fileName, cv.inputStream))

    override fun get(key: String) = userData[key]!!
}