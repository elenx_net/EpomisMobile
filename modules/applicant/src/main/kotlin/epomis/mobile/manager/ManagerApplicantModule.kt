package epomis.mobile.manager

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val applicantManagerModule = Kodein.Module("applicantManager")
{
    bind<DomainNameExtractor>() with singleton { DomainNameExtractor() }
    bind<ApplicantFactory>() with singleton { ApplicantFactory(instance()) }
    bind<ApplicantManager>() with singleton { RxApplicantManager(instance(), instance(), instance()) }
}