package epomis.mobile.manager

import com.google.common.net.InternetDomainName

import java.net.URL

import epomis.mobile.model.Ad


internal class DomainNameExtractor
{
    private val digitsAsWords = arrayOf("Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine")

    fun extractDomain(ad: Ad): String = baseDomain(extractUrl(ad))

    private fun baseDomain(domain: String): String
    {
        val baseDomain = InternetDomainName
            .from(domain)
            .topPrivateDomain()
            .parts()[0]

        return if (startsWithLetter(baseDomain))
            baseDomain
        else
            convertDigitToLetter(baseDomain)
    }

    private fun extractUrl(ad: Ad): String
    {
        val host = URL(ad.href).host

        return if (host.startsWith("www")) host else "www.$host"
    }

    private fun startsWithLetter(baseDomain: String): Boolean = Character.isLetter(baseDomain[0])

    private fun convertDigitToLetter(domain: String): String = digitsAsWords[Character.getNumericValue(domain[0])] + domain.substring(1)
}
