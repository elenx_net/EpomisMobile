package epomis.mobile.manager

import epomis.mobile.model.Ad
import epomis.mobile.model.ApplicationResult
import epomis.mobile.model.UserData
import org.reactivestreams.Publisher

interface ApplicantManager
{
    fun applyFor(ad: Ad, userResume: UserData): Publisher<ApplicationResult>
}