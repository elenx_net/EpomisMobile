package epomis.mobile.manager

import epomis.mobile.applicant.Applicant
import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.model.Ad
import epomis.mobile.model.UserData
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.DataEntry

internal class ApplicantFactory(private val domainNameExtractor: DomainNameExtractor)
{
    fun applicationFormFor(ad: Ad, userData: UserData): ApplicationForm<Any>
        = ApplicationForm(
            ad = ad,
            baseDomain = domainNameExtractor.extractDomain(ad),
            userResume = UserResume.from(userData)
        )

    fun prepareRequest(applicant: Applicant<Any>, applicationForm: ApplicationForm<Any>)
        = ConnectionRequest(
            headers = applicant.headersFor(applicationForm),
            cookies = applicant.cookiesFor(applicationForm),
            data = applicant.dataEntriesFor(applicationForm) as List<DataEntry>,
            jsonObject = applicant.jsonData(applicationForm),
            url = applicant.urlFor(applicationForm)
        )
}
