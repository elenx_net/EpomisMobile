package epomis.mobile.manager

import epomis.mobile.applicant.Applicant
import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.model.Ad
import epomis.mobile.model.ApplicationResult
import epomis.mobile.model.UserData
import epomis.service.connection6.ConnectionService6
import epomis.utils.expand
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.schedulers.Schedulers

private val TERMINATING_STATUSES = listOf(
    ApplicationForm.Status.SUCCESS,
    ApplicationForm.Status.FAILURE,
    ApplicationForm.Status.EXCEPTION
)

internal class RxApplicantManager(
    private val connectionService6: ConnectionService6,
    private val applicants: Set<Applicant<Any>>,
    private val applicantFactory: ApplicantFactory
) : ApplicantManager
{
    override fun applyFor(ad: Ad, userData: UserData): Flowable<ApplicationResult> = createApplicationForm(ad, userData)
        .expand { applyWith(it) }
        .filter { it.isStatusTerminating() }
        .map { ApplicationResult(success = it.status == ApplicationForm.Status.SUCCESS) }

    private fun createApplicationForm(ad: Ad, userData: UserData) = Flowable
        .just(applicantFactory.applicationFormFor(ad, userData))

    private fun applyWith(applicationForm: ApplicationForm<Any>): Flowable<ApplicationForm<Any>> = Flowable
        .create({ emitter: FlowableEmitter<Applicant<Any>> ->
            if (!applicationForm.isStatusTerminating())
            {
                selectAppropriateApplicants(applicationForm)
                    .onEach { emitter.onNext(it) }
                    .ifEmpty { emitter.onError(Throwable("Could not find suitable applicant for this form")) }
            }
            emitter.onComplete()
        }, BackpressureStrategy.BUFFER)
        .flatMap { sendForm(it, applicationForm) }
        .onErrorReturn {
            it.printStackTrace()
            applicationForm.copy(status = ApplicationForm.Status.FAILURE, throwable = it)
        }

    private fun selectAppropriateApplicants(applicationForm: ApplicationForm<Any>): List<Applicant<Any>> = applicants
        .filter { it.isAppropriateFor(applicationForm) }

    private fun sendForm(applicant: Applicant<Any>, applicationForm: ApplicationForm<Any>) = Flowable
        .fromPublisher(
            applicant.send(connectionService6, applicantFactory.prepareRequest(applicant, applicationForm))
        )
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.computation())
        .map { applicant.advanceApplication(applicationForm, it) }
        .onErrorReturn {
            it.printStackTrace()
            applicationForm.copy(status = ApplicationForm.Status.EXCEPTION, throwable = it)
        }

    private fun ApplicationForm<Any>.isStatusTerminating() = status in TERMINATING_STATUSES
}
