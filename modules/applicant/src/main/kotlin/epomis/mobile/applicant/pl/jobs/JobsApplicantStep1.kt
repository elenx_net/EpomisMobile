package epomis.mobile.applicant.pl.jobs

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant

import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse

private const val URL = "https://www.jobs.pl/formularz-aplikacyjny-"
private const val JOBS_STEP = "jobs_step"
private const val SUITABLE_STEP_FOR_THIS_APPLICANT = "0"
private const val NEXT_STEP = "1"

private const val DATA_FROM_LAST_REQUEST = "dataFromLastRequest"
private const val JOB_APPLY_SEND_EMAIL = "job_apply_send_email"
private const val AD_LOC_ID = "offer_loc_id"
private const val APPLY = "aplikuj"

private const val APPLY_VALUE = "Aplikuj teraz"
private const val COOKIE_SID = "sid"

internal class JobsApplicantStep1 : HtmlApplicant
{
    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>) =
        "$URL${applicationForm.ad.href.substringAfterLast('-')}"

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
        hasInProgressStatus(applicationForm.status)
                && hasProperCustomData(applicationForm.customData)
                && hasCorrectStep(applicationForm.customData[JOBS_STEP]!!)
                && hasApplicationOnSite(applicationForm.customData[JOB_APPLY_SEND_EMAIL]!!)

    private fun hasProperCustomData(customData: Map<String, String>) =
        customData.containsKey(JOBS_STEP)
                && customData.containsKey(JOB_APPLY_SEND_EMAIL)

    private fun hasInProgressStatus(status: ApplicationForm.Status) = status == ApplicationForm.Status.IN_PROGRESS

    private fun hasCorrectStep(step: String) = step == SUITABLE_STEP_FOR_THIS_APPLICANT

    private fun hasApplicationOnSite(jobApplySendEmail: String) = jobApplySendEmail.isNotBlank()

    override fun advanceApplication(
        applicationForm: ApplicationForm<HtmlResponse>,
        currentResponse: HtmlResponse?
    ): ApplicationForm<HtmlResponse>
    {
        return applicationForm.copy(
            status = if (isSuccessful(currentResponse!!)) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = createCustomData(applicationForm)
        )
    }

    private fun createCustomData(applicationForm: ApplicationForm<HtmlResponse>): Map<String, String> = mapOf(
        JOBS_STEP to NEXT_STEP,
        COOKIE_SID to applicationForm.previousResponse?.connectionResponse!!.cookies[COOKIE_SID]!!
    )

    private fun isSuccessful(currentResponse: HtmlResponse) =
        !currentResponse.cookies.contains(DATA_FROM_LAST_REQUEST)

    override fun dataEntriesFor(applicationForm: ApplicationForm<HtmlResponse>) =
        listOf(
            DataEntry(APPLY, APPLY_VALUE),
            DataEntry(JOB_APPLY_SEND_EMAIL, applicationForm.customData[JOB_APPLY_SEND_EMAIL]!!),
            DataEntry(AD_LOC_ID, applicationForm.customData[AD_LOC_ID]!!)
        )
}