package epomis.mobile.applicant.pl.olx

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse

import java.util.Collections
import java.util.stream.Collectors

private const val SUFFIX = "?bs=adpage_chat_login_bottom"
private const val MESSAGE =
    "Szanowni Państwo:\n" +
        "W odpowiedzi na Państwa ofertę pracy:\n" +
        "Stanowisko / tytuł: %S\n" +
        "Zgłoszoną w dniu: %S\n" +
        "\n" +
        "przesyłam swoją aplikację.\n" +
        "\n" +
        "Proszę o rozważenie mojej kandydatury na powyższe stanowisko."

private val REQUIRED_COOKIES_FROM_PREVIOUS_STEP = listOf(
    "READY_TO_APPLY", "PHPSESSID",
    "user_id", "access_token",
    "remember_login", "refresh_token"
)

internal class OlxApplicant : HtmlApplicant
{
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = hasProperUrl(applicationForm) && isInProgress(applicationForm) && hasRequiredCookies(applicationForm)

    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>)
        = applicationForm.ad.href + SUFFIX

    override fun constantDataEntries(): List<DataEntry>
        = Collections.singletonList(DataEntry("contact[debug]", "7"))

    override fun userDataEntries(userResume: UserResume)
        = listOf(
                DataEntry("contact[txt]", MESSAGE),
                DataEntry("attach_cv", userResume.cv.fileName, userResume.cv.inputStream)
            )

    override fun cookiesFor(applicationForm: ApplicationForm<HtmlResponse>): Map<String, String>
        = applicationForm
            .customData
            .entries
            .stream()
            .filter{ REQUIRED_COOKIES_FROM_PREVIOUS_STEP.contains(it.key) }
            .collect(Collectors.toMap(Map.Entry<String, String>::key, Map.Entry<String, String>::value))

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse?)
        = applicationForm.copy(
            status = if (currentResponse!!.isOk()) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse
        )

    private fun hasProperUrl(applicationForm: ApplicationForm<*>)
        = containsDomain(applicationForm.ad.href)

    private fun containsDomain(href: String)
        = href.contains("olx.pl")

    private fun isInProgress(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.IN_PROGRESS

    private fun hasRequiredCookies(applicationForm: ApplicationForm<*>)
        = applicationForm
            .customData
            .keys
            .containsAll(REQUIRED_COOKIES_FROM_PREVIOUS_STEP)
}