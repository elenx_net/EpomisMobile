package epomis.mobile.applicant.pl.olx

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.service.connection6.response.HtmlResponse

import java.util.HashMap

private val REQUIRED_COOKIES_FROM_PREVIOUS_STEP = listOf(
    "PHPSESSID1", "user_id1", "access_token1",
    "remember_login1", "refresh_token1"
)

internal class OlxDataFetcher : HtmlApplicant
{
    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>)
        = applicationForm.ad.href
    
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = isUrlPrepared(applicationForm) && isInProgress(applicationForm) && hasRequiredCookies(applicationForm)
    
    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>,
                                    currentResponse: HtmlResponse?): ApplicationForm<HtmlResponse>
    {
        return applicationForm.copy(
            status = if (currentResponse!!.isOk()) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = createCustomData(applicationForm.customData)
        )
    }
    
    private fun isUrlPrepared(applicationForm: ApplicationForm<*>)
        = containsDomain(applicationForm.ad.href)
    
    private fun containsDomain(href: String)
        = href.contains("olx.pl")
    
    private fun isInProgress(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.IN_PROGRESS
    
    private fun hasRequiredCookies(applicationForm: ApplicationForm<*>)
        = applicationForm
            .customData
            .keys
            .containsAll(REQUIRED_COOKIES_FROM_PREVIOUS_STEP)
    
    private fun createCustomData(previousResponseCustomData: Map<String, String>): Map<String, String>
    {
        val customData = HashMap<String, String>()
        customData.apply {
            previousResponseCustomData["PHPSESSID1"]?.let { put("PHPSESSID2", it) }
            previousResponseCustomData["user_id1"]?.let { put("user_id2", it) }
            previousResponseCustomData["access_token1"]?.let { put("access_token2", it) }
            previousResponseCustomData["remember_login1"]?.let { put("remember_login2", it) }
            previousResponseCustomData["refresh_token1"]?.let { put("refresh_token2", it) }
            put("CV", "CV")
        }
        
        return customData
    }
}
