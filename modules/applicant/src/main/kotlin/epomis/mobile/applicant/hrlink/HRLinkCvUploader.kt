package epomis.mobile.applicant.hrlink

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse

import java.util.Collections

private const val SESSION_COOKIE_FROM_PREVIOUS_STEP = "PHPSESSID1"
private const val ADD_CV_URL = "https://light.hrlink.pl/?sv=9999&page_typ=2&module=pliki&typ=dodajcv"

internal class HRLinkCvUploader : HtmlApplicant
{
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = hasProperUrl(applicationForm) && hasInProgressStatus(applicationForm) && hasSessionCookieId(applicationForm)

    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>) = ADD_CV_URL

    override fun userDataEntries(userResume: UserResume)
        = Collections.singletonList(
            DataEntry("file_cv", userResume.cv.fileName, userResume.cv.inputStream)
        )

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse?): ApplicationForm<HtmlResponse>
    {
        val shouldContinue = currentResponse!!.isOk() && responseContainsCvFileName(applicationForm, currentResponse)

        return applicationForm.copy(
            status = if (shouldContinue) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            customData = Collections.singletonMap<String, String>("PHPSESSID2",
                applicationForm.customData[SESSION_COOKIE_FROM_PREVIOUS_STEP])
        )
    }

    private fun responseContainsCvFileName(applicationForm: ApplicationForm<HtmlResponse>, response: HtmlResponse): Boolean
    {
        val fileNameWithoutExtension = removeFileNameExtension(
            applicationForm
                .userResume
                .cv
                .fileName
        )

        return responseContains(fileNameWithoutExtension, response)
    }

    private fun removeFileNameExtension(fileName: String) = fileName.split(".")[0]

    private fun responseContains(fileNameWithoutExtension: String, response: HtmlResponse)
        = response
            .document
            .text()
            .contains(fileNameWithoutExtension)

    private fun hasProperUrl(applicationForm: ApplicationForm<*>)
        = containsDomain(applicationForm.ad.href)

    private fun containsDomain(href: String) = href.contains("hrlink.pl")

    private fun hasInProgressStatus(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.IN_PROGRESS

    private fun hasSessionCookieId(applicationForm: ApplicationForm<*>)
        = containsSessionCookie(applicationForm.customData)

    private fun containsSessionCookie(cookies: Map<String, String>)
        = cookies.containsKey(SESSION_COOKIE_FROM_PREVIOUS_STEP)
}
