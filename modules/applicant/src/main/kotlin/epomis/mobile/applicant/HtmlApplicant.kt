package epomis.mobile.applicant

import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.HtmlResponse
import org.reactivestreams.Publisher

internal interface HtmlApplicant : Applicant<HtmlResponse>
{
    override fun send(connectionService6: ConnectionService6, connectionRequest: ConnectionRequest): Publisher<HtmlResponse>
        = connectionService6.reactivePostForHtml(connectionRequest)

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>,
                                    currentResponse: HtmlResponse?)
        = applicationForm.copy(
            status = if (currentResponse != null && currentResponse.isOk()) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse
        )

    override fun previousResponseCookies(previousResponse: HtmlResponse?): Map<String, String>
        = previousResponse?.cookies ?: emptyMap()
}
