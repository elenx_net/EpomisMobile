package epomis.mobile.applicant.pl.jober

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.JsonApplicant
import epomis.mobile.applicant.pl.jober.model.JoberStep1Response
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.JsonResponse

private const val URL = "https://www.jober.pl/?c=apply&a=save"

private const val STEP = "jober.pl_step"
private const val SUITABLE_STEP_FOR_THIS_APPLICANT = "0"
private const val NEXT_STEP = "1"

private const val CONTENT_TYPE = "Content-Type"
private const val NAME = "name"
private const val LASTNAME = "lastname"
private const val JOB_ID = "job_id"
private const val PHONE = "phone"
private const val EMAIL = "email"
private const val TMP_CV = "tmpCV"
private const val DESCRIPTION = "description"

private const val CONTENT_TYPE_VALUE = "multipart/form-data"
private const val NAME_VALUE = "name"
private const val LASTNAME_VALUE = "lastname"
private const val PHONE_VALUE = "132321123"
private const val EMAIL_VALUE = "aaa@test.com"
private const val DESCRIPTION_VALUE = "Przesyłam swoje CV i wyrażam chęć wzięcia udziału w procesie rekrutacyjnym"

private const val CORRECT_STATUS = true

internal class JoberApplicantStep1 : JsonApplicant<JoberStep1Response>
{
    override val jsonClass = JoberStep1Response::class.java

    override fun urlFor(applicationForm: ApplicationForm<JsonResponse<JoberStep1Response>>) = URL

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
        hasInProgressStatus(applicationForm.status)
            && hasProperCustomData(applicationForm.customData)
            && hasCorrectStep(applicationForm.customData[STEP]!!)
            && hasTmpCv(applicationForm.customData[TMP_CV]!!)

    override fun constantHeaders() = mapOf(CONTENT_TYPE to CONTENT_TYPE_VALUE)

    override fun dataEntriesFor(applicationForm: ApplicationForm<JsonResponse<JoberStep1Response>>) =
        listOf(
            DataEntry(TMP_CV, applicationForm.customData[TMP_CV]!!),
            DataEntry(NAME, NAME_VALUE),
            DataEntry(LASTNAME, LASTNAME_VALUE),
            DataEntry(PHONE, PHONE_VALUE),
            DataEntry(EMAIL, EMAIL_VALUE),
            DataEntry(DESCRIPTION, DESCRIPTION_VALUE),
            DataEntry(TMP_CV, applicationForm.customData[TMP_CV]!!),
            DataEntry(JOB_ID, applicationForm.ad.href.filter { it.isDigit() })
        )

    override fun advanceApplication(applicationForm: ApplicationForm<JsonResponse<JoberStep1Response>>,
                                    currentResponse: JsonResponse<JoberStep1Response>?) =
        applicationForm.copy(
            status = if (currentResponse!!.json.status == CORRECT_STATUS) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE,
            customData = createCustomData()
        )

    private fun hasProperCustomData(customData: Map<String, String>) =
        customData.containsKey(STEP) && customData.containsKey(TMP_CV)

    private fun hasInProgressStatus(status: ApplicationForm.Status) = status == ApplicationForm.Status.IN_PROGRESS

    private fun hasCorrectStep(step: String) = step == SUITABLE_STEP_FOR_THIS_APPLICANT

    private fun hasTmpCv(tmpCv: String) = tmpCv.isNotBlank()

    private fun createCustomData() = mapOf(STEP to NEXT_STEP)
}
