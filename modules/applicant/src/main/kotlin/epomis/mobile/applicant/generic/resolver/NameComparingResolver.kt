package epomis.mobile.applicant.generic.resolver

import epomis.mobile.applicant.generic.model.ApplyFormEntry
import epomis.service.connection6.request.DataEntry
import epomis.utils.TextAnalyzer

import java.util.Collections
import java.util.stream.Collectors

internal class NameComparingResolver : RequestDataResolver
{
    override fun resolveFormData(availableData: List<DataEntry>, formEntries: List<ApplyFormEntry>): List<DataEntry>
    {
        if(availableData.size < countEmptyFields(formEntries))
        {
            throw IllegalArgumentException("Not enough user data to fill the form!")
        }

        return formEntries
            .stream()
            .map{ calculateNameSimilarities(it, availableData) }
            .flatMap { it.stream() }
            .collect(Collectors.groupingBy(this::availableDataKey))
            .entries
            .stream()
            .map { it.value }
            .map(this::pickEntryWithMostSimilarKey)
            .collect(Collectors.groupingBy(this::formEntryKey))
            .entries
            .stream()
            .map(this::toSingleEntry)
            .map(this::toDataEntry)
            .collect(Collectors.toList<DataEntry>())
    }

    private fun countEmptyFields(formEntries: List<ApplyFormEntry>)
        = formEntries
            .stream()
            .filter{ it.data.value.isEmpty() }
            .count()

    private fun calculateNameSimilarities(formEntry: ApplyFormEntry, availableData: List<DataEntry>): List<SimilarityEntry>
    {
        if(isValuePresent(formEntry))
        {
            return defaultEntryValue(formEntry)
        }

        return availableData
            .mapNotNull { toSimilarityEntry(formEntry, it) }
            .toList()
    }

    private fun isValuePresent(formEntry: ApplyFormEntry) = formEntry.data.value.isNotEmpty()

    private fun defaultEntryValue(formEntry: ApplyFormEntry): List<SimilarityEntry>
        = Collections.singletonList(
            SimilarityEntry(
                formEntry = formEntry,
                availableData = formEntry.data,
                similarity = Float.MAX_VALUE
            )
    )

    private fun toSimilarityEntry(formEntry: ApplyFormEntry, dataEntry: DataEntry): SimilarityEntry?
    {
        if(formEntry.isFile && dataEntry.hasInputStream() || !formEntry.isFile && !dataEntry.hasInputStream())
        {
            return SimilarityEntry(
                formEntry = formEntry,
                similarity = TextAnalyzer.stringSimilarity(formEntry.data.key, dataEntry.key),
                availableData = dataEntry
            )
        }

        return null
    }

    private fun availableDataKey(similarityEntry: SimilarityEntry)
        = similarityEntry
            .availableData
            .key

    private fun pickEntryWithMostSimilarKey(similarityEntries: List<SimilarityEntry>)
        = similarityEntries
            .stream()
            .reduce(this::mostSimilar)
            .orElseThrow{ RuntimeException() }

    private fun mostSimilar(similarityEntry: SimilarityEntry, similarityEntry1: SimilarityEntry)
        = if (similarityEntry.similarity > similarityEntry1.similarity) similarityEntry else similarityEntry1

    private fun formEntryKey(similarityEntry: SimilarityEntry)
        = similarityEntry.formEntry.data.key

    private fun toSingleEntry(stringListEntry: Map.Entry<String, List<SimilarityEntry>>)
        = stringListEntry
            .value
            .stream()
            .reduce(this::mostSimilar)
            .orElseThrow{ RuntimeException()}

    private fun toDataEntry(similarityEntry: SimilarityEntry)
        = DataEntry(
            key = similarityEntry.formEntry.data.key,
            value = similarityEntry.availableData.value,
            inputStream = similarityEntry.availableData.inputStream
        )
}