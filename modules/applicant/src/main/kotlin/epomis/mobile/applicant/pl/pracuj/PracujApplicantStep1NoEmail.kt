package epomis.mobile.applicant.pl.pracuj

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.applicant.cookie.CookieCollector
import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document

internal class PracujApplicantStep1NoEmail(private val cookieCollector: CookieCollector) : HtmlApplicant
{
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = isInProgress(applicationForm) && isStep1(applicationForm) && hasAdId(applicationForm)
        && isHtmlResponse(applicationForm) && isNoEmail(applicationForm)

    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>)
        = extractHref(applicationForm.previousResponse!!.document)

    override fun cookiesFor(applicationForm: ApplicationForm<HtmlResponse>)
        = cookieCollector.readCookiesFromCustomData(applicationForm)

    override fun send(connectionService6: ConnectionService6, connectionRequest: ConnectionRequest)
        = connectionService6.reactiveGetForHtml(connectionRequest)

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse?): ApplicationForm<HtmlResponse>
        = applicationForm.copy(
            status = ApplicationForm.Status.IN_PROGRESS,
            previousResponse = currentResponse!!,
            customData = createCustomData(applicationForm, currentResponse)
        )

    private fun isInProgress(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.IN_PROGRESS
    private fun isStep1(applicationForm: ApplicationForm<*>)
        = applicationForm.customData.containsKey(PRACUJPL_STEP) && (applicationForm.customData[PRACUJPL_STEP] ?: "") == "1"
    private fun hasAdId(applicationForm: ApplicationForm<*>)
        = applicationForm.customData.containsKey(PRACUJPL_AD_ID)
    private fun isHtmlResponse(applicationForm: ApplicationForm<*>)
        = applicationForm.previousResponse is HtmlResponse
    private fun isNoEmail(applicationForm: ApplicationForm<*>)
        = (applicationForm.previousResponse as HtmlResponse).document.getElementById("continueButton") != null

    private fun createCustomData(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse)
        = cookieCollector.saveCookiesToCustomData(applicationForm, currentResponse)

    private fun extractHref(document: Document)
        = document.getElementById("continueButton").attr("href")!!
}