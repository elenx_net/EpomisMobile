package epomis.mobile.applicant

import epomis.mobile.model.Ad
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry

import java.util.LinkedList

internal interface DataEntriesAdapter<in RESPONSE>
{
    fun dataEntriesFor(applicationForm: ApplicationForm<RESPONSE>): Collection<DataEntry>
    {
        val dataEntries = LinkedList<DataEntry>()
        dataEntries.apply {
            addAll(constantDataEntries())
            addAll(adsDataEntries(applicationForm.ad))
            addAll(userDataEntries(applicationForm.userResume))
            addAll(previousResponseDataEntries(applicationForm.previousResponse))
        }

        return dataEntries
    }

    fun constantDataEntries(): Collection<DataEntry> = emptyList()

    fun adsDataEntries(ad: Ad): Collection<DataEntry> = emptyList()

    fun userDataEntries(userResume: UserResume): Collection<DataEntry> = emptyList()

    fun previousResponseDataEntries(previousResponse: RESPONSE?): Collection<DataEntry> = emptyList()
}
