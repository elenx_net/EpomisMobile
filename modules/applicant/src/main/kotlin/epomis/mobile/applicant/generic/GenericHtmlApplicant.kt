package epomis.mobile.applicant.generic

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.applicant.generic.provider.ApplyFormProvider
import epomis.mobile.applicant.generic.model.ApplyForm
import epomis.mobile.applicant.generic.resolver.RequestDataResolver
import epomis.mobile.resume.UserResume
import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking

private const val URL = "genericHtmlApplicantUrl"

internal class GenericHtmlApplicant(private val requestDataResolver: RequestDataResolver,
                           private val applyFormProvider: ApplyFormProvider,
                           private val connectionService: ConnectionService6) : HtmlApplicant
{
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = applicationForm.previousResponse == null

    override fun isAppropriateFor(adBaseDomain: String) = true

    override fun cookiesFor(applicationForm: ApplicationForm<HtmlResponse>)
        = fetchInformationFromUrl(applicationForm.ad.href, { it.cookies })

    override fun dataEntriesFor(applicationForm: ApplicationForm<HtmlResponse>): Collection<DataEntry>
    {
        val document = fetchInformationFromUrl(
            url = applicationForm.ad.href,
            responseMapper = { it.document }
        )
        val applyForm = applyFormProvider.extractApplyFormFrom(document)

        applicationForm
            .customData
            .plus(Pair(URL, applyForm.url))

        return resolverMissingRequestData(applyForm, applicationForm.userResume)
    }

    private fun resolverMissingRequestData(applyForm: ApplyForm, userResume: UserResume): List<DataEntry>
    {
        val userDataEntries = userResume.userDataEntries()
        val formData = applyForm.formData

        return requestDataResolver.resolveFormData(userDataEntries, formData)
    }

    private fun <T> fetchInformationFromUrl(url: String, responseMapper: (HtmlResponse) -> T): T
    {
        val request = ConnectionRequest(url)

        return runBlocking { responseMapper(connectionService.reactiveGetForHtml(request).awaitSingle()) }
    }

    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>) = applicationForm.customData[URL]!!
}