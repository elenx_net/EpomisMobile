package epomis.mobile.applicant.pl.praca

import com.google.common.net.HttpHeaders

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.JsonApplicant
import epomis.mobile.applicant.pl.praca.model.PracaResponse
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.JsonResponse

import org.apache.commons.io.IOUtils
import org.apache.commons.codec.binary.Base64

import java.util.Collections


private const val SAVE_FILE_URL = "https://www.praca.pl/ad/application_api/saveFile"
private const val PRACA_STEP = "praca.pl_step"
private val base64Encoder = Base64()

internal class PracaCvUploader : JsonApplicant<PracaResponse>
{
    override val jsonClass: Class<PracaResponse>
        get() = PracaResponse::class.java

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
        hasNewStatus(applicationForm.status) && hasValidBaseDomain(applicationForm.baseDomain)

    private fun hasNewStatus(status: ApplicationForm.Status) =
        status == ApplicationForm.Status.NEW

    private fun hasValidBaseDomain(baseDomain: String) =
        baseDomain.toLowerCase() == "praca"

    override fun urlFor(applicationForm: ApplicationForm<JsonResponse<PracaResponse>>) = SAVE_FILE_URL

    override fun userDataEntries(userResume: UserResume) = listOf(
        DataEntry("data[Employee][file][base64]", encodeCvToBase64(userResume)),
        DataEntry("data[Employee][file][name]", userResume.cv.fileName)
    )

    private fun encodeCvToBase64(userResume: UserResume) =
        "data:application/pdf;base64," + String(base64Encoder.encode(IOUtils.toByteArray(userResume.cv.inputStream)))

    override fun advanceApplication(applicationForm: ApplicationForm<JsonResponse<PracaResponse>>,
                                    currentResponse: JsonResponse<PracaResponse>?): ApplicationForm<JsonResponse<PracaResponse>>
    {
        val isSuccessful = currentResponse!!
            .json
            .status == "success"

        return applicationForm.copy(
            status = if (isSuccessful) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = mapOf(PRACA_STEP to "0")
        )
    }

    override fun constantHeaders() = Collections.singletonMap(HttpHeaders.X_REQUESTED_WITH, "XMLHttpRequest")
}
