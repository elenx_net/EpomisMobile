package epomis.mobile.applicant.pl.jober.model

import org.codehaus.jackson.annotate.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class JoberStep1Response
(
    var status: Any?
)
