package epomis.mobile.applicant.pl.olx

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils

import java.util.stream.Collectors
import java.util.Collections
import java.util.HashMap

private const val REQUIRED_BODY_MESSAGE = "[{\"status\":1}]"
private const val UPLOAD_CV_URL = "https://www.olx.pl/ajax/myaccount/cv/upload/"

private val REQUIRED_COOKIES_FROM_PREVIOUS_STEP = listOf(
    "CV", "PHPSESSID2", "user_id2",
    "access_token2", "remember_login2", "refresh_token2"
)

internal class OlxCvUploader : HtmlApplicant
{
    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>) = UPLOAD_CV_URL

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = hasRequiredCookies(applicationForm)

    override fun userDataEntries(userResume: UserResume)
        = Collections.singletonList(DataEntry("file", userResume.cv.fileName, userResume.cv.inputStream))

    override fun cookiesFor(applicationForm: ApplicationForm<HtmlResponse>): Map<String, String>
        = applicationForm
            .customData
            .entries
            .stream()
            .filter{ REQUIRED_COOKIES_FROM_PREVIOUS_STEP.contains(it.key) }
            .collect(Collectors.toMap({ StringUtils.chop(it.key) }, Map.Entry<String, String>::value))

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>,
                                    currentResponse: HtmlResponse?): ApplicationForm<HtmlResponse>
    {
        return applicationForm.copy(
            status = if (hasJsonStatusOne(currentResponse!!)) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = createCustomData(applicationForm.customData)
        )
    }

    private fun hasJsonStatusOne(htmlResponse: HtmlResponse)
        = htmlResponse
            .document
            .body()
            .toString()
            .contains(REQUIRED_BODY_MESSAGE)

    private fun hasRequiredCookies(applicationForm: ApplicationForm<*>)
        = applicationForm
            .customData
            .keys
            .containsAll(REQUIRED_COOKIES_FROM_PREVIOUS_STEP)

    private fun createCustomData(previousResponseCustomData: Map<String, String>): Map<String, String>
    {
        val customData = HashMap<String, String>()
        customData.apply {
            previousResponseCustomData["PHPSESSID2"]?.let { put("PHPSESSID", it) }
            previousResponseCustomData["user_id2"]?.let { put("user_id", it) }
            previousResponseCustomData["access_token2"]?.let { put("access_token", it) }
            previousResponseCustomData["remember_login2"]?.let { put("remember_login", it) }
            previousResponseCustomData["refresh_token2"]?.let { put("refresh_token", it) }
            put("READY_TO_APPLY", "READY")
        }

        return customData
    }
}