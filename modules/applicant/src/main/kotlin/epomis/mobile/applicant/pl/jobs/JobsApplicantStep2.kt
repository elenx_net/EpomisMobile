package epomis.mobile.applicant.pl.jobs

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse
import org.apache.commons.lang3.StringUtils

private const val URL = "https://www.jobs.pl/formularz-aplikacyjny-"
private const val JOBS_STEP = "jobs_step"
private const val SUITABLE_STEP_FOR_THIS_APPLICANT = "1"
private const val NEXT_STEP = "2"

private const val DATA_FROM_LAST_REQUEST = "dataFromLastRequest"

private const val USER_FILE_TYPE = "user_file_1_type"
private const val USER_FILE = "user_file_1"
private const val USERNAME = "username"
private const val RULE_ACCEPT = "rule_accept"
private const val CONTENT_TYPE = "Content-Type"

private const val USER_FILE_TYPE_VALUE = "3"
private const val USERNAME_VALUE = "test@test.com"
private const val RULE_ACCEPT_VALUE = "on"
private const val CONTENT_TYPE_VALUE = "multipart/form-data"
private const val COOKIE_SID = "sid"

internal class JobsApplicantStep2 : HtmlApplicant
{
    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>): String =
        "$URL${applicationForm.ad.href.filter { it.isDigit() }}"

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
        hasInProgressStatus(applicationForm.status)
                && hasProperCustomData(applicationForm.customData)
                && hasCorrectStep(applicationForm.customData[JOBS_STEP]!!)

    override fun cookiesFor(applicationForm: ApplicationForm<HtmlResponse>) =
        mapOf("sid" to applicationForm.customData["sid"]!!)

    private fun hasInProgressStatus(status: ApplicationForm.Status) = status == ApplicationForm.Status.IN_PROGRESS

    private fun hasProperCustomData(customData: Map<String, String>) = customData.containsKey(JOBS_STEP)

    private fun hasCorrectStep(step: String) = step == SUITABLE_STEP_FOR_THIS_APPLICANT

    override fun constantHeaders() = mapOf(CONTENT_TYPE to CONTENT_TYPE_VALUE)

    override fun userDataEntries(userResume: UserResume) =
        listOf(
            DataEntry(USER_FILE, StringUtils.EMPTY, userResume.cv.inputStream),
            DataEntry(USER_FILE_TYPE, USER_FILE_TYPE_VALUE),
            DataEntry(USERNAME, USERNAME_VALUE),
            DataEntry(RULE_ACCEPT, RULE_ACCEPT_VALUE)
        )

    override fun advanceApplication(
        applicationForm: ApplicationForm<HtmlResponse>,
        currentResponse: HtmlResponse?
    ): ApplicationForm<HtmlResponse> =
        applicationForm.copy(
            status = if (isSuccessful(currentResponse!!)) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = createCustomData(applicationForm)
        )

    private fun createCustomData(applicationForm: ApplicationForm<HtmlResponse>): Map<String, String> = mapOf(
        JOBS_STEP to NEXT_STEP,
        COOKIE_SID to applicationForm.customData[COOKIE_SID]!!
    )

    private fun isSuccessful(currentResponse: HtmlResponse) =
        !currentResponse.cookies.contains(DATA_FROM_LAST_REQUEST)
}