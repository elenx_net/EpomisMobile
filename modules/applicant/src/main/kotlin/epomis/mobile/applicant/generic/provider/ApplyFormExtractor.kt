package epomis.mobile.applicant.generic.provider

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

internal class ApplyFormExtractor
{
    fun extractFrom(document: Document): Element
        = document
            .getElementsByTag("form")
            .stream()
            .filter(this::isPost)
            .findAny()
            .orElseThrow{ RuntimeException("There is no <form> in given Document") }

    private fun isPost(form: Element) = form.attr("method").equals("post", ignoreCase = true)
}