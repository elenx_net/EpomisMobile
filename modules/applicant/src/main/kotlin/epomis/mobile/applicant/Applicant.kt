package epomis.mobile.applicant

import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.request.ConnectionRequest

import org.apache.commons.lang3.StringUtils
import org.reactivestreams.Publisher

const val DEFAULT_MESSAGE = "I send my CV and express my willingness to participate in the recruitment process"

internal interface Applicant<RESPONSE> : DataEntriesAdapter<RESPONSE>, HeadersAdapter<RESPONSE>, CookiesAdapter<RESPONSE>
{
    /**
     * @param applicationForm to be possibly processed
     *
     * @return false if there is no chance for success in applyFor method, true otherwise
     */
    fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = isAppropriateFor(applicationForm.baseDomain)

    fun isAppropriateFor(adBaseDomain: String)
        = StringUtils.startsWithIgnoreCase(javaClass.simpleName, adBaseDomain)

    /**
     * @return full and valid URL where data shall be sent
     */
    fun urlFor(applicationForm: ApplicationForm<RESPONSE>): String
        = applicationForm.baseDomain + applicationForm.ad.href

    /**
     * @param connectionService6 current implementation of service responsible for issuing network requests
     * @param connectionRequest  request prepared basing on information provided by Applicant interface
     *
     * @return promise for response or empty promise if there is no request to be made
     */
    fun send(connectionService6: ConnectionService6, connectionRequest: ConnectionRequest): Publisher<RESPONSE>

    /**
     * @param applicationForm that contains data for this application step
     * @param currentResponse that was received from server or null if #send() returned empty promise
     *
     * @return applicationForm for next step
     */
    fun advanceApplication(applicationForm: ApplicationForm<RESPONSE>, currentResponse: RESPONSE?)
        = applicationForm.copy(previousResponse = currentResponse, status = ApplicationForm.Status.DEFAULT)

    /**
     * @param applicationForm currently processed
     *
     * @return object to place in request body, for empty body return null
     */
    fun jsonData(applicationForm: ApplicationForm<RESPONSE>): Any? = null
}
