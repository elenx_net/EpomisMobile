package epomis.mobile.applicant

import epomis.mobile.model.Ad
import epomis.mobile.resume.UserResume

import java.util.HashMap

internal interface CookiesAdapter<in RESPONSE>
{
    fun cookiesFor(applicationForm: ApplicationForm<RESPONSE>): Map<String, String>
    {
        val cookies = HashMap<String, String>()
        cookies.apply {
            putAll(constantCookies())
            putAll(adCookies(applicationForm.ad))
            putAll(userDataCookies(applicationForm.userResume))
            putAll(previousResponseCookies(applicationForm.previousResponse))
        }

        return cookies
    }

    fun constantCookies(): Map<String, String> = emptyMap()

    fun adCookies(ad: Ad): Map<String, String> = emptyMap()

    fun userDataCookies(userResume: UserResume): Map<String, String> = emptyMap()

    fun previousResponseCookies(previousResponse: RESPONSE?): Map<String, String> = emptyMap()
}
