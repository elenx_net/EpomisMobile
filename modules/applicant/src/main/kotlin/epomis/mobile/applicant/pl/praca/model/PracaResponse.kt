package epomis.mobile.applicant.pl.praca.model

import com.google.api.client.util.Key

import org.codehaus.jackson.annotate.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
internal class PracaResponse(@Key var status: String,
                    @Key var data: Data)
{
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Data(@Key var id: Int = 0)
}
