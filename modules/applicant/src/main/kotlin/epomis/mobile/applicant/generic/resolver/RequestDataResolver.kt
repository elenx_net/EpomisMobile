package epomis.mobile.applicant.generic.resolver

import epomis.mobile.applicant.generic.model.ApplyFormEntry
import epomis.service.connection6.request.DataEntry

internal interface RequestDataResolver
{
    fun resolveFormData(availableData: List<DataEntry>, formEntries: List<ApplyFormEntry>): List<DataEntry>
}