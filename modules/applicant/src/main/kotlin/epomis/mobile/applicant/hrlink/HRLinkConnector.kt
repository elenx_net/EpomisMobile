package epomis.mobile.applicant.hrlink

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.service.connection6.response.HtmlResponse

import java.util.Collections

internal class HRLinkConnector : HtmlApplicant
{
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = hasProperUrl(applicationForm) && hasNewStatus(applicationForm)

    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>) = applicationForm.ad.href

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>,
                                    currentResponse: HtmlResponse?): ApplicationForm<HtmlResponse>
    {
        val cookie = obtainSessionCookie(currentResponse!!.cookies)
        val shouldContinue = currentResponse.isOk()

        return applicationForm.copy(
            status = if (shouldContinue) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = when(cookie)
            {
                null -> HashMap()
                else -> Collections.singletonMap("PHPSESSID1", cookie)
            }
        )
    }

    private fun obtainSessionCookie(cookies: Map<String, String>) = cookies["PHPSESSID"]

    private fun hasProperUrl(applicationForm: ApplicationForm<*>) = containsDomain(applicationForm.ad.href)

    private fun containsDomain(href: String) = href.contains("hrlink.pl")

    private fun hasNewStatus(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.NEW
}
