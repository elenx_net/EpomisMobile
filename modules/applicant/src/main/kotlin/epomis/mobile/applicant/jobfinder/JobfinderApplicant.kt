package epomis.mobile.applicant.jobfinder

import epomis.mobile.applicant.HtmlApplicant
import epomis.service.connection6.request.DataEntry

import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document

internal class JobfinderApplicant: HtmlApplicant
{
    fun extractData(htmlFile: Document): Collection<DataEntry>
    {
        val firstHrefWithinApplyClassSelector =
            htmlFile
            .getElementsByClass("apply")
            .first()
            .child(0)
            .attr("href")

        return listOf(
            DataEntry("data[applyEmail]", extractEmail(firstHrefWithinApplyClassSelector)),
            DataEntry("data[referenceNumber]", extractReferenceNumber(firstHrefWithinApplyClassSelector))
        )
    }

    private fun extractReferenceNumber(firstHrefWithinApplyClassSelector: String)
        = StringUtils.substringBetween(firstHrefWithinApplyClassSelector, "Nr ref. ", ")")

    private fun extractEmail(firstHrefWithinApplyClassSelector: String)
        = StringUtils.substringBetween(firstHrefWithinApplyClassSelector, "mailto:", "?")
}
