package epomis.mobile.applicant.generic

import epomis.mobile.applicant.generic.provider.ApplyFormProvider
import epomis.mobile.applicant.generic.provider.ApplyFormProviderImpl
import epomis.mobile.applicant.generic.provider.ApplyFormExtractor
import epomis.mobile.applicant.generic.resolver.NameComparingResolver
import epomis.mobile.applicant.generic.resolver.RequestDataResolver
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val genericApplicantModule = Kodein.Module("genericApplicant")
{
    bind<RequestDataResolver>() with singleton { NameComparingResolver() }

    bind<ApplyFormExtractor>() with singleton { ApplyFormExtractor() }
    bind<ApplyFormProvider>() with singleton { ApplyFormProviderImpl(instance(), instance()) }
}