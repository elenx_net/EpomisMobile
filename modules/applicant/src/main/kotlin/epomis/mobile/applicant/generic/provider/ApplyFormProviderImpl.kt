package epomis.mobile.applicant.generic.provider

import epomis.mobile.applicant.generic.model.ApplyForm
import epomis.mobile.applicant.generic.model.ApplyFormEntry
import epomis.service.connection6.request.DataEntry
import epomis.service.url.UrlValidator

import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

import java.util.stream.Collectors

private const val ATTR_TYPE = "type"
private const val ATTR_VALUE = "value"
private const val ATTR_ACTION = "action"
private const val FILE = "file"
private const val NAME = "name"
private const val TAG_OPTION = "option"

internal class ApplyFormProviderImpl(private val applyFormExtractor: ApplyFormExtractor,
                            private val urlValidator: UrlValidator) : ApplyFormProvider
{
    private val inputTags = listOf("input", "textarea", "select")

    override fun extractApplyFormFrom(document: Document): ApplyForm
    {
        val applyForm = applyFormExtractor.extractFrom(document)
        val applyUrl = urlValidator.makeUrl(document.baseUri(), applyForm.attr(ATTR_ACTION))

        return ApplyForm(formData = extractFormData(applyForm), url = applyUrl)
    }

    private fun extractFormData(applyForm: Element)
        = applyForm
            .allElements
            .stream()
            .filter(this::isInputTag)
            .filter(this::isNotHiddenAndEmpty)
            .map(this::asApplyFormEntry)
            .filter{ it.data.key.isNotEmpty() }
            .collect(Collectors.groupingBy { applyFormEntry: ApplyFormEntry -> applyFormEntry.data.key })
            .entries
            .stream()
            .map(this::anyNotEmptyOrFirst)
            .collect(Collectors.toList())

    private fun isInputTag(element: Element) = inputTags.contains(element.tagName())

    private fun anyNotEmptyOrFirst(stringListEntry: Map.Entry<String, List<ApplyFormEntry>>)
        = stringListEntry
            .value
            .stream()
            .filter { it.data.value.isNotEmpty() }
            .findAny()
            .orElse(stringListEntry.value[0])

    private fun isNotHiddenAndEmpty(element: Element)
        = !("hidden".equals(element.attr(ATTR_TYPE), ignoreCase = true)
        && StringUtils.EMPTY.equals(element.attr(ATTR_VALUE), ignoreCase = true))

    private fun asApplyFormEntry(element: Element)
        = ApplyFormEntry(
            isFile = element.attr(ATTR_TYPE) == FILE,
            data = DataEntry(element.attr(NAME), valueFor(element))
        )

    private fun valueFor(element: Element): String
    {
        val tagName = element.tagName()

        return when
        {
            elementIsTextArea(tagName) -> element.text()
            elementIsInput(tagName) -> if (checkBoxIsSelected(element)) "on" else element.attr(ATTR_VALUE)
            elementIsSelect(tagName) -> firstSelectOption(element) else -> StringUtils.EMPTY
        }
    }

    private fun checkBoxIsSelected(element: Element)
        = "checkbox".equals(element.attr(ATTR_TYPE), ignoreCase = true) && element.attr(ATTR_VALUE).isEmpty()

    private fun elementIsTextArea(tagName: String)
        = "textarea".equals(tagName, ignoreCase = true)

    private fun elementIsInput(tagName: String)
        = "input".equals(tagName, ignoreCase = true)

    private fun elementIsSelect(tagName: String)
        = "select".equals(tagName, ignoreCase = true)

    private fun firstSelectOption(element: Element)
        = element
            .getElementsByTag(TAG_OPTION)
            .first()
            .attr(ATTR_VALUE)
}