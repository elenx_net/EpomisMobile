package epomis.mobile.applicant.generic.resolver

import epomis.mobile.applicant.generic.model.ApplyFormEntry
import epomis.service.connection6.request.DataEntry

internal data class SimilarityEntry(val formEntry: ApplyFormEntry,
                           val availableData: DataEntry,
                           val similarity: Float)
