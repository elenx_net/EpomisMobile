package epomis.mobile.applicant

import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.JsonResponse
import org.reactivestreams.Publisher

internal interface JsonApplicant<T> : Applicant<JsonResponse<T>>
{
    val jsonClass: Class<T>

    override fun send(connectionService6: ConnectionService6, connectionRequest: ConnectionRequest): Publisher<JsonResponse<T>>
        = connectionService6.reactivePostForJson(connectionRequest, jsonClass)

    override fun advanceApplication(applicationForm: ApplicationForm<JsonResponse<T>>,
                                    currentResponse: JsonResponse<T>?): ApplicationForm<JsonResponse<T>>
    {
        val httpResponse = currentResponse?.httpResponse
        val isSuccessful = httpResponse?.statusCode == 200 && httpResponse.statusMessage.contains("OK")

        return applicationForm.copy(
            status = if (isSuccessful) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse
        )
    }

    override fun previousResponseCookies(previousResponse: JsonResponse<T>?): Map<String, String>
        = previousResponse?.cookies ?: emptyMap()
}
