package epomis.mobile.applicant.generic.provider

import epomis.mobile.applicant.generic.model.ApplyForm
import org.jsoup.nodes.Document

internal interface ApplyFormProvider
{
    fun extractApplyFormFrom(document: Document): ApplyForm
}
