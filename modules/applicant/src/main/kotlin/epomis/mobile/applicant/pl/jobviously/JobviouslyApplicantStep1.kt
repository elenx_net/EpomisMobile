package epomis.mobile.applicant.pl.jobviously

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.DEFAULT_MESSAGE
import epomis.mobile.applicant.JsonApplicant
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.JsonResponse

private const val REQUIRED_APPLICATION_HEADER = "X-XSRF-TOKEN"
private const val REQUIRED_APPLICATION_COOKIE = "XSRF-TOKEN"

private const val APPLICATION_URL_SUFFIX = "/apps"

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class JobviouslyStep1Response(var message: String?)

internal class JobviouslyApplicantStep1 : JsonApplicant<JobviouslyStep1Response> {

    override val jsonClass = JobviouslyStep1Response::class.java

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
            applicationForm.status == ApplicationForm.Status.IN_PROGRESS

    override fun constantDataEntries() = listOf(
            DataEntry("personal_data", "true"),
            DataEntry("regulations", "true")
    )

    override fun userDataEntries(userResume: UserResume): List<DataEntry> {
        TODO("When the jobviously webiste wont return 500 error when sending application with CV, implement this method and fix unit test")
        return listOf(
                DataEntry("name", "${userResume["firstName"]} ${userResume["lastName"]}"),
                DataEntry("email", userResume["email"]),
                DataEntry("message", DEFAULT_MESSAGE),
                DataEntry("attachments[]", "", userResume.cv.inputStream),
                DataEntry("attachmentTitles[]", userResume.cv.fileName)
        )
    }

    override fun urlFor(applicationForm: ApplicationForm<JsonResponse<JobviouslyStep1Response>>) =
            applicationForm.ad.href + APPLICATION_URL_SUFFIX

    override fun previousResponseHeaders(previousResponse: JsonResponse<JobviouslyStep1Response>?) =
            mapOf(REQUIRED_APPLICATION_HEADER to extractTokenFromCookies(previousResponse))

    override fun advanceApplication(applicationForm: ApplicationForm<JsonResponse<JobviouslyStep1Response>>, currentResponse: JsonResponse<JobviouslyStep1Response>?) =
            applicationForm.copy(status = receiveApplicationStatus(currentResponse))

    private fun receiveApplicationStatus(currentResponse: JsonResponse<JobviouslyStep1Response>?) =
            if (isApplicationSuccessful(currentResponse)) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE

    private fun isApplicationSuccessful(response: JsonResponse<JobviouslyStep1Response>?) =
            response?.httpResponse?.statusCode == 201

    private fun extractTokenFromCookies(previousResponse: JsonResponse<JobviouslyStep1Response>?)
            = previousResponse?.cookies
                ?.get(REQUIRED_APPLICATION_COOKIE)?.replace("%3D", "=").orEmpty()
}