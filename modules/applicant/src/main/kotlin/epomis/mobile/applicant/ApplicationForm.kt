package epomis.mobile.applicant

import epomis.mobile.model.Ad
import epomis.mobile.resume.UserResume
import java.io.Serializable

import java.util.HashMap

internal data class ApplicationForm<out RESPONSE>(val ad: Ad,
                                         val baseDomain: String = "",
                                         val status: ApplicationForm.Status = ApplicationForm.Status.NEW,
                                         val throwable: Throwable? = null,
                                         @Transient val userResume: UserResume,
                                         @Transient val previousResponse: RESPONSE? = null,
                                         @Transient val customData: Map<String, String> = HashMap()) : Serializable
{
    /**
     * EXCEPTION -> set by ApplicantManager when Applicant throws any RuntimeException,
     * it means Applicant failed to apply for offer
     *
     * DEFAULT -> every Applicant sets DEFAULT Status that indicates that implementator forgot to override it,
     * it's treated as FAILURE to avoid infinite loop
     */
    enum class Status
    {
        NEW,
        IN_PROGRESS,
        FAILURE,
        SUCCESS,
        EXCEPTION,
        DEFAULT,
    }
}