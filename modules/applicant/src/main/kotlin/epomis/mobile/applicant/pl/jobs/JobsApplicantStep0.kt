package epomis.mobile.applicant.pl.jobs

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.select.Elements
import java.util.Locale

private const val JOBS_STEP = "jobs_step"
private const val NEXT_STEP = "0"
private const val SUITABLE_BASE_DOMAIN = "jobs"

private const val JOB_APPLY_SEND_EMAIL = "job_apply_send_email"

private const val AD_LOC_ID = "offer_loc_id"
private const val CLASS_AD_APPLY_NOW = "offer-apply-now"
private const val TAG_INPUT = "input"
private const val ATTR_NAME = "name"
private const val ATTR_VALUE = "value"

internal class JobsApplicantStep0 : HtmlApplicant
{
    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>) = applicationForm.ad.href

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
        hasNewStatus(applicationForm.status)
                && hasValidBaseDomain(applicationForm.baseDomain)

    private fun hasNewStatus(status: ApplicationForm.Status) = status == ApplicationForm.Status.NEW

    private fun hasValidBaseDomain(domain: String) = domain.toLowerCase(Locale.getDefault()) == SUITABLE_BASE_DOMAIN

    override fun advanceApplication(
        applicationForm: ApplicationForm<HtmlResponse>,
        currentResponse: HtmlResponse?
    ): ApplicationForm<HtmlResponse> =
        applicationForm.copy(
            status = if (currentResponse!!.isOk()) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = createCustomData(currentResponse)
        )

    private fun createCustomData(currentResponse: HtmlResponse): Map<String, String> = extractInputList(currentResponse)
        .let {
            mapOf(
                JOBS_STEP to NEXT_STEP,
                JOB_APPLY_SEND_EMAIL to extractJobApplySendEmail(it),
                AD_LOC_ID to extractAdLocId(it)
            )
        }

    private fun extractInputList(currentResponse: HtmlResponse) = currentResponse
        .document
        .getElementsByClass(CLASS_AD_APPLY_NOW)
        .firstOrNull()
        ?.getElementsByTag(TAG_INPUT)

    private fun extractJobApplySendEmail(adApplyNowInput: Elements?) = adApplyNowInput
        ?.firstOrNull { it.attr(ATTR_NAME) == JOB_APPLY_SEND_EMAIL }
        ?.attr(ATTR_VALUE)
        .orEmpty()

    private fun extractAdLocId(adApplyNowInput: Elements?) = adApplyNowInput
        ?.firstOrNull { it.attr(ATTR_NAME) == AD_LOC_ID }
        ?.attr(ATTR_VALUE)
        .orEmpty()
}