package epomis.mobile.applicant.pl.jobviously

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.JsonApplicant
import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.JsonResponse
import org.reactivestreams.Publisher
import java.util.Locale

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class JobviouslyStep0Response(var id: String?)

internal class JobviouslyApplicantStep0 : JsonApplicant<JobviouslyStep0Response> {
    override val jsonClass = JobviouslyStep0Response::class.java

    override fun send(connectionService6: ConnectionService6, connectionRequest: ConnectionRequest): Publisher<JsonResponse<JobviouslyStep0Response>> {
        return connectionService6.reactiveGetForJson(connectionRequest, jsonClass)
    }

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
            hasNewStatus(applicationForm.status) && hasValidBaseDomain(applicationForm.baseDomain)

    override fun advanceApplication(applicationForm: ApplicationForm<JsonResponse<JobviouslyStep0Response>>,
                                    currentResponse: JsonResponse<JobviouslyStep0Response>?)
         = applicationForm.copy(
                status = if (isSuccessful(currentResponse)) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
                previousResponse = currentResponse)

    override fun urlFor(applicationForm: ApplicationForm<JsonResponse<JobviouslyStep0Response>>) = applicationForm.ad.href

    private fun isSuccessful(currentResponse: JsonResponse<JobviouslyStep0Response>?)
        = currentResponse?.connectionResponse?.httpResponse?.statusCode == 200
        && currentResponse.json.id != null

    private fun hasNewStatus(status: ApplicationForm.Status) =
            status == ApplicationForm.Status.NEW

    private fun hasValidBaseDomain(baseDomain: String) =
            baseDomain.toLowerCase(Locale.getDefault()) == "jobviously"
}