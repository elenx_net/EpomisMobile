package epomis.mobile.applicant.pl.pracuj

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.applicant.cookie.CookieCollector
import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.HtmlResponse
import java.util.regex.Pattern

internal const val PRACUJPL_AD_ID = "PRACUJPL_OFFERID"
internal const val PRACUJPL_STEP = "PRACUJPL_STEP"
internal const val PRACUJPL_DOMAIN = "pracuj"
internal const val PRACUJPL_COOKIE_KEY = "PRACUJPL_COOKIE_"
internal val PRACUJPL_AD_ID_PATTERN = Pattern.compile(""",(\d{5,10})""")!!

internal class PracujApplicantStep0(private val cookieCollector: CookieCollector) : HtmlApplicant
{
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = isBaseDomain(applicationForm) && hasNewStatus(applicationForm) && isAdUrl(applicationForm)

    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>)
        = applicationForm.ad.href.replace("praca", "aplikuj") + "?awl=true"

    override fun send(connectionService6: ConnectionService6, connectionRequest: ConnectionRequest)
        = connectionService6.reactiveGetForHtml(connectionRequest)

    override fun cookiesFor(applicationForm: ApplicationForm<HtmlResponse>)
        = cookieCollector.readCookiesFromCustomData(applicationForm)

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse?): ApplicationForm<HtmlResponse>
    {
        return applicationForm.copy(
            status = if(isSuccessful(currentResponse!!)) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = createCustomData(applicationForm, currentResponse)
        )
    }

    private fun isBaseDomain(applicationForm: ApplicationForm<*>)
        = applicationForm.baseDomain == PRACUJPL_DOMAIN
    private fun hasNewStatus(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.NEW
    private fun isAdUrl(applicationForm: ApplicationForm<*>)
        = applicationForm.ad.href.contains("/praca/") && applicationForm.ad.href.any { it.isDigit() }

    private fun isSuccessful(currentResponse: HtmlResponse)
        = currentResponse.document.getElementsByClass("AdWarning_header").none()

    private fun createCustomData(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse)
        = mapOf(PRACUJPL_STEP to "1", PRACUJPL_AD_ID to obtainAdIdFromUrl(applicationForm.ad.href)) +
            cookieCollector.saveCookiesToCustomData(applicationForm, currentResponse)
    private fun obtainAdIdFromUrl(url: String)
        = PRACUJPL_AD_ID_PATTERN
        .matcher(url)
        .apply { find() }
        .group(1)
}