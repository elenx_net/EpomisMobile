package epomis.mobile.applicant.generic.model

import epomis.service.connection6.request.DataEntry

internal data class ApplyFormEntry(val isFile: Boolean,
                          val data: DataEntry)
