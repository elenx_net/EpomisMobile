package epomis.mobile.applicant.hrlink

import com.google.common.collect.ImmutableList

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.ArrayUtils
import org.apache.http.NameValuePair
import org.apache.http.client.utils.URIBuilder

import java.util.Collections
import java.util.stream.Collectors
import java.util.stream.Collectors.collectingAndThen

private val requiredParameters = ArrayUtils.toArray("sv", "page_typ", "page", "module", "typ")
private const val SESSION_COOKIE_FROM_PREVIOUS_STEP = "PHPSESSID2"

internal class HRLinkApplicant : HtmlApplicant
{
    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = hasProperUrl(applicationForm) && hasInProgressStatus(applicationForm) && hasSessionCookieId(applicationForm)

    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>)
        = createCvUploadUrl(applicationForm.ad.href)

    override fun userDataEntries(userResume: UserResume)
        = listOf(
            DataEntry("action", "1"),
            DataEntry("filereq", "1")
        )

    override fun cookiesFor(applicationForm: ApplicationForm<HtmlResponse>)
        = Collections.singletonMap<String, String>("PHPSESSID",
        applicationForm.customData[SESSION_COOKIE_FROM_PREVIOUS_STEP])

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse?)
        = applicationForm.copy(
            status = if (currentResponse!!.isFound()) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE
        )

    private fun createCvUploadUrl(url: String): String
    {
        val requestUrl = URIBuilder(url)

        val urlParameters = requestUrl
            .queryParams
            .stream()
            .filter(this::isRequiredParameter)
            .collect(collectingAndThen(Collectors.toList())
            { elements: List<NameValuePair> -> ImmutableList.copyOf(elements) })

        return requestUrl
            .setParameters(urlParameters)
            .build()
            .toString()
    }

    private fun isRequiredParameter(nameValuePair: NameValuePair)
        = ArrayUtils.contains(requiredParameters, nameValuePair.name)

    private fun hasProperUrl(applicationForm: ApplicationForm<*>)
        = containsDomain(applicationForm.ad.href)

    private fun containsDomain(href: String) = href.contains("hrlink.pl")

    private fun hasSessionCookieId(applicationForm: ApplicationForm<*>)
        = applicationForm.customData.containsKey(SESSION_COOKIE_FROM_PREVIOUS_STEP)

    private fun hasInProgressStatus(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.IN_PROGRESS
}
