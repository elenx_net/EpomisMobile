package epomis.mobile.applicant.pl.praca

import com.google.common.net.HttpHeaders
import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.JsonApplicant
import epomis.mobile.applicant.pl.praca.model.PracaResponse
import epomis.mobile.model.Ad
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.JsonResponse
import java.text.SimpleDateFormat
import java.util.*

private val DATE = SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().time)
private const val PRACA_STEP = "praca.pl_step"

private const val MESSAGE =
    "Szanowni Państwo:\n" +
        "W odpowiedzi na Państwa ofertę pracy:\n" +
        "Stanowisko / tytuł: %S\n" +
        "Zgłoszoną w dniu: %S\n" +
        "\n" +
        "przesyłam swoją aplikację.\n" +
        "\n" +
        "Proszę o rozważenie mojej kandydatury na powyższe stanowisko."

internal class PracaApplicant : JsonApplicant<PracaResponse>
{
    override val jsonClass: Class<PracaResponse>
        get() = PracaResponse::class.java

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
        applicationForm.customData.containsKey(PRACA_STEP)
            && applicationForm.customData[PRACA_STEP].equals("0")

    override fun urlFor(applicationForm: ApplicationForm<JsonResponse<PracaResponse>>) = "https://www.praca.pl/ad/application_api/applyInternally.html"

    override fun advanceApplication(applicationForm: ApplicationForm<JsonResponse<PracaResponse>>,
                                    currentResponse: JsonResponse<PracaResponse>?) = applicationForm.copy(
        status = if (isSuccessful(currentResponse!!)) ApplicationForm.Status.SUCCESS else ApplicationForm.Status.FAILURE,
        previousResponse = currentResponse,
        customData = mapOf(PRACA_STEP to "1")
    )

    private fun isSuccessful(currentResponse: JsonResponse<PracaResponse>): Boolean
    {
        val httpResponse = currentResponse.httpResponse

        return httpResponse.statusCode == 200 &&
            httpResponse.statusMessage.contains("OK") &&
            currentResponse.json.status.equals("success", ignoreCase = true)
    }

    override fun constantDataEntries() = listOf(
        DataEntry("data[Consent][newAccount]", "0"),
        DataEntry("data[Consent][marketing]", "0"),
        DataEntry("data[Consent][storeFiles]", "0"),
        DataEntry("data[Consent][terms]", "1")
    )

    override fun adsDataEntries(ad: Ad): Collection<DataEntry>
    {
        val id = ad.href.substring(ad.href.indexOf('_') + 1, ad.href.indexOf(".html"))

        return listOf(
            DataEntry("data[Ad][id]", id),
            DataEntry("data[Employee][message]", String.format(MESSAGE, ad.title, DATE))
        )
    }

    override fun userDataEntries(userResume: UserResume) = listOf(
        DataEntry("data[Employee][firstname]", userResume["firstName"]),
        DataEntry("data[Employee][lastname]", userResume["lastName"]),
        DataEntry("data[Employee][email]", userResume["email"])
    )

    override fun previousResponseDataEntries(previousResponse: JsonResponse<PracaResponse>?): Collection<DataEntry> = Collections.singletonList(DataEntry("data[Employee][file][]", previousResponse!!.json.data.id.toString()))

    override fun constantHeaders(): Map<String, String> = Collections.singletonMap(HttpHeaders.X_REQUESTED_WITH, "XMLHttpRequest")
}
