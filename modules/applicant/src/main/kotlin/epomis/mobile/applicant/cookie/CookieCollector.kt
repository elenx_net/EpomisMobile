package epomis.mobile.applicant.cookie

import epomis.mobile.applicant.ApplicationForm
import epomis.service.connection6.response.HtmlResponse

/**
 * Component responsible for storing cookies between application steps.
 * Stores data in applicationForm.customData as map (name, value)
 * Cookie metadata is skipped because ConnectionService ignores it.
 * Fully interoperable with {@link SeleniumCookieCollector}
 */
internal class CookieCollector
{
    /**
     * Serialises cookies from currentResponse to customData map and joins them with cookies from previous step
     * To be used in applicant advanceApplication/createCustomData method
     *
     * @param applicationForm to extract cookies from previous step
     * @param currentResponse to extract new cookies
     * @return new cookies customData
     */
    fun saveCookiesToCustomData(applicationForm: ApplicationForm<HtmlResponse>, currentResponse: HtmlResponse): Map<String, String>
        = (readCookiesFromCustomData(applicationForm) + currentResponse.cookies)
            .mapKeys { obtainPrefix(applicationForm) + it.key }

    /**
     * Deserialises cookies from applicationForm.customData (if exist)
     * To be used in applicant cookiesFor(applicantForm) method
     */
    fun readCookiesFromCustomData(applicationForm: ApplicationForm<HtmlResponse>)
        = applicationForm.customData
            .filterKeys { it.startsWith(obtainPrefix(applicationForm)) }
            .mapKeys { it.key.removePrefix(obtainPrefix(applicationForm)) }

    private fun obtainPrefix(applicationForm: ApplicationForm<HtmlResponse>)
        = "COOKIE[${applicationForm.baseDomain}]_"
}