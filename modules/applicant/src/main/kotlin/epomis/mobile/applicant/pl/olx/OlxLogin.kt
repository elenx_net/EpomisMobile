package epomis.mobile.applicant.pl.olx

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.HtmlApplicant
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.HtmlResponse

import java.util.HashMap

private const val SUFFIX_OF_LOGIN_URL = "?ref%5B0%5D%5Baction%5D=myaccount&ref%5B0%5D%5Bmethod%5D=#login"

internal class OlxLogin : HtmlApplicant
{
    override fun urlFor(applicationForm: ApplicationForm<HtmlResponse>)
        = "https://www.olx.pl/konto/$SUFFIX_OF_LOGIN_URL"

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>)
        = hasProperUrl(applicationForm) && hasNewStatus(applicationForm)

    override fun userDataEntries(userResume: UserResume)
        = listOf(
            DataEntry("login[email]", userResume["login"]),
            DataEntry("login[password]", userResume["password"]),
            DataEntry("login[remember-me]", "on")
    )

    override fun advanceApplication(applicationForm: ApplicationForm<HtmlResponse>,
                                    currentResponse: HtmlResponse?): ApplicationForm<HtmlResponse>
    {
        return applicationForm.copy(
            status = if (currentResponse!!.isMovedPermanently()) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            previousResponse = currentResponse,
            customData = createCustomData(currentResponse)
        )
    }

    private fun hasProperUrl(applicationForm: ApplicationForm<*>)
        = containsDomain(applicationForm.ad.href)

    private fun containsDomain(href: String) = href.contains("olx.pl")

    private fun hasNewStatus(applicationForm: ApplicationForm<*>)
        = applicationForm.status == ApplicationForm.Status.NEW

    private fun createCustomData(currentResponse: HtmlResponse): Map<String, String>
    {
        val currentResponseCookies = currentResponse.cookies

        val customData = HashMap<String, String>()
        customData.apply {
            currentResponseCookies["PHPSESSID"]?.let { put("PHPSESSID1", it) }
            currentResponseCookies["user_id"]?.let { put("user_id1", it) }
            currentResponseCookies["access_token"]?.let { put("access_token1", it) }
            currentResponseCookies["remember_login"]?.let { put("remember_login1", it) }
            currentResponseCookies["refresh_token"]?.let { put("refresh_token1", it) }
        }

        return customData
    }
}