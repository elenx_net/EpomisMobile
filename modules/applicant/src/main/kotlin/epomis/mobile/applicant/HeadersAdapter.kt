package epomis.mobile.applicant

import epomis.mobile.model.Ad
import epomis.mobile.resume.UserResume
import java.util.HashMap

internal interface HeadersAdapter<in RESPONSE>
{
    fun headersFor(applicationForm: ApplicationForm<RESPONSE>): Map<String, String>
    {
        val headers = HashMap<String, String>()
        headers.apply {
            putAll(constantHeaders())
            putAll(adHeaders(applicationForm.ad))
            putAll(userDataHeaders(applicationForm.userResume))
            putAll(previousResponseHeaders(applicationForm.previousResponse))
        }

        return headers
    }

    fun constantHeaders(): Map<String, String> = emptyMap()

    fun adHeaders(ad: Ad): Map<String, String> = emptyMap()

    fun userDataHeaders(userResume: UserResume): Map<String, String> = emptyMap()

    fun previousResponseHeaders(previousResponse: RESPONSE?): Map<String, String> = emptyMap()
}
