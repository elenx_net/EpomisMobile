package epomis.mobile.applicant.pl.jober

import epomis.mobile.applicant.ApplicationForm
import epomis.mobile.applicant.JsonApplicant
import epomis.mobile.applicant.pl.jober.model.JoberStep0Response
import epomis.mobile.resume.UserResume
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.response.JsonResponse
import org.apache.commons.lang3.StringUtils

private const val URL = "https://www.jober.pl/?c=apply&a=uploadCV"

private const val STEP = "jober.pl_step"
private const val NEXT_STEP = "0"

private const val CONTENT_TYPE = "Content-Type"
private const val TMP_CV = "tmpCV"
private const val FILE = "file"

private const val CONTENT_TYPE_VALUE = "multipart/form-data"

internal class JoberApplicantStep0 : JsonApplicant<JoberStep0Response>
{
    override val jsonClass = JoberStep0Response::class.java

    override fun urlFor(applicationForm: ApplicationForm<JsonResponse<JoberStep0Response>>) = URL

    override fun isAppropriateFor(applicationForm: ApplicationForm<*>) =
        hasNewStatus(applicationForm.status) && hasValidBaseDomain(applicationForm.baseDomain)

    override fun constantHeaders() = mapOf(CONTENT_TYPE to CONTENT_TYPE_VALUE)

    override fun userDataEntries(userResume: UserResume) =
        listOf(DataEntry(FILE, StringUtils.EMPTY, userResume.cv.inputStream))

    override fun advanceApplication(applicationForm: ApplicationForm<JsonResponse<JoberStep0Response>>,
                                    currentResponse: JsonResponse<JoberStep0Response>?) =
        applicationForm.copy(
            status = if (currentResponse!!.json.error == null) ApplicationForm.Status.IN_PROGRESS else ApplicationForm.Status.FAILURE,
            customData = createCustomData(currentResponse)
        )

    private fun hasNewStatus(status: ApplicationForm.Status) =
        status == ApplicationForm.Status.NEW

    private fun hasValidBaseDomain(baseDomain: String) =
        baseDomain.toLowerCase() == "jober"

    private fun createCustomData(currentResponse: JsonResponse<JoberStep0Response>) =
        mapOf(STEP to NEXT_STEP, TMP_CV to currentResponse.json.tmpFile!!)
}
