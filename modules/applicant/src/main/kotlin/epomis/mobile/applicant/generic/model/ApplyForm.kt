package epomis.mobile.applicant.generic.model

internal class ApplyForm(val url: String,
                val formData: List<ApplyFormEntry>)
{
    fun emptyFieldsCount() = formData
        .stream()
        .filter(this::notFileAndEmpty)
        .count()

    private fun notFileAndEmpty(applyFormEntry: ApplyFormEntry)
        = !applyFormEntry.isFile && applyFormEntry.data.value.isEmpty()
}