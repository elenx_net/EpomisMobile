package epomis.mobile

import epomis.connectionModule
import epomis.mobile.applicant.Applicant
import epomis.mobile.applicant.cookie.CookieCollector
import epomis.mobile.applicant.pl.praca.PracaApplicant
import epomis.mobile.applicant.pl.praca.PracaCvUploader
import epomis.mobile.manager.applicantManagerModule
import epomis.utilsModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.inSet
import org.kodein.di.generic.setBinding
import org.kodein.di.generic.singleton

val applicantModule = Kodein.Module("applicant")
{
    /** MODULES **/
    importOnce(utilsModule, allowOverride = true)
    importOnce(connectionModule, allowOverride = true)

    /** APPLICANTS **/
    bind<CookieCollector>() with singleton { CookieCollector() }

    bind() from setBinding<Applicant<*>>()

    bind<Applicant<*>>().inSet() with singleton { PracaCvUploader() }
    bind<Applicant<*>>().inSet() with singleton { PracaApplicant() }

//    bind<Applicant<*>>().inSet() with singleton { JobsApplicantStep0() }
//    bind<Applicant<*>>().inSet() with singleton { JobsApplicantStep1() }
//    bind<Applicant<*>>().inSet() with singleton { JobsApplicantStep2() }

//    Waits until website wont return error 500 while sending the application
//    bind<Applicant<*>>().inSet() with singleton { JobviouslyApplicantStep0() }
//    bind<Applicant<*>>().inSet() with singleton { JobviouslyApplicantStep1() }

    importOnce(applicantManagerModule)
}