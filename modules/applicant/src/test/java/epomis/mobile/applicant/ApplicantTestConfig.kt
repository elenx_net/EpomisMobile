package epomis.mobile.applicant

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.api.client.http.HttpHeaders
import com.google.api.client.http.HttpResponse
import epomis.mobile.resume.BasicUserResume
import epomis.service.connection6.response.ConnectionResponse
import epomis.service.connection6.response.HtmlResponse
import epomis.service.connection6.response.JsonResponse
import org.jsoup.Jsoup
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.io.FileNotFoundException

internal class ApplicantTestConfig(private val testClass: Class<*>)
{
    val applicantModule by lazy { Kodein.direct { import(epomis.mobile.applicantModule, allowOverride = true) } }
    private val objectMapper = applicantModule.instance<ObjectMapper>()

    fun getApplicantsTestData(resourceFileName: String) =
        objectMapper.readValue<List<ApplicantTestData>>(testClass.getResourceAsStream(resourceFileName)!!)

    fun configureUserResume(userData: Map<String, String>): BasicUserResume = BasicUserResume(
        userData,
        "test input stream".byteInputStream()
    )

    fun configureResponse(applicant: Applicant<*>, responseData: ApplicantTestResponseData): Any
    {
        val connectionResponse = Mockito.mock(ConnectionResponse::class.java)
        configureConnectionResponseMocks(connectionResponse, responseData)

        return when (responseData.type)
        {
            "json" -> configureJsonResponse((applicant as JsonApplicant<*>).jsonClass, responseData, connectionResponse)
            "html" -> configureHtmlResponse(responseData, connectionResponse)
            else -> throw IllegalStateException("Only json and html types allowed")
        }
    }

    private fun configureJsonResponse(
        jsonClass: Class<*>,
        responseData: ApplicantTestResponseData,
        connectionResponse: ConnectionResponse
    ): JsonResponse<Any>
    {
        val json = loadJson(testClass, jsonClass, objectMapper, responseData.file)

        return JsonResponse(connectionResponse, json)
    }

    private fun loadJson(
        currentTestClass: Class<*>,
        jsonClass: Class<*>,
        objectMapper: ObjectMapper,
        resourceFileName: String
    ): Any
    {
        val resource = currentTestClass.getResourceAsStream(resourceFileName)
            ?: throw FileNotFoundException("Test response file not found: $resourceFileName")
        return objectMapper.readValue(resource, jsonClass)
    }

    private fun configureHtmlResponse(
        responseData: ApplicantTestResponseData,
        connectionResponse: ConnectionResponse
    ): HtmlResponse
    {
        val document = loadDocument(testClass, responseData.file)

        return HtmlResponse(connectionResponse, document)
    }

    private fun loadDocument(
        currentTestClass: Class<*>,
        resourceFileName: String
    ) = currentTestClass.getResourceAsStream(resourceFileName)?.use {
        Jsoup.parse(it, "UTF-8", resourceFileName)
    } ?: throw FileNotFoundException("Test response file not found: $resourceFileName")

    private fun configureConnectionResponseMocks(
        connectionResponse: ConnectionResponse,
        responseData: ApplicantTestResponseData
    )
    {
        val httpResponse = Mockito.mock(HttpResponse::class.java)

        `when`(connectionResponse.cookies).thenReturn(responseData.cookies ?: mapOf())
        `when`(connectionResponse.httpResponse).thenReturn(httpResponse)
        `when`(connectionResponse.httpResponse.statusCode).thenReturn(responseData.code)
        `when`(connectionResponse.httpResponse.headers).thenReturn(
            convertToHttpHeaders(
                responseData.headers
                    ?: mapOf()
            )
        )
    }

    private fun convertToHttpHeaders(headers: Map<String, String>): HttpHeaders
    {
        val httpHeaders = HttpHeaders()
        headers.forEach { httpHeaders.set(it.key, it.value) }
        return httpHeaders
    }
}