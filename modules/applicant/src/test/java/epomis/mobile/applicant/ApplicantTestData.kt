package epomis.mobile.applicant

import epomis.mobile.model.Ad
import org.mockito.Mockito

internal data class ApplicantTestData(val className: String,
                                      val disabled: Boolean?,
                                      val expected: ApplicantTestExpectedData,
                                      val provided: ApplicantTestProvidedData)

internal data class ApplicantTestExpectedData(val applicationFormJson: ApplicationFormJson,
                                              val url: String,
                                              val headers: Map<String, String>?,
                                              val cookies: Map<String, String>?,
                                              val dataEntries: Map<String, String>?)

internal data class ApplicantTestProvidedData(val response: ApplicantTestResponseData,
                                              val applicationFormJson: ApplicationFormJson)

internal data class ApplicantTestResponseData(val file: String,
                                              val type: String,
                                              val code: Int,
                                              val headers: Map<String, String>?,
                                              val cookies: Map<String, String>?)

internal data class ApplicationFormJson(val ad: Ad?,
                                        val baseDomain: String = "",
                                        val status: ApplicationForm.Status,
                                        val userData: Map<String, String>?,
                                        val customData: Map<String, String>?,
                                        val previousResponse: ApplicantTestResponseData?
)

internal object ApplicationFormJsonConverter
{
    fun jsonToApplicationForm(applicationFormJson: ApplicationFormJson,
                              applicantTestConfig: ApplicantTestConfig,
                              applicant: Applicant<*>): ApplicationForm<Any>
    {
        val userResume = applicantTestConfig.configureUserResume(applicationFormJson.userData ?: emptyMap())
        val previousResponse = preparePreviousResponse(applicationFormJson, applicantTestConfig, applicant)

        return ApplicationForm(
            ad = applicationFormJson.ad ?: Mockito.mock(Ad::class.java),
            baseDomain = applicationFormJson.baseDomain,
            status = applicationFormJson.status,
            userResume = userResume,
            previousResponse = previousResponse,
            customData = applicationFormJson.customData ?: mapOf())
    }

    private fun preparePreviousResponse(applicationFormJson: ApplicationFormJson,
                                        applicantTestConfig: ApplicantTestConfig,
                                        applicant: Applicant<*>) =
        if (applicationFormJson.previousResponse != null)
        {
            applicantTestConfig.configureResponse(applicant, applicationFormJson.previousResponse)
        }
        else
        {
            null
        }
}