package epomis.mobile.applicant

import mu.KotlinLogging

import org.kodein.di.generic.instance
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

import kotlin.test.assertEquals
import kotlin.test.assertTrue

private val logger = KotlinLogging.logger("ApplicantTest")

class ApplicantTest : Spek(
    {
        val applicantTestConfig = ApplicantTestConfig(ApplicantTest::class.java)
        val applicants = applicantTestConfig.applicantModule.instance<Set<Applicant<*>>>()
        val applicantsTestData = applicantTestConfig.getApplicantsTestData("applicantsTestData.json")

        describe("Applicants")
        {
            for (applicant in applicants)
            {
                val className = applicant::class.qualifiedName
                val simpleName = applicant::class.simpleName
                val testData = applicantsTestData.find { it.className == className }
                if (testData == null)
                    logger.warn { "No test data found for $simpleName" }
                else if (testData.disabled != null && testData.disabled)
                    logger.info { "Test disabled for $simpleName" }
                else
                {
                    context("Applicant: $simpleName")
                    {
                        val response = applicantTestConfig.configureResponse(
                            applicant,
                            testData.provided.response
                        )
                        val expectedApplicationForm = ApplicationFormJsonConverter.jsonToApplicationForm(testData.expected.applicationFormJson, applicantTestConfig, applicant)
                        val providedApplicationForm = ApplicationFormJsonConverter.jsonToApplicationForm(testData.provided.applicationFormJson, applicantTestConfig, applicant)

                        @Suppress("UNCHECKED_CAST")
                        val castedApplicant = applicant as Applicant<Any>

                        val isAppropriate = castedApplicant.isAppropriateFor(providedApplicationForm)
                        val url = castedApplicant.urlFor(providedApplicationForm)
                        val cookies = castedApplicant.cookiesFor(providedApplicationForm)
                        val headers = castedApplicant.headersFor(providedApplicationForm)
                        val dataEntries = castedApplicant.dataEntriesFor(providedApplicationForm).map { it.key to it.value }.toMap()
                        val advanceApplication = castedApplicant.advanceApplication(providedApplicationForm, response)

                        it("should return valid isAppropriate") {
                            assertTrue(isAppropriate)
                        }
                        it("should return valid url") {
                            assertEquals(testData.expected.url, url)
                        }
                        it("should return valid cookies") {
                            assertTrue(testData.expected.cookies?.isSubMapOf(cookies) ?: true)
                        }
                        it("should return valid headers") {
                            assertTrue(testData.expected.headers?.isSubMapOf(headers) ?: true)
                        }
                        it("should return valid dataEntries") {
                            assertTrue(testData.expected.dataEntries?.isSubMapOf(dataEntries) ?: true)
                        }
                        it("should return valid advanceApplication customData") {
                            assertTrue(expectedApplicationForm.customData.isSubMapOf(advanceApplication.customData))
                        }
                        it("should return valid advanceApplication status") {
                            assertEquals(expectedApplicationForm.status, advanceApplication.status)
                        }
                        it("should return valid advanceApplication baseDomain") {
                            assertEquals(expectedApplicationForm.baseDomain, advanceApplication.baseDomain)
                        }
                    }
                }
            }
        }
    }
)

private fun <K, V> Map<K, V>.isSubMapOf(map: Map<K, V>) =
    this.all { map.containsKey(it.key) && map[it.key] == it.value }
