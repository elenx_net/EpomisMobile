plugins {
    kotlin
    kodein
    rx
    coroutines
    test
}

dependencies {
    implementation(project(":modules:connection"))
    implementation(project(":modules:utils"))
    implementation(project(":modules:model"))

    implementation("io.github.microutils:kotlin-logging:1.7.9")
    implementation("commons-io:commons-io:2.6")
}