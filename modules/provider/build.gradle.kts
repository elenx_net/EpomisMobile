plugins {
    kotlin
    kodein
    rx
    test
}

dependencies {
    implementation(project(":modules:connection"))
    implementation(project(":modules:utils"))
    implementation(project(":modules:model"))

    implementation("io.github.microutils:kotlin-logging:1.7.9")
    implementation("io.vavr:vavr:0.9.3")
}