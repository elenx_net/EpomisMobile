package epomis.mobile.provider

import epomis.mobile.providerModule
import epomis.mobile.model.AdFilter
import epomis.service.connection6.ConnectionService6
import mu.KotlinLogging
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertFalse
import kotlin.test.assertTrue

private val logger = KotlinLogging.logger("IntegrationTest")

private class ProviderFailureData(var simpleName: String,
                                  var stackTrace: Array<StackTraceElement>? = null,
                                  var message: String? = null,
                                  var returnedUrl: String? = null,
                                  var returnedAdsSize: Int? = null,
                                  var returnedFurthestKnownPageNumber: Int? = null)

@RunWith(JUnitPlatform::class)
class ProviderIntegrationTest : Spek
    ({
    val kodein = Kodein.direct {
        importOnce(providerModule, allowOverride = true)
    }

    val connectionService: ConnectionService6 = kodein.instance()
    val providers: Set<GenericAdProvider<*>> = kodein.instance()
    val filters = listOf(AdFilter("", ""), AdFilter("developer", ""))
    for(filter in filters)
    {
        val failedProviders = mutableListOf<ProviderFailureData>()
        describe("Querying all providers with filter: ${filter.keyword} ${filter.location}")
        {
            for (provider in providers)
            {
                val simpleName = provider::class.simpleName!!
                context("Provider: $simpleName")
                {
                    val failureData = ProviderFailureData(simpleName)
                    try
                    {
                        val httpMethod = provider.methodFor(filter.keyword, filter.location, 1)
                        val connectionRequest = provider.requestFor(filter.keyword, filter.location, 1)

                        @Suppress("UNCHECKED_CAST")
                        val castedProvider = provider as GenericAdProvider<Any>

                        val response = castedProvider.send(connectionRequest, connectionService, httpMethod).blockingGet()
                        val ads = castedProvider.extractAdsFrom(response)
                        val adsSize = ads.size
                        failureData.returnedAdsSize = adsSize
                        val furthestKnownPageNumber = castedProvider.furthestKnownPageNumber(response)
                        failureData.returnedFurthestKnownPageNumber = furthestKnownPageNumber

                        val adsSizeCorrect = adsSize > 0
                        val furthestKnownPageNumberCorrect = furthestKnownPageNumber > 0
                        val adsTitleFailure = ads.any { it.title.trim().isEmpty() }

                        if (!adsSizeCorrect || !furthestKnownPageNumberCorrect)
                            failedProviders.add(failureData)

                        it("$simpleName should extract any ads") { assertTrue { adsSizeCorrect } }
                        it("$simpleName should correctly extract ads title") { assertFalse { adsTitleFailure } }
                        it("$simpleName should extract any page number") { assertTrue { furthestKnownPageNumberCorrect } }
                    }
                    catch (e: Exception)
                    {
                        failedProviders.add(failureData.apply { stackTrace = e.stackTrace; message = e.message })
                        throw e
                    }
                }
            }
        }
        afterGroup {
            logger.info { "***PROVIDER TEST RESULTS***" }
            logger.info { "With query AdFilter: keyword=${filter.keyword}, location=${filter.location}"}
            logger.info { "${providers.size - failedProviders.size} SUCCEDED, ${failedProviders.size} FAILED" }
            for (failure in failedProviders)
            {
                if (failure.stackTrace == null)
                {
                    logger.info { "Provider ${failure.simpleName} FAILED: Soft fail (couldn't extract any ad or pagination)" }
                }
                else
                {
                    logger.info { "Provider ${failure.simpleName} FAILED: Hard fail (thrown exception): ${failure.message}" }
                }
                logger.info {
                    "Returned state: url=${failure.returnedUrl}, adsSize=${failure.returnedAdsSize}, " +
                            "furthestKnownPageNumber=${failure.returnedFurthestKnownPageNumber}"
                }

            }
        }
    }
})