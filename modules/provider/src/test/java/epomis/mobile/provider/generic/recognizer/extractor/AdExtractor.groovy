package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.AdFilter
import epomis.mobile.provider.generic.recognizer.AdMostFrequentSelectorRecognizer
import epomis.mobile.provider.generic.selector.CssSelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorSanitizerImpl
import epomis.service.url.UrlValidator
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import spock.lang.Specification

class AdExtractor extends Specification
{
    String adsPath = "/epomis/mobile/provider/generic/ads/"

    Map<String, Long> adsPerPage =
        ["amundio.html"         : 10,
         "bulldogjob.html"      : 3,
         "indeedCom.html"       : 18,
         "infoPraca.html"       : 18,
         "pracaMoney.html"      : 81,
         "pracaEgospodarka.html": 41,
         "adzuna.html"          : 8,
         "glassdoor.html"       : 7,
         "olx.html"             : 6,
         "jobfinder.html"       : 1,
         "jobdesk.html"         : 7]

    def "should find all ads on page"()
    {
        given:

            def selectorSanitizer = new SelectorSanitizerImpl()
            def selectorExtractor = new CssSelectorExtractor(selectorSanitizer)
            def adFacade = new GenericAdExtractorFacade(new TitleExtractor(), new LocationExtractor(), new HrefExtractor(new TitleExtractor(), new UrlValidator()))
            def recognizer = new AdMostFrequentSelectorRecognizer(selectorExtractor, adFacade, selectorSanitizer)

        def adFilter = new AdFilter("Java", "Warszawa")

            def countAds = { it -> recognizer.recognize(matchHtmlFile(it), adFilter).count().blockingGet() }

        when:

            Map<String, Long> result = adsPerPage.collectEntries { k, v -> [(k): countAds(k)] }

        then:

            result == adsPerPage
    }

    Document matchHtmlFile(String url)
    {
        String fileName = adsPerPage
            .keySet()
            .stream()
            .filter { key -> url.contains(key) }
            .findAny()
            .get()

        InputStream file = AdExtractor.getResourceAsStream(adsPath + fileName)

       return Jsoup.parse(file, "UTF-8", adsPath + fileName)
    }
}
