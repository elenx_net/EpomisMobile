package epomis.mobile.provider.generic.recognizer.selector

import epomis.mobile.provider.generic.selector.SelectorSanitizerImpl
import spock.lang.Specification

class SelectorSanitizerTest extends Specification
{
    void "test selector sanitizer"()
    {
        given:

            def selector = "#resultsWrapper > div.js_result_container.clearfix:nth-child(2) > article.js_result_row > div.jobTitle"
            def selectorSanitizer = new SelectorSanitizerImpl()

        when:

            def sanitizedSelector = selectorSanitizer.toGenericSelector(selector)

        then:

            sanitizedSelector == "id > div.js_result_container.clearfix:nth-child > article.js_result_row > div.jobTitle"
    }

}
