package epomis.mobile.provider.generic

import epomis.mobile.model.AdFilter
import epomis.mobile.provider.generic.recognizer.AdMostFrequentSelectorRecognizer
import epomis.mobile.provider.generic.recognizer.AdRecognizer
import epomis.mobile.provider.generic.recognizer.extractor.*
import epomis.mobile.provider.generic.selector.CssSelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorSanitizer
import epomis.mobile.provider.generic.selector.SelectorSanitizerImpl
import epomis.service.url.UrlValidator
import org.jsoup.Jsoup
import spock.lang.Shared
import spock.lang.Specification

class AdMostFrequentSelectorRecognizerTest extends Specification
{
    @Shared
    SelectorSanitizer selectorSanitizer = new SelectorSanitizerImpl()
    @Shared
    SelectorExtractor selectorExtractor = new CssSelectorExtractor(selectorSanitizer)
    @Shared
    GenericAdExtractor adFacade = new GenericAdExtractorFacade(
            new TitleExtractor(),
            new LocationExtractor(),
            new HrefExtractor(new TitleExtractor(), new UrlValidator())
    )
    @Shared
    AdRecognizer recognizer = new AdMostFrequentSelectorRecognizer(selectorExtractor, adFacade, selectorSanitizer)
    @Shared
    AdFilter filter = new AdFilter("developer", "warszawa")

    void "test job ad top selector recognizer"()
    {
        when:

            def resource = AdMostFrequentSelectorRecognizerTest.getResourceAsStream(htmlPath)
            def document = Jsoup.parse(resource, "UTF-8", htmlPath)

            def recognizeAdSize = recognizer.recognize(document, filter).count().blockingGet()

            resource.close()

        then:

            assert recognizeAdSize == adSize

        where:

            htmlPath                                                    | adSize

            "/epomis/mobile/provider/generic/ads/infoPraca.html"                | 12
            "/epomis/mobile/provider/generic/ads/indeedCom.html"                | 11
            "/epomis/mobile/provider/generic/ads/amundio.html"                  | 7
            "/epomis/mobile/provider/generic/ads/pracaEgospodarka.html"         | 39
            "/epomis/mobile/provider/generic/ads/jobfinder.html"                | 5
            "/epomis/mobile/provider/generic/ads/glassdoor.html"                | 14
            "/epomis/mobile/provider/generic/ads/olx.html"                      | 39
    }
}