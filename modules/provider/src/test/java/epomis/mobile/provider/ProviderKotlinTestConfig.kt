package epomis.mobile.provider

import com.fasterxml.jackson.databind.ObjectMapper
import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.response.HtmlResponse
import epomis.service.connection6.response.JsonResponse
import io.reactivex.Flowable
import org.jsoup.Jsoup
import org.kodein.di.Kodein
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.io.FileNotFoundException

class ProviderKotlinTestConfig
{
    val providerModule by lazy { Kodein.direct { import(epomis.mobile.providerModule, allowOverride = true) } }

    fun configureHtmlResponse(currentTestClass: Class<*>, resourceFileName: String): HtmlResponse
    {
        val document = loadDocument(currentTestClass, resourceFileName)
        val htmlResponse  = mock(HtmlResponse::class.java)
        `when`(htmlResponse.document).thenReturn(document)

        return htmlResponse
    }

    fun configureJsonResponse(objectMapper: ObjectMapper, currentTestClass: Class<*>, jsonClass: Class<*>, resourceFileName: String, cookies: Map<String, String>): JsonResponse<Any>
    {
        val resource = currentTestClass.getResourceAsStream(resourceFileName) ?: throw FileNotFoundException("Test input file not found: $resourceFileName")
        val json = objectMapper.readValue(resource, jsonClass)
        val jsonResponse = mock(JsonResponse::class.java)
        `when`(jsonResponse.json).thenReturn(json)
        `when`(jsonResponse.cookies).thenReturn(cookies)
        @Suppress("UNCHECKED_CAST")
        return jsonResponse as JsonResponse<Any>
    }

    fun configureConnectionService(currentTestClass: Class<*>, resourceFileName: String): ConnectionService6
    {
        val document = loadDocument(currentTestClass, resourceFileName)

        val connectionService = mock(ConnectionService6::class.java)
        val htmlResponse  = mock(HtmlResponse::class.java)

        `when`(htmlResponse.document).thenReturn(document)
        `when`(connectionService.reactiveGetForHtml(any())).thenReturn(Flowable.just(htmlResponse))

        return connectionService
    }

    private fun loadDocument(currentTestClass: Class<*>,resourceFileName: String)
            = currentTestClass.getResourceAsStream(resourceFileName)?.use {
                Jsoup.parse(it, "UTF-8", resourceFileName)
            } ?: throw FileNotFoundException("Test input file not found: $resourceFileName")
}