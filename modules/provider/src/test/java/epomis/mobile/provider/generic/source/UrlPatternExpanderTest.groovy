package epomis.mobile.provider.generic.source

import spock.lang.Specification

import java.util.stream.Collectors

class UrlPatternExpanderTest extends Specification
{
    void "test expanding url"()
    {
        given:

            def expander = new UrlPatternExpander()
            def testCases = [
                "www.oferty.pl?page=%s": ["www.oferty.pl?page=1", "www.oferty.pl?page=2", "www.oferty.pl?page=3"]
            ]

        when:

            def results = testCases
                .entrySet()
                .stream()
                .map { testCase -> testPattern(expander, testCase.key, testCase.value) }
                .collect(Collectors.toList())

        then:

            !results.contains(false)

    }

    boolean testPattern(UrlPatternExpander expander, def pattern, def exptectedResult)
    {
        return expander.expand(pattern, 1, 3).containsAll(exptectedResult)
    }
}
