package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.AdFilter
import epomis.service.url.UrlValidator
import io.reactivex.Maybe
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import spock.lang.Specification

import static org.mockito.ArgumentMatchers.any
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class ExtractorsTest extends Specification {
    void "test extractors"() {
        given:

        def elementInputStream = ExtractorsTest.class.getResourceAsStream("element.html")
        def element = Jsoup.parse(elementInputStream, "utf-8", "www.jobs.pl")

        AdFilter filterMock = new AdFilter("java", "warszawa")
            UrlValidator urlValidator = mock(UrlValidator)
            TitleExtractor titleMock = mock(TitleExtractor)

            when(titleMock.extract(any(), any())).thenReturn(Maybe.just("Junior Java Developer"))
            when(urlValidator.makeUrl(any(), any())).thenReturn("https://www.jobs.pl/junior-java-developer-oferta-1690915")

        when:

            def hrefPassed = testExtractor(new HrefExtractor(titleMock, urlValidator), filterMock, element, "https://www.jobs.pl/junior-java-developer-oferta-1690915")
            def titlePassed = testExtractor(new TitleExtractor(), filterMock, element, "Junior Java Developer")
            def locationPassed = testExtractor(new LocationExtractor(), filterMock, element, "Warszawa (mazowieckie)")

        then:

            titlePassed
            locationPassed
            hrefPassed
    }

    private static boolean testExtractor(InformationExtractor extractor, AdFilter filter, Element inputElement, String expectedOutput) {
        return extractor.extract(inputElement, filter).blockingGet() == expectedOutput
    }
}
