package epomis.mobile.provider

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

import mu.KotlinLogging

import org.kodein.di.generic.instance
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

import kotlin.test.assertEquals

private val logger = KotlinLogging.logger("ProviderTest")

private data class ProviderTestData(val className: String,
                                    val disabled: Boolean?,
                                    val type: String,
                                    val inputFile: String,
                                    val expectedUri: String,
                                    val expectedAdsSize: Int,
                                    val expectedFurthestKnownPageNumber: Int,
                                    val cookies: Map<String, String>?)

class ProviderTest : Spek
({
    val crawlerTestConfig = ProviderKotlinTestConfig()
    val providers = crawlerTestConfig.providerModule.instance<Set<GenericAdProvider<*>>>()
    val objectMapper = crawlerTestConfig.providerModule.instance<ObjectMapper>()
    val providerTestData = objectMapper.readValue<List<ProviderTestData>>(ProviderTest::class.java.getResourceAsStream("providersTestData.json"))

    describe("Ad Providers")
    {
        for(provider in providers)
        {
            val className = provider::class.qualifiedName
            val simpleName = provider::class.simpleName
            val testData = providerTestData.find { it.className == className }
            if(testData == null)
                logger.warn { "No test data found for $simpleName" }
            else if(testData.disabled != null && testData.disabled)
                logger.info { "Test disabled for $simpleName" }
            else
            {
                context("Provider: $simpleName")
                {
                    val response: Any = when (testData.type)
                    {
                        "json" -> crawlerTestConfig.configureJsonResponse(objectMapper,
                            ProviderTest::class.java,
                            (provider as JsonAdProvider<*>).getJsonClass(),
                            testData.inputFile,
                            testData.cookies ?: emptyMap())
                        "html" -> crawlerTestConfig.configureHtmlResponse(ProviderTest::class.java, testData.inputFile)
                        else -> throw IllegalStateException("Only json and html types allowed")
                    }

                    @Suppress("UNCHECKED_CAST")
                    val castedProvider = provider as GenericAdProvider<Any>

                    val uri = provider.urlFor("KEYWORD", "LOCATION", 1).toString()
                    val adsSize = castedProvider.extractAdsFrom(response).size
                    val furthestKnownPageNumber = castedProvider.furthestKnownPageNumber(response)

                    it("should return valid uri") { assertEquals(testData.expectedUri, uri) }
                    it("should extract ads from response") { assertEquals(testData.expectedAdsSize, adsSize) }
                    it("should extract furthest known page number") { assertEquals(testData.expectedFurthestKnownPageNumber, furthestKnownPageNumber) }
                }
            }
        }
    }
})