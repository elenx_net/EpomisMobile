package epomis.mobile.provider.com.linkedin


import epomis.mobile.provider.ProviderTestConfig
import epomis.mobile.provider.com.linkedin.feed.LinkedinFeedData
import epomis.mobile.provider.com.linkedin.feed.LinkedinFeedProvider
import epomis.service.connection6.response.HtmlResponse
import spock.lang.Shared
import spock.lang.Specification

class LinkedinFeedProviderTest extends Specification
{

    @Shared
    ProviderTestConfig config = new ProviderTestConfig()
    @Shared
    HtmlResponse response = config.configureHtmlResponse(this.class, "linkedinFeed.html")

    LinkedinFeedProvider feedProvider = new LinkedinFeedProvider()

    def "should receive ten feeds"()
    {

        when:

        Set<LinkedinFeedData> feeds = feedProvider.receiveFeeds(response)

        then:

        feeds.size() == 10
    }
}
