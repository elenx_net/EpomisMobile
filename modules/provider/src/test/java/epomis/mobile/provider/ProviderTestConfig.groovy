package epomis.mobile.provider

import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.response.HtmlResponse
import io.reactivex.Single
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.mockito.Mockito
import spock.lang.Specification

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class ProviderTestConfig extends Specification
{
    HtmlResponse configureHtmlResponse(Class currentTestClass, String resourceFileName)
    {
        def document = loadDocument(currentTestClass, resourceFileName)

        HtmlResponse htmlResponse = mock(HtmlResponse)
        when(htmlResponse.getDocument()).thenReturn(document)

        return htmlResponse
    }

    ConnectionService6 configureConnectionService(Class currentTestClass, String resourceFileName)
    {
        def document = loadDocument(currentTestClass, resourceFileName)

        ConnectionService6 connectionService = mock(ConnectionService6)
        HtmlResponse htmlResponse = mock(HtmlResponse)

        when(htmlResponse.getDocument()).thenReturn(document)
        when(connectionService.reactiveGetForHtml(Mockito.any())).thenReturn(Single.just(htmlResponse))

        return connectionService
    }

    Document loadDocument(Class currentTestClass, String resourceFileName)
    {
        def inputStream = currentTestClass.getResourceAsStream(resourceFileName)
        def doc = Jsoup.parse(inputStream, "UTF-8", resourceFileName)

        inputStream.close()

        return doc
    }
}
