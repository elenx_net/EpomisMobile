package epomis.mobile.provider.generic.recognizer.selector

import epomis.mobile.model.AdFilter
import epomis.mobile.provider.generic.selector.CssSelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorSanitizerImpl
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import spock.lang.Specification

class CssSelectorExtractorTest extends Specification
{
    void "test css selector extracting"()
    {
        given:

        AdFilter filter = new AdFilter("java", "warszawa")

            def parameters = [
                "/epomis/mobile/provider/generic/ads/infoPraca.html" : "id > li.container.results-list-item.clear-me:nth-child",
                "/epomis/mobile/provider/generic/ads/amundio.html" : "id > tbody > tr:nth-child"
            ]

            CssSelectorExtractor cssSelectorExtractor = new CssSelectorExtractor(new SelectorSanitizerImpl())

        when:

            def results = new ArrayList<Boolean>()

            parameters
                .entrySet()
                .stream()
                .map({parameter-> loadFromClassPath(parameter)})
                .forEach({parameter -> checkIfPassed(cssSelectorExtractor, parameter, filter, results)})

        then:

            !results.contains(false)
    }

    private Map.Entry<Element, String> loadFromClassPath(Map.Entry<String, String> entry)
    {
        def inputStream = CssSelectorExtractorTest.getResourceAsStream(entry.key)
        def document = Jsoup.parse(inputStream, "utf-8", entry.key)

        def elements = new HashMap<Element, String>()
        elements.put(document.body(), entry.value)

        return elements.entrySet().iterator().next()
    }

    private static void checkIfPassed(SelectorExtractor selectorExtractor, Map.Entry<Element, String> parameter,
                                      AdFilter filter, List<Boolean> results)
    {
        results.add(selectorExtractor.findMostFrequentCssSelector(parameter.getKey(), filter) == parameter.getValue())
    }
}
