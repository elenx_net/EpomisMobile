package epomis.mobile.provider.io.whoishiring

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class WhoIsHiringResponse(@JsonProperty("hits") val searchResult: SearchResult)

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class SearchResult(@JsonProperty("hits") val data: List<WhoIsHiringAdData>)

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class WhoIsHiringAdData(@JsonProperty("_source") val ad: WhoIsHiringAd)

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class WhoIsHiringAd(val id: String,
                                  val city: String,
                                  val title: String,
                                  val company: String,
                                  val description: String)
