package epomis.mobile.provider.pl.gumtree

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import epomis.mobile.model.Ad
import epomis.mobile.provider.HtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils

import org.jsoup.nodes.Element

import java.net.URL

private const val URL = "http://www.gumtree.pl/s-programisci-informatyka-i-internet"
private const val IT_CATEGORY_ID = "9005"
private const val QUERY_CONSTANT = "v1c"
private const val PROVIDER_NAME = "gumtree.pl"
private const val CATEGORY_LOCATION_FOOTER_CLASS = "category-location"
private const val SPAN = "span"
private const val TITLE_CLASS = "title"
private const val AD_CLASS = "tilev1"
private const val LAST_PAGE_NUMBER_CLASS = "pag-box-last"
private const val PAGE = "page-"
private const val DOMAIN = "http://www.gumtree.pl"
private const val DEFAULT_POLAND_ID = "202"
private const val NOT_INCLUDING_CLASS = ".top-listings"
private const val HREF = "href"

internal class GumtreeProvider(private val objectMapper: ObjectMapper ) : HtmlAdProvider
{
    override fun extractAdsFrom(response: HtmlResponse): Set<Ad> = response
        .document
        .getElementsByClass(AD_CLASS)
        .not(NOT_INCLUDING_CLASS)
        .asSequence()
        .map(this::extractAd)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL(createUrl(keyword, location, pageNumber))

    override fun furthestKnownPageNumber(response: HtmlResponse): Int
    {
        val element = response
            .document
            .getElementsByClass(LAST_PAGE_NUMBER_CLASS)
            .first()

        return if (element == null) 1 else extractPageNumber(element)
    }

    private fun extractAd(ad: Element): Ad
    {
        val title = ad.getElementsByClass(TITLE_CLASS).firstOrNull()
        val jobTitle = title?.text().orEmpty()

        val city = ad
            .getElementsByClass(CATEGORY_LOCATION_FOOTER_CLASS)
            .firstOrNull()
            ?.getElementsByTag(SPAN)
            ?.firstOrNull()
            ?.text().orEmpty()
            .split(",")
            .dropLastWhile { it.isEmpty() }
            .toTypedArray()[2]

        val href = DOMAIN + title?.getElementsByClass("href-link")
            ?.firstOrNull()
            ?.attributes()
            ?.get(HREF).orEmpty()

        return Ad(
            href = href,
            title = jobTitle,
            location = city,
            company = StringUtils.EMPTY,
            provider = PROVIDER_NAME)
    }

    private fun createUrl(keyword: String, location: String, pageNumber: Int): String = "$URL/$location/${PAGE}$pageNumber/$QUERY_CONSTANT${IT_CATEGORY_ID}l${receiveLocalizationCodeByName(location)}p1?q=$keyword"

    private fun extractPageNumber(element: Element): Int = element
        .attr(HREF)
        .let { StringUtils.substringBetween(it, PAGE, "/") }
        .let(Integer::parseInt)


    private fun receiveLocalizationCodeByName(cityName: String): String
    {
        val localizationMap = objectMapper.readValue<Map<String, String>>(GumtreeProvider::class.java.getResourceAsStream("localizations.json"))

        return localizationMap.getOrDefault(cityName, DEFAULT_POLAND_ID)
    }
}
