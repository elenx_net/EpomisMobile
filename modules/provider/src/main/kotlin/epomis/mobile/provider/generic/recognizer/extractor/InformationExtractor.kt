package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.AdFilter
import io.reactivex.Maybe
import org.jsoup.nodes.Element

interface InformationExtractor
{
    fun extract(parentElement: Element, adFilter: AdFilter): Maybe<String>
}