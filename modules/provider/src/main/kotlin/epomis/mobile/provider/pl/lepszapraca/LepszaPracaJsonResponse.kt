package epomis.mobile.provider.pl.lepszapraca

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
internal class LepszaPracaJsonResponse(val total: Int, val hits: List<LepszaPracaAdJson>)

@JsonIgnoreProperties(ignoreUnknown = true)
internal class LepszaPracaAdJson(val title: String, val url: String, val location: Location)

@JsonIgnoreProperties(ignoreUnknown = true)
internal class Location(val city: String)
