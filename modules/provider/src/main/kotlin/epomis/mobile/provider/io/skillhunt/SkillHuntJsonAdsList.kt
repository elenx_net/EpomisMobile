package epomis.mobile.provider.io.skillhunt

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class SkillHuntJsonAdsList(val data: List<SkillHuntJsonAd>)
