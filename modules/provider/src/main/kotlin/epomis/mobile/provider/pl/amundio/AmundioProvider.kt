package epomis.mobile.provider.pl.amundio

import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.net.URL
import kotlin.math.max

private const val URL = "http://www.amundio.pl/praca/"
private const val PROVIDER_NAME = "amundio.pl"
private const val BASE_URL = "http://www.amundio.pl"

private const val ATTR_B = "b"
private const val ATTR_A = "a"
private const val ATTR_LI = "li"
private const val ATTR_HREF = "href"
private const val ATTR_H2 = "h3"
private const val ATTR_TR = "tr"

private const val CLASS_COMPANY = "lightBlue"
private const val CLASS_CITY = "cityTd"
private const val CLASS_DESCRIPTION = "blockSpan break-word"
private const val CLASS_TITLE= "headerTitle"

private const val KEYWORD_PARAMETER = "q"

private const val LOCATION_PARAMETER = "l"

private const val PAGE_PARAMETER = "p"

internal class AmundioProvider : SimpleHtmlAdProvider {

    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByTag(ATTR_TR)
            .filter { it.children().any { child -> child.hasClass(CLASS_CITY) } }
            .toSet()

    override fun extractHref(ad: Element) = BASE_URL + ad
            .getElementsByTag(ATTR_A)
            .attr(ATTR_HREF)

    override fun extractTitle(ad: Element) =  ad
            .getElementsByClass(CLASS_TITLE)
            ?.text()
            .orEmpty()

    override fun extractLocation(ad: Element) = ad
            .getElementsByTag(ATTR_B)
            .text()

    override fun extractCompany(ad: Element) = ad
            .getElementsByClass(CLASS_COMPANY)
            .text()
            .split(" ")
            .first()

     fun extractDescription(ad: Element) = ad
            .getElementsByClass(CLASS_DESCRIPTION)
            .text()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL?$KEYWORD_PARAMETER=$keyword&$LOCATION_PARAMETER=$location&$PAGE_PARAMETER=$pageNumber")

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
            .document
            .getElementsByTag(ATTR_LI)
            .asSequence()
            .map { it.text().toIntOrNull() }
            .filterNotNull()
            .asSequence()
            .max() ?: 1
}
