package epomis.mobile.provider

internal data class ProviderException(val providerName: String, override val message: String) : RuntimeException(message)
{
    override fun toString() = "ProviderException(['$providerName']: message='$message')"
}