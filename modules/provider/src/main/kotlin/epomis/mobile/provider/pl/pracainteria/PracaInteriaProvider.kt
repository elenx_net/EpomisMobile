package epomis.mobile.provider.pl.pracainteria

import epomis.mobile.provider.SimpleHtmlAdProvider

import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document

import org.jsoup.nodes.Element

import java.net.URL

private const val URL = "http://praca.interia.pl/oferty-pracy"
private const val PROVIDER_NAME = "praca.interia.pl"
private const val CLASS_AD = "job-row-body"
private const val CLASS_AD_NAME = "job-row-name"
private const val CLASS_AD_DETAILS = "job-row-employer"
private const val CLASS_PAGINATION = "pagination"

private const val ATTR_HREF = "href"

private const val LOCATION_SPLIT_STRING = " - "

internal class PracaInteriaProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document): Set<Element> = document
        .getElementsByClass(CLASS_AD)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) =
        if(paramsAreBlank(keyword, location))
            URL("$URL/wszystkie")
        else
            URL("$URL,q,$keyword%20$location,nPack,$pageNumber")

    private fun paramsAreBlank(vararg params: String) = params
        .none(StringUtils::isNotBlank)

    override fun furthestKnownPageNumber(response: HtmlResponse): Int = extractPaginationElements(response)
        .asSequence()
        .map(Element::text)
        .filter(StringUtils::isNumeric)
        .map(Integer::parseInt)
        .max() ?: 1

    private fun extractPaginationElements(response: HtmlResponse) = response
        .document
        .getElementsByClass(CLASS_PAGINATION)
        .firstOrNull()
        ?.getElementsByTag("ul")
        ?.firstOrNull()
        ?.getElementsByTag("li")
        .orEmpty()

    override fun extractLocation(ad: Element) = ad
        .getElementsByClass(CLASS_AD_DETAILS)
        .firstOrNull()
        ?.text()
        ?.substringAfter(LOCATION_SPLIT_STRING)
        .orEmpty()

    override fun extractCompany(ad: Element) = ad
        .getElementsByClass(CLASS_AD_DETAILS)
        .firstOrNull()
        ?.getElementsByTag("a")
        ?.firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_AD_NAME)
        .firstOrNull()
        ?.getElementsByTag("a")
        ?.attr(ATTR_HREF)
        ?.let { URL + it }
        .orEmpty()

    override fun extractTitle(ad: Element) = ad
        .getElementsByClass(CLASS_AD_NAME)
        .firstOrNull()
        ?.text()
        .orEmpty()
}
