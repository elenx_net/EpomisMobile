package epomis.mobile.provider.generic.selector

import org.jsoup.nodes.Element

interface SelectorSanitizer
{
    fun toGenericSelector(cssSelector: String): String

    fun cssSelectorFilter(element: Element, cssSelector: String): Boolean
}