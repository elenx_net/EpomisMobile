package epomis.mobile.provider.pl.pracagratka

import epomis.mobile.model.Ad
import epomis.mobile.provider.HtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils

import org.jsoup.nodes.Element

import java.net.URL

private const val DOCUMENT_SELECTOR = "article.teaser"
private const val URL = "http://gratka.pl/praca/"
private const val PROVIDER_NAME = "praca.gratka.pl"
private const val CITY = "teaser__location"
private const val TITLE = "teaser__title"
private const val PAGINATION_INPUT_CLASS = "pagination__input"
private const val MAX_ATTR = "max"
private const val DATA_HREF_ATTR = "data-href"
private const val LI_TAG = "li"
private const val COLON_DELIMITER = ": "
private const val Q_PARAM = "q"
private const val PAGE_PARAM = "page"

internal class PracaGratkaPlProvider : HtmlAdProvider
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL$location?${if(!keyword.isBlank())"$Q_PARAM=$keyword&" else ""}$PAGE_PARAM=$pageNumber")

    override fun extractAdsFrom(response: HtmlResponse) = response
        .document
        .select(DOCUMENT_SELECTOR)
        .asSequence()
        .map{ extractAd(it) }
        .toSet()

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
        .document
        .getElementsByClass(PAGINATION_INPUT_CLASS)
        ?.first()
        ?.attr(MAX_ATTR)
        ?.toInt() ?: 1

    private fun extractAd(ad: Element): Ad
    {
        val jobTitle = ad.getElementsByClass(TITLE).text()
        val city = ad.getElementsByClass(CITY).text()
        val company = ad.getElementsByTag(LI_TAG).first().text().split(COLON_DELIMITER)[1]
        val href = URL + ad.attr(DATA_HREF_ATTR)

        return Ad(
            href = href,
            title = jobTitle,
            company = company,
            location = city,
            provider = PROVIDER_NAME
        )
    }
}
