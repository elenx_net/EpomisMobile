package epomis.mobile.provider.io.skillhunt

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.response.JsonResponse

import java.net.URL

private const val SLASH = "/"
private const val ADS_AS_JSON_URL = "https://api.skillhunt.io/api/v1/offers"
private const val BASE_URL = "https://app.skillhunt.io/jobs/view/"
private const val PROVIDER_NAME = "skillhunt.io"

internal class SkillHuntProviderJson : JsonAdProvider<SkillHuntJsonAdsList>
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL = URL(ADS_AS_JSON_URL)

    override fun extractAdsFrom(response: JsonResponse<SkillHuntJsonAdsList>): Set<Ad> = response
            .json
            .data
            .asSequence()
            .filter { it.closed.equals("false", true) && it.slug != null }
            .map(this::extractAd)
            .toSet()

    override fun furthestKnownPageNumber(response: JsonResponse<SkillHuntJsonAdsList>): Int = 1

    override fun getJsonClass(): Class<SkillHuntJsonAdsList> = SkillHuntJsonAdsList::class.java

    private fun extractAd(jsonAd: SkillHuntJsonAd) = Ad(
            href = BASE_URL + jsonAd.id + SLASH + jsonAd.slug,
            title = jsonAd.position,
            company = jsonAd.company,
            location = jsonAd.location.city,
            provider = PROVIDER_NAME)
}
