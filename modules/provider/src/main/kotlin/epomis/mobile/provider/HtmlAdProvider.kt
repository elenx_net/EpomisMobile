package epomis.mobile.provider

import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.http.HttpMethod
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.HtmlResponse
import io.reactivex.Single

internal interface HtmlAdProvider : GenericAdProvider<HtmlResponse>
{
    override fun send(connectionRequest: ConnectionRequest, connectionService: ConnectionService6, httpMethod: HttpMethod) = Single.fromPublisher(
        when (httpMethod)
        {
            HttpMethod.GET -> connectionService.reactiveGetForHtml(connectionRequest)
            HttpMethod.POST -> connectionService.reactivePostForHtml(connectionRequest)
            else -> throw UnsupportedOperationException("Http method not supported")
        }
    )
}
