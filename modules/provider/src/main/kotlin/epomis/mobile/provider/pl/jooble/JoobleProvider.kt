package epomis.mobile.provider.pl.jooble

import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.net.URL

private const val URL = "https://pl.jooble.org"
private const val PROVIDER_NAME = "jooble.pl"

private const val CLASS_AD = "result"
private const val CLASS_TITLE = "position"
private const val CLASS_LOCATION = "date_location__region"
private const val CLASS_COMPANY = "company-name"
private const val CLASS_HREF = "link-position"

private const val ID_PAGINATION = "paging"

private const val TAG_A = "a"

private const val USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"

internal class JoobleProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun headersFor(keyword: String, location: String, pageNumber: Int) = mapOf("User-Agent" to USER_AGENT)

    override fun selectAdElements(document: Document) = document
        .getElementsByClass(CLASS_AD)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) =
        URL("$URL/praca-$keyword/$location?p=$pageNumber")

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
        .document
        .getElementById(ID_PAGINATION)
        ?.getElementsByTag(TAG_A)
        ?.asSequence()
        ?.map(Element::text)
        ?.map(Integer::parseInt)
        ?.max() ?: 1

    override fun extractCompany(ad: Element) = ad
        .getElementsByClass(CLASS_COMPANY)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractLocation(ad: Element) = ad
        .getElementsByClass(CLASS_LOCATION)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_HREF)
        .firstOrNull()
        ?.attr("href")
        .orEmpty()

    override fun extractTitle(ad: Element) = ad
        .getElementsByClass(CLASS_TITLE)
        .firstOrNull()
        ?.text()
        .orEmpty()
}
