package epomis.mobile.provider.pl.indeed

import epomis.mobile.provider.*
import epomis.service.connection6.response.HtmlResponse
import org.apache.commons.lang3.StringUtils
import java.net.URL
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

private const val SEARCH_URL = "https://pl.indeed.com/praca"
private const val URL = "https://pl.indeed.com"
private const val PROVIDER_NAME = "pl.indeed.com"
private const val DEFAULT_COUNTRY = "polska"
private const val CLASS_AD_ELEMENT = "jobsearch-SerpJobCard"
private const val CLASS_TITLE = "jobtitle"
private const val CLASS_LOCATION = "location"
private const val CLASS_COMPANY = "company"
private const val CLASS_PAGINATION = "pagination"
private const val HTML_PAGE_NUMBER = "a > .pn"
private const val ATTR_HREF = "href"
private const val TAG_A = "a"
private const val TAG_B = "b"
private const val HREF_EXCEPTION_MESSAGE = "Could not parse href"
private const val TITLE_EXCEPTION_MESSAGE = "Could not parse title"

internal class IndeedProvider : SimpleHtmlAdProvider
{

    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
        .getElementsByClassNotNull(CLASS_AD_ELEMENT)
        .toSet()

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_TITLE)
        .firstOrNull()
        ?.getElementsByTag(TAG_A)
        ?.firstOrNull()
        ?.attr(ATTR_HREF)
        ?.let { URL + it }
        ?: throw ProviderException(PROVIDER_NAME, HREF_EXCEPTION_MESSAGE)

    override fun extractTitle(ad: Element) = extractTextFromElement(ad, CLASS_TITLE)
        ?: throw ProviderException(PROVIDER_NAME, TITLE_EXCEPTION_MESSAGE)

    override fun extractLocation(ad: Element) = extractTextFromElement(ad, CLASS_LOCATION) ?: StringUtils.EMPTY

    override fun extractCompany(ad: Element) = extractTextFromElement(ad, CLASS_COMPANY) ?: StringUtils.EMPTY

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$SEARCH_URL?q=$keyword&l=${if(location.isNotEmpty()) "$location" else DEFAULT_COUNTRY}&start=${pageNumber*10}")

    override fun furthestKnownPageNumber(response: HtmlResponse): Int
    {
        val paginationElement = response
            .document
            .getElementsByClass(CLASS_PAGINATION)
            .firstOrNull() ?: return 1

        val currentPageNumber = extractCurrentPageNumber(paginationElement) ?: return 1
        val maxPageNumber = extractMaxPageNumber(paginationElement)

        return maxOf(maxPageNumber, currentPageNumber)
    }

    private fun extractTextFromElement(element: Element, classSelector: String) = element
        .getElementsByClass(classSelector)
        .firstOrNull()
        ?.text()

    private fun extractCurrentPageNumber(element: Element) = element
        .getElementsByTagNotNull(TAG_B)
        .firstOrNull()
        ?.text()
        ?.toInt()

    private fun extractMaxPageNumber(element: Element) = element
        .selectNotNull(HTML_PAGE_NUMBER)
        .map { it.text() }
        .map { it.toIntOrNull() }
        .map { it ?: 0 }
        .max() ?: 1
}