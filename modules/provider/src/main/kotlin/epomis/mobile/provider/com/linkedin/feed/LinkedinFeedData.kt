package epomis.mobile.provider.com.linkedin.feed

internal data class LinkedinFeedData(val author: String,
                                     val authorHref: String,
                                     val content: String)