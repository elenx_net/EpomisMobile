package epomis.mobile.provider.pl.jobs

import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.jsoup.nodes.Document

import org.jsoup.nodes.Element
import java.net.URL

private const val URL = "https://www.jobs.pl/praca"
private const val PROVIDER_NAME = "jobs.pl"

private const val CLASS_AD = "offer"
private const val CLASS_AD_TITLE = "title"
private const val CLASS_AD_COMPANY = "employer"
private const val CLASS_AD_LOCATION = "localization"
private const val CLASS_PAGINATION = "pagination"

private const val TAG_A = "a"

private const val ATTR_HREF = "href"

internal class JobsProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
        .getElementsByClass(CLASS_AD)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) =
        URL("$URL/$location/$keyword;k/p-$pageNumber")

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
        .document
        .getElementsByClass(CLASS_PAGINATION)
        .firstOrNull()
        ?.getElementsByTag(TAG_A)
        ?.asSequence()
        ?.map(Element::text)
        ?.filter { it.toIntOrNull() != null }
        ?.map(Integer::parseInt)
        ?.max() ?: 1

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_AD_TITLE)
        .firstOrNull()
        ?.getElementsByTag(TAG_A)
        ?.firstOrNull()
        ?.attr(ATTR_HREF)
        ?.let { URL + it }
        .orEmpty()

    override fun extractTitle(ad: Element) = ad
        .getElementsByClass(CLASS_AD_TITLE)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractCompany(ad: Element) = ad
        .getElementsByClass(CLASS_AD_COMPANY)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractLocation(ad: Element) = ad
        .getElementsByClass(CLASS_AD_LOCATION)
        .firstOrNull()
        ?.text()
        .orEmpty()
}
