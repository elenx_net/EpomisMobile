package epomis.mobile.provider.generic

import epomis.mobile.provider.generic.recognizer.AdMostFrequentSelectorRecognizer
import epomis.mobile.provider.generic.recognizer.AdRecognizer
import epomis.mobile.provider.generic.recognizer.extractor.GenericAdExtractor
import epomis.mobile.provider.generic.recognizer.extractor.GenericAdExtractorFacade
import epomis.mobile.provider.generic.recognizer.extractor.HrefExtractor
import epomis.mobile.provider.generic.recognizer.extractor.LocationExtractor
import epomis.mobile.provider.generic.recognizer.extractor.TitleExtractor
import epomis.mobile.provider.generic.selector.CssSelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorSanitizer
import epomis.mobile.provider.generic.selector.SelectorSanitizerImpl
import epomis.mobile.provider.generic.source.UrlPatternExpander
import epomis.mobile.provider.generic.source.UrlStorage

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val genericProviderModule = Kodein.Module("genericProvider")
{
    bind<UrlStorage>() with singleton { UrlStorage() }
    bind<UrlPatternExpander>() with singleton { UrlPatternExpander() }
    bind<SelectorSanitizer>() with singleton { SelectorSanitizerImpl() }
    bind<SelectorExtractor>() with singleton { CssSelectorExtractor(instance()) }
    bind<AdRecognizer>() with singleton { AdMostFrequentSelectorRecognizer(instance(), instance(), instance()) }
    bind<LocationExtractor>() with singleton { LocationExtractor() }
    bind<TitleExtractor>() with singleton { TitleExtractor() }
    bind<HrefExtractor>() with singleton { HrefExtractor(instance(), instance()) }
    bind<GenericAdExtractor>() with singleton { GenericAdExtractorFacade(instance(), instance(), instance()) }
}