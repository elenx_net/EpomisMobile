package epomis.mobile.provider.pl.grafton


import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import java.net.URL
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element



private const val URL = "https://www.grafton.pl/"
private const val PROVIDER_NAME = "grafton.pl"

private const val OFFER_CLASS = "job-result-item"
private const val PAGINATION_CLASS = "pagination"
private const val HREF_CLASS = "job-details"
private const val TITLE_CLASS = "job-title"
private const val LOCATION_CLASS = "result-job-location"

private const val DEFAULT_KEYWORD = ""

private const val EXCEPTION_URL_MESSAGE = "Could not parse URL"
private const val EXCEPTION_TITLE_MESSAGE = "Could not parse title"

private const val EXTRACT_KEYWORD = "search[query]"
private const val EXTRACT_LOCATION = "seo_location"
private const val EXTRACT_PAGE = "page"

private const val ATTR_HREF = "href"

internal class GraftonProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(OFFER_CLASS)
            .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL?$EXTRACT_KEYWORD=$keyword&$EXTRACT_LOCATION=$location&$EXTRACT_PAGE=$pageNumber")

    private fun considerKeyword(keyword: String) =
            DEFAULT_KEYWORD.takeIf { keyword.isBlank() } ?: keyword

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
            .document
            .getElementsByClass(PAGINATION_CLASS)
            .map { extractPageNumberFromElement(it) }
            .max() ?: 1

    private fun extractPageNumberFromElement(pagination: Element) = pagination
            .text()
            .toInt()

    override fun extractHref(ad: Element) = ad
            .getElementsByClass(HREF_CLASS)
            .firstOrNull()
            ?.attr(ATTR_HREF)
            ?: throw ProviderException(PROVIDER_NAME, EXCEPTION_URL_MESSAGE)

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(TITLE_CLASS)
            .firstOrNull()
            ?.text()
            ?: throw ProviderException(PROVIDER_NAME, EXCEPTION_TITLE_MESSAGE)

    override fun extractLocation(ad: Element)= ad
            .getElementsByClass(LOCATION_CLASS)
            .firstOrNull()
            ?.text()
            .orEmpty()


}
