package epomis.mobile.provider.pl.justjoin

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class JustJoinData(@JsonProperty("title") val title: String,
                        @JsonProperty("company_name") val companyName: String,
                        @JsonProperty("city") val city: String,
                        @JsonProperty("id") val id: String)