package epomis.mobile.provider.pl.hays

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.http.HttpMethod
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.DataEntry
import epomis.service.connection6.request.RequestContentType
import epomis.service.connection6.response.JsonResponse
import java.net.URL

private const val PROVIDER_NAME = "m.hays.pl"
private const val URL = "https://m.hays.pl/googleSearch/GetResults"
private const val QUERY = "query"
private const val LOCATION = "location"

internal class HaysProvider : JsonAdProvider<HaysResponse>
{
    override fun getJsonClass(): Class<HaysResponse> = HaysResponse::class.java

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL = URL(URL)

    override fun dataEntriesFor(keyword: String, location: String, pageNumber: Int): List<DataEntry> = listOf(
        DataEntry(QUERY, keyword),
        DataEntry(LOCATION, location)
    )

    override fun methodFor(keyword: String, location: String, pageNumber: Int): HttpMethod = HttpMethod.POST

    override fun requestFor(keyword: String, location: String, pageNumber: Int): ConnectionRequest =
        ConnectionRequest(
            url = urlFor(keyword, location, pageNumber).toString(),
            data = dataEntriesFor(keyword, location, pageNumber),
            contentType = RequestContentType.URL_ENCODED
        )

    override fun furthestKnownPageNumber(response: JsonResponse<HaysResponse>): Int = 1

    override fun extractAdsFrom(response: JsonResponse<HaysResponse>): Set<Ad> =
        response.json.jobs
            .asSequence()
            .map(this::extractAd)
            .toSet()

    private fun extractAd(haysJob: HaysJob) = Ad(
        href = haysJob.url,
        title = haysJob.title,
        location = haysJob.location,
        company = haysJob.company,
        provider = PROVIDER_NAME
    )
}