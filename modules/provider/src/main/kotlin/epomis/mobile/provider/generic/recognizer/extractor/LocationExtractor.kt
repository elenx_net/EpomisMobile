package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.AdFilter
import epomis.utils.toMaybe
import io.reactivex.Maybe
import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Element

class LocationExtractor : InformationExtractor
{
    override fun extract(parentElement: Element, adFilter: AdFilter): Maybe<String> = parentElement
        .getElementsContainingOwnText(adFilter.location)
        .asSequence()
        .map(Element::ownText)
        .filter { StringUtils.containsIgnoreCase(it, adFilter.location) }
        .minBy { it.length.toLong() }
        .orEmpty()
        .toMaybe()
}