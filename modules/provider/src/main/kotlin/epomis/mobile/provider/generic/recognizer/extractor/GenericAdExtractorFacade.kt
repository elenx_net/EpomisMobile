package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter

import io.reactivex.Maybe
import io.reactivex.functions.Function3

import org.jsoup.nodes.Element

class GenericAdExtractorFacade(private val titleExtractor: TitleExtractor,
                               private val locationExtractor: LocationExtractor,
                               private val hrefExtractor: HrefExtractor) : GenericAdExtractor
{
    override fun buildAdFromElement(parentElement: Element, adFilter: AdFilter): Maybe<Ad>
    {
        val maybeTitle = titleExtractor.extract(parentElement, adFilter)
        val maybeLocation = locationExtractor.extract(parentElement, adFilter)
        val maybeHref = hrefExtractor.extract(parentElement, adFilter)

        return Maybe
            .zip(
                maybeTitle,
                maybeLocation,
                maybeHref,
                Function3<String?, String?, String?, Maybe<Ad>> { title, location, href ->
                    createAd(
                        title,
                        location,
                        href
                    )
                })
            .flatMap { it }
    }

    private fun createAd(title: String?, location: String?, href: String?): Maybe<Ad>
    {
        return if(isNotNull(title)  && isNotNull(location) && isNotNull(href))
        {
            val genericProviderFqn = javaClass.name

            Maybe.just(Ad(href = href!!,
                          title = title!!,
                          company = genericProviderFqn,
                          location = location!!,
                          provider = genericProviderFqn
            ))
        }
        else Maybe.empty()
    }

    private fun isNotNull(value: String?) = value != null
}
