package epomis.mobile.provider.com.functionalworks

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class FunctionalWorksResponse(val data: QueryData)

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class QueryData(@JsonProperty("jobs_search")
                     val searchResult: SearchResult)

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class SearchResult(val numberOfPages: Int,
                        val jobs: List<FunctionalWorksAd>)

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class FunctionalWorksAd(val id: String,
                             val title: String,
                             val company: Company,
                             val location: Location)

internal data class Location(val city: String)

internal data class Company(val name: String)