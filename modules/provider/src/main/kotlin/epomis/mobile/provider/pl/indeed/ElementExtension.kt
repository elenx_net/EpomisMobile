package epomis.mobile.provider.pl.indeed

import org.jsoup.nodes.Element

internal fun Element.selectNotNull(cssQuery: String) = select(cssQuery).filterNotNull()
internal fun Element.getElementsByClassNotNull(className: String) = getElementsByClass(className).filterNotNull()
internal fun Element.getElementsByTagNotNull(tagName: String) = getElementsByTag(tagName).filterNotNull()