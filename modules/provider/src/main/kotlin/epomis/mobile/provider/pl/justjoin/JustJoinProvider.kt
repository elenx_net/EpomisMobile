package epomis.mobile.provider.pl.justjoin

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.response.JsonResponse
import java.net.URL

import org.apache.commons.lang3.StringUtils

private const val REST_URL = "http://justjoin.it/api/offers"
private const val HREF = "http://justjoin.it/offers/"
private const val PROVIDER_NAME = "justjoin.it"
private const val JAVA = "java "

internal class JustJoinProvider : JsonAdProvider<Array<JustJoinData>>
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int) =
        URL("$REST_URL/search?categories[]=$keyword&locations[]=$location")

    override fun extractAdsFrom(response: JsonResponse<Array<JustJoinData>>) = response
            .json.asSequence()
            .map(this::extractAd)
            .toSet()

    override fun furthestKnownPageNumber(response: JsonResponse<Array<JustJoinData>>) = 1

    override fun getJsonClass() = Array<JustJoinData>::class.java

    private fun extractAd(justJoinData: JustJoinData) = Ad(href = HREF + justJoinData.id,
        title = justJoinData.title,
        company = justJoinData.companyName,
        location = justJoinData.city,
        provider = PROVIDER_NAME
    )
}
