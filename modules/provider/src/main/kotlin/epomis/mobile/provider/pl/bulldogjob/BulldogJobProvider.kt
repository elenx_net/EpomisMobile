package epomis.mobile.provider.pl.bulldogjob

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

import java.net.URL

import java.lang.StringBuilder

private const val URL = "https://bulldogjob.pl/companies/jobs/s/"

private const val CLASS_AD = "job-item"
private const val CLASS_AD_TITLE = "title"
private const val CLASS_AD_COMPANY = "company"
private const val CLASS_AD_LOCATION = "location"
private const val CLASS_PAGINATION = "pagination"

private const val EXCEPTION_MESSAGE_HREF = "Could not parse url"
private const val EXCEPTION_MESSAGE_TITLE = "Could not parse title"

private const val TAG_H2 = "h2"

private const val ATTR_DATA_TOTAL = "data-total"
private const val ATTR_HREF = "href"

private const val PROVIDER_NAME = "pl.bulldogjob"

internal class BulldogJobProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(CLASS_AD)
            .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL {

        val urlBuilder = StringBuilder()

        location.takeIf { it.isNotBlank() && !it.equals("LOCATION")}
            ?.let { urlBuilder.append("city,$it") }

        keyword.takeIf { it.isNotBlank() && !it.equals("KEYWORD") }
            ?.let { urlBuilder.append("/skills,$it") }

        if (keyword.isBlank() || keyword.equals("LOCATION") && location.isBlank() || keyword.equals("KEYWORD"))
            urlBuilder.append("city,/skills,?page=1")
        else
            urlBuilder.append("?page=$pageNumber")

        return URL("${URL}$urlBuilder")
    }

    override fun furthestKnownPageNumber(htmlResponse: HtmlResponse) =
        htmlResponse
            .document
            .getElementsByClass(CLASS_PAGINATION)
            .firstOrNull()
            ?.attr(ATTR_DATA_TOTAL)
            ?.toInt()
            ?: 1

    override fun extractHref(ad: Element) = ad
            .attr(ATTR_HREF)
            ?: throw ProviderException(providerName, EXCEPTION_MESSAGE_HREF)

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(CLASS_AD_TITLE)
            .firstOrNull()
            ?.getElementsByTag(TAG_H2)
            ?.firstOrNull()
            ?.text()
            ?: throw ProviderException(providerName, EXCEPTION_MESSAGE_TITLE)

    override fun extractCompany(ad: Element) = ad
            .getElementsByClass(CLASS_AD_COMPANY)
            .firstOrNull()
            ?.text()
            .orEmpty()

    override fun extractLocation(ad: Element) = ad
            .getElementsByClass(CLASS_AD_LOCATION)
            .firstOrNull()
            ?.text()
            .orEmpty()
}
