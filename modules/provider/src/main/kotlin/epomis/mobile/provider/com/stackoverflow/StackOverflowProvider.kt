package epomis.mobile.provider.com.stackoverflow

import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import java.net.URL
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

private const val URL = "http://stackoverflow.com"
private const val PROVIDER_NAME = "stackoverflow.com"
private const val JOBS = "/jobs"
private const val JS_RESULT = "js-result"
private const val PAGINATION_CLASS = "s-pagination"
private const val SPAN = "span"
private const val A = "a"
private const val H3 = "h3"
private const val TITLE = "title"
private const val MAX_OFFERS_AMOUNT_PER_PAGE = 25
private const val JS_SEARCH_TITLE = "js-search-title"
private const val LOCATION_CLASS = "fc-black-500"
private const val DATA_PREVIEW_URL_ATTR = "data-preview-url"
private const val S_LINK_CLASS = "s-link"

internal class StackOverflowProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document): Set<Element>
    {
        val totalOffersAmount = extractTotalOffersAmount(document)
        val adElements = document.getElementsByClass(JS_RESULT).toSet()

        return if (totalOffersAmount > MAX_OFFERS_AMOUNT_PER_PAGE)
        {
            adElements
        }
        else
        {
            adElements.take(totalOffersAmount).toSet()
        }
    }

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL =
        if (pageNumber <= 1)
        {
            URL("${URL + JOBS}?q=$keyword&l=$location")
        }
        else
        {
            URL("${URL + JOBS}?q=$keyword&l=$location&pg=$pageNumber")
        }

    override fun furthestKnownPageNumber(response: HtmlResponse): Int = response
            .document
            .getElementsByClass(PAGINATION_CLASS)
            .firstOrNull()
            ?.getElementsByTag(A)
            ?.attr(TITLE)
            ?.takeLast(1)
            ?.toInt()!!

    override fun extractLocation(ad: Element): String = ad
            .getElementsByClass(LOCATION_CLASS)
            .first()
            .text()

    override fun extractCompany(ad: Element) = ad
            .getElementsByTag(H3)
            .firstOrNull()
            ?.getElementsByTag(SPAN)
            ?.firstOrNull()
            ?.text()!!

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(S_LINK_CLASS)
            .firstOrNull()
            ?.attr(TITLE)!!

    override fun extractHref(ad: Element) = URL + ad
            .attr(DATA_PREVIEW_URL_ATTR)

    private fun extractTotalOffersAmount(document: Document): Int = document
            .getElementsByClass(JS_SEARCH_TITLE)
            .firstOrNull()
            ?.getElementsByTag(SPAN)
            ?.text()
            ?.filter { it.isDigit() }
            ?.toInt()!!
}
