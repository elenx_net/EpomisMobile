package epomis.mobile.provider.generic.source

internal class UrlPatternExpander
{
    internal fun expand(pattern: String): List<String> = expand(pattern, 1, 3)

    private fun expand(pattern: String, start: Int, end: Int): List<String> = IntProgression
            .fromClosedRange(start, end, 1)
            .map { pageNumber -> String.format(pattern, pageNumber) }
            .toList()
}
