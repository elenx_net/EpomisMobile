package epomis.mobile.provider.com.functionalworks

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.response.JsonResponse
import java.net.URL
import java.net.URLEncoder

private const val GRAPHQL_QUERY = "?query=%%7Bjobs_search(search_term:%%22%s+%s%%22,page:%d)%%7BnumberOfPages+jobs%%7Bid+title+company%%7Bname%%7Ddescription+location%%7Bcity%%7D%%7D%%7D%%7D"
private const val SITE_ENDPOINT = "https://functional.works-hub.com/jobs/"
private const val GRAPHQL_URL = "https://functional.works-hub.com/api/graphql"
private const val PROVIDER_NAME = "functionalworks.com"

internal class FunctionalWorksProvider : JsonAdProvider<FunctionalWorksResponse>
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL =
        URL(
            GRAPHQL_URL + String.format(GRAPHQL_QUERY,
                URLEncoder.encode(keyword, "UTF-8"),
                URLEncoder.encode(location, "UTF-8"),
                pageNumber)
        )

    override fun extractAdsFrom(response: JsonResponse<FunctionalWorksResponse>): Set<Ad> = extractAdsData(response)
        .asSequence()
        .map(this::extractOffer)
        .toSet()

    override fun furthestKnownPageNumber(response: JsonResponse<FunctionalWorksResponse>): Int = response
        .json
        .data
        .searchResult
        .numberOfPages

    override fun getJsonClass(): Class<FunctionalWorksResponse> = FunctionalWorksResponse::class.java

    private fun extractOffer(ad: FunctionalWorksAd): Ad = Ad(
        href = SITE_ENDPOINT + ad.id,
        title = ad.title,
        company = ad.company.name,
        location = ad.location.city,
        provider = PROVIDER_NAME
    )

    private fun extractAdsData(response: JsonResponse<FunctionalWorksResponse>): List<FunctionalWorksAd> = response
        .json
        .data
        .searchResult
        .jobs
}
