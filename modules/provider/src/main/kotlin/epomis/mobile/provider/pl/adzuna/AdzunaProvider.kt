package epomis.mobile.provider.pl.adzuna

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.lang.StringBuilder
import java.net.URL

private const val CLASS_OFFER = "a"
private const val CLASS_OFFER_CITY = "loc"
private const val CLASS_OFFER_H2 = "h2"

private const val ATTR_HREF = "href"
private const val ATTR_TAG = "pg"
private const val ATTR_A = "A"
private const val TAG_A = "a"

private const val URL = "https://www.adzuna.pl/search?"
private const val PROVIDER_NAME = "adzuna.pl"

internal class AdzunaProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(CLASS_OFFER)
            .toSet()

    override fun extractHref(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER)
            .firstOrNull()
            ?.getElementsByTag(TAG_A)
            ?.firstOrNull()
            ?.attr(ATTR_HREF)
            ?.let { URL + it }
            ?: throw ProviderException(PROVIDER_NAME, "Could not parse url")

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER)
            .firstOrNull()
            ?.getElementsByTag(CLASS_OFFER_H2)
            ?.text()
            ?: throw ProviderException(PROVIDER_NAME, "Could not parse title")

    override fun extractLocation(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER_CITY)
            .firstOrNull()
            ?.text()
            .orEmpty()

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL {

        val urlBuilder = StringBuilder()

        keyword.takeIf { it.isNotBlank() }
                ?.let { urlBuilder.append("q=$it&") }

        location.takeIf { it.isNotBlank() }
                ?.let { urlBuilder.append("w=$it&") }

        return URL("$URL${urlBuilder}p=$pageNumber")
    }

    override fun furthestKnownPageNumber(htmlResponse: HtmlResponse) = htmlResponse
            .document
            .getElementsByClass(ATTR_TAG)
            ?.let { extractLastPageNumber(it) }
            ?: 1

    private fun extractLastPageNumber(elements: Elements) = elements
            .lastOrNull()
            ?.getElementsByTag(ATTR_A)
            ?.dropLast(1)
            ?.lastOrNull()
            ?.text()
            ?.toInt()
            ?: 1

}


