package epomis.mobile.provider.pl.uget

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class UGetData(val id: String,
                             val title: String,
                             val remote: Boolean,
                             val address: Address?)
{
    fun getAddressLabel(): String? = address?.label
}

@JsonIgnoreProperties(ignoreUnknown = true)
internal class Address(val label: String)