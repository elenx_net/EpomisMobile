package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.AdFilter
import epomis.utils.toMaybe
import io.reactivex.Maybe
import org.jsoup.nodes.Element

class TitleExtractor : InformationExtractor
{
    override fun extract(parentElement: Element, adFilter: AdFilter): Maybe<String> = parentElement
        .getElementsContainingOwnText(adFilter.keyword)
        .asSequence()
        .sortedBy { ownTextLength(it) }
        .toMaybe()
        .filter { it.any() }
        .map { it.first() }
        .map { it.ownText() }

    private fun ownTextLength(element: Element) = element.ownText().length
}
