package epomis.mobile.provider.pl.forprogrammers

import epomis.mobile.model.Ad
import epomis.mobile.provider.HtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils

import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import java.net.URL

import java.util.Collections

private const val URL = "https://4programmers.net/Praca"
private  const val PROVIDER_NAME = "4programmers.net"
private const val CLASS_ACTIVE = "active"

internal class ForProgrammersProvider : HtmlAdProvider
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL?q=$keyword&city=$location&page=$pageNumber")

    override fun extractAdsFrom(response: HtmlResponse): Set<Ad> = response
        .document
        .getElementsByTag("tr")
        .asSequence()
        .map { this.extractAd(it) }
        .toSet()

    override fun furthestKnownPageNumber(response: HtmlResponse): Int
    {
        val pageNumbersList = response
            .document
            .getElementsByClass("pagination")
            .first()

        val furthestKnownPageNumber = Collections.max(extractPageNumbers(pageNumbersList))
        val currentPageNumber = extractCurrentPageNumber(pageNumbersList)

        return if (currentPageNumber > furthestKnownPageNumber) currentPageNumber else furthestKnownPageNumber
    }

    private fun extractAd(element: Element): Ad
    {
        val linkToAd = element.getElementsByTag("a")
        val company = linkToAd[2].text()
        val city = linkToAd[3].text()
        val href = linkToAd.attr("href")
        val title = element.getElementsByTag("h2").text()

        return Ad(
            href = href,
            title = title,
            company = company,
            location = city,
            provider = PROVIDER_NAME
        )
    }

    private fun extractPageNumbers(pageNumbersList: Element): List<Int> = pageNumbersList
        .getElementsByTag("li")
        .asSequence()
        .filter(this::hasPageNumber)
        .map { element -> element.getElementsByTag("a") }
        .map(Elements::first)
        .map(Element::text)
        .filter(StringUtils::isNumeric)
        .map(Integer::parseInt)
        .toList()

    private fun extractCurrentPageNumber(pageNumbersList: Element): Int = pageNumbersList
        .getElementsByClass(CLASS_ACTIVE)
        .first()
        .getElementsByTag("span")
        .text()
        .let(Integer::parseInt)

    private fun hasPageNumber(element: Element): Boolean = !element.hasClass(CLASS_ACTIVE) && !element.hasClass("disabled")
}
