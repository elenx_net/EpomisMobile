package epomis.mobile.provider.pl.hays

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class HaysResponse(
    @JsonProperty("jobs") val jobs: List<HaysJob>
)

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class HaysJob(
    @JsonProperty("trackingUrl") val url: String,
    @JsonProperty("companyTitle") val company: String,
    @JsonProperty("title") val title: String,
    @JsonProperty("location") val location: String
)
