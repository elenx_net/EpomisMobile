package epomis.mobile.provider

import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.http.HttpMethod
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.JsonResponse
import io.reactivex.Single

internal interface JsonAdProvider<T> : GenericAdProvider<JsonResponse<T>>
{
    fun getJsonClass(): Class<T>

    override fun send(connectionRequest: ConnectionRequest, connectionService: ConnectionService6, httpMethod: HttpMethod) = Single.fromPublisher(
        when (httpMethod)
        {
            HttpMethod.GET -> connectionService.reactiveGetForJson(connectionRequest, getJsonClass())
            HttpMethod.POST -> connectionService.reactivePostForJson(connectionRequest, getJsonClass())
            else -> throw UnsupportedOperationException("Http method not supported")
        }
    )
}
