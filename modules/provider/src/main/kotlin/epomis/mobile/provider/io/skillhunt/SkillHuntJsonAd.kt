package epomis.mobile.provider.io.skillhunt

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
internal class SkillHuntJsonAd(val company: String,
                               val position: String,
                               val id: String,
                               val slug: String?,
                               val location: SkillHuntJsonAdLocation,
                               val closed: String)

@JsonIgnoreProperties(ignoreUnknown = true)
internal class SkillHuntJsonAdLocation(val city: String)
