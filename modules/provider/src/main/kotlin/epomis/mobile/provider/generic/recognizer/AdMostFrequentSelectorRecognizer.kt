package epomis.mobile.provider.generic.recognizer

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter
import epomis.mobile.provider.generic.recognizer.extractor.GenericAdExtractor
import epomis.mobile.provider.generic.selector.SelectorExtractor
import epomis.mobile.provider.generic.selector.SelectorSanitizer

import io.reactivex.Flowable

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

internal class AdMostFrequentSelectorRecognizer(private val selectorExtractor: SelectorExtractor,
                                                private val adFacade: GenericAdExtractor,
                                                private val selectorSanitizer: SelectorSanitizer) : AdRecognizer
{
    override fun recognize(document: Document, adFilter: AdFilter): Flowable<Ad> = Flowable
        .fromIterable(toAdElements(document.body(), adFilter))
        .map { adFacade.buildAdFromElement(it, adFilter) }
        .flatMap { it.toFlowable() }

    private fun toAdElements(body: Element, adFilter: AdFilter): Set<Element>
    {
        val mostFrequentSelector = selectorExtractor.findMostFrequentCssSelector(body, adFilter)

        return mostFrequentSelector?.let { extractElementsByCssSelector(body, it) } ?: emptySet()
    }

    private fun extractElementsByCssSelector(body: Element, selector: String): Set<Element> = body
        .allElements
        .asSequence()
        .filter { element -> selectorSanitizer.cssSelectorFilter(element, selector) }
        .toSet()
}
