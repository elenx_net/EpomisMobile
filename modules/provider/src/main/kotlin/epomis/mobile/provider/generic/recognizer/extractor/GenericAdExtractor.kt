package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter

import io.reactivex.Maybe

import org.jsoup.nodes.Element

interface GenericAdExtractor
{
    fun buildAdFromElement(parentElement: Element, adFilter: AdFilter): Maybe<Ad>
}