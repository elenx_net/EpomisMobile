package epomis.mobile.provider.pl.pracamoney

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse


import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.net.URL
import kotlin.math.max

import kotlin.text.StringBuilder

private const val URL = "https://praca.money.pl/oferty,pracy,wyszukaj,0"
private const val PROVIDER_NAME = "pracamoney.pl"

private const val CLASS_AD = "lista-ofert"
private const val CLASS_AD_TITLE = "oferta"
private const val CLASS_AD_COMPANY = "firma"
private const val CLASS_AD_LOCATION = "city"
private const val CLASS_AD_DESCRIPTION = "opis"
private const val CLASS_PAGINATION = "pager"

private const val ATTR_A = "a"
private const val ATTR_ID = "id"
private const val ATTR_TR = "tr"
private const val ATTR_SPAN = "span"
private const val ATTR_HREF = "href"
private const val ATTR_TBODY = "tbody"

private const val EMPTY_URL_MESSAGE = "Could not parse url"
private const val EMPTY_TITLE_MESSAGE = "Could not parse title"

private const val CITY_PARAMETER = "miasto"
private const val KEYWORD_PARAMETER = "slowo"

internal class PracaMoneyPlProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(CLASS_AD)
            .firstOrNull()
            ?.getElementsByTag(ATTR_TBODY)
            ?.firstOrNull()
            ?.getElementsByTag(ATTR_TR)
            ?.filter { it.hasAttr(ATTR_ID) }
            ?.toSet()
            .orEmpty()

    override fun extractHref(ad: Element) = ad
            .getElementsByTag(ATTR_A)
            .firstOrNull()
            ?.getElementsByAttribute(ATTR_HREF)
            ?.firstOrNull()
            ?.text()
            ?: throw ProviderException(PROVIDER_NAME, EMPTY_URL_MESSAGE)

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(CLASS_AD_TITLE)
            .firstOrNull()
            ?.text()
            ?: throw ProviderException(PROVIDER_NAME, EMPTY_TITLE_MESSAGE)

    override fun extractCompany(ad: Element) = ad
            .getElementsByClass(CLASS_AD_COMPANY)
            .firstOrNull()
            ?.text()
            .orEmpty()

    override fun extractLocation(ad: Element) = ad
            .getElementsByClass(CLASS_AD_LOCATION)
            .firstOrNull()
            ?.text()
            .orEmpty()

     fun extractDescription(ad: Element) = ad
            .getElementsByClass(CLASS_AD_DESCRIPTION)
            .firstOrNull()
            ?.text()
            .orEmpty()


    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("${buildString(pageNumber)}?$KEYWORD_PARAMETER=$keyword&$CITY_PARAMETER=$location")

    private fun buildString(pageNumber: Int) =
        StringBuilder(URL)
                .append(
                        if (pageNumber == 1) ".html"
                        else "-p${pageNumber - 1}.html")
                .toString()

    override fun furthestKnownPageNumber(response: HtmlResponse): Int
    {
        val pager = response.document.getElementsByClass(CLASS_PAGINATION).firstOrNull()
        val linkValue = obtainMaxValueFromLink(pager)
        val spanValue = obtainMaxValueFromSpan(pager)

        return max(spanValue, linkValue)
    }

    private fun obtainMaxValueFromSpan(pager: Element?) = pager
            ?.getElementsByTag(ATTR_SPAN)
            ?.text()
            ?.filter { it.isDigit() }
            ?.toInt()
            ?: 1

    private fun obtainMaxValueFromLink(pager: Element?) = pager
            ?.getElementsByTag(ATTR_A)
            ?.let { it[it.size - 2] }
            ?.text()
            ?.toInt()
            ?: 1
}
