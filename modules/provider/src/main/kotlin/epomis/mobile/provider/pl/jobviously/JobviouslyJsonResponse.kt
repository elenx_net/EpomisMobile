package epomis.mobile.provider.pl.jobviously

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
internal class JobviouslyJsonResponse(@JsonProperty("total_count") val totalCount: Int,
                                      val page: Int,
                                      @JsonProperty("count_per_page") val countPerPage: Int,
                                      val offers: List<JobviouslyJsonAd>)

@JsonIgnoreProperties(ignoreUnknown = true)
internal class JobviouslyJsonAd(val id: Int,
                                val position: String,
                                val slug: String,
                                val name: String,
                                val address: Address)

@JsonIgnoreProperties(ignoreUnknown = true)
internal class Address(val city: String)