package epomis.mobile.provider.generic.recognizer.extractor

import epomis.mobile.model.AdFilter
import epomis.service.url.UrlValidator
import epomis.utils.toMaybe
import io.reactivex.Maybe
import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

class HrefExtractor(private val titleExtractor: TitleExtractor,
                    private val urlValidator: UrlValidator) : InformationExtractor
{
    override fun extract(parentElement: Element, filter: AdFilter): Maybe<String> = titleExtractor
                .extract(parentElement, filter)
                .flatMap { extractElementContainingTitle(it, parentElement) }
                .map { urlValidator.makeUrl(parentElement.baseUri(), it.attr("href")) }
                .switchIfEmpty(fromHref(parentElement, filter))

    private fun fromHref(parentElement: Element,  filter: AdFilter): Maybe<String>
    {
        val optionalHref = parentElement
                .getElementsByTag("a")
                .asSequence()
                .filter{ element -> containsKeyWord(element, filter.keyword) }
                .map{ it.attr("href") }
                .orEmpty()

        return if(optionalHref.any())
        {
            optionalHref
                    .first()
                    .let{ href -> urlValidator.makeUrl(parentElement.baseUri(), href)}
                    .toMaybe()
        }
        else Maybe.empty()
    }

    private fun containsKeyWord(element: Element, keyWord: String) = StringUtils.containsIgnoreCase(element.toString(), keyWord)

    private fun extractElementContainingTitle(title: String, parentElement: Element): Maybe<Element> = parentElement
            .getElementsContainingOwnText(title)
            .toMaybe()
            .map { containsSpecificTag(it) }
            .filter { it.isNotEmpty() }
            .map { it.first() }

    private fun containsSpecificTag(elements: Elements) = elements
            .filter { StringUtils.equalsIgnoreCase("a", it.tagName()) }
}