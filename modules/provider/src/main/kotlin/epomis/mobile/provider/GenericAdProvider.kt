package epomis.mobile.provider

import epomis.mobile.model.Ad
import epomis.service.connection6.ConnectionService6
import epomis.service.connection6.http.HttpMethod
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.request.DataEntry
import io.reactivex.Single
import java.net.URL

interface GenericAdProvider<RESPONSE>
{
    fun urlFor(keyword: String, location: String, pageNumber: Int): URL
    fun extractAdsFrom(response: RESPONSE): Set<Ad>
    fun furthestKnownPageNumber(response: RESPONSE): Int
    fun send(connectionRequest: ConnectionRequest, connectionService: ConnectionService6, httpMethod: HttpMethod): Single<RESPONSE>

    fun requestFor(keyword: String, location: String, pageNumber: Int): ConnectionRequest =
        ConnectionRequest(
            url = urlFor(keyword, location, pageNumber).toString(),
            headers = headersFor(keyword, location, pageNumber),
            data = dataEntriesFor(keyword, location, pageNumber)
        )

    fun dataEntriesFor(keyword: String, location: String, pageNumber: Int): List<DataEntry> = emptyList()

    fun methodFor(keyword: String, location: String, pageNumber: Int) = HttpMethod.GET

    fun headersFor(keyword: String, location: String, pageNumber: Int): Map<String, String> = emptyMap()
}
