package epomis.mobile.provider.pl.gowork

import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document

import org.jsoup.nodes.Element

import java.net.URL

private const val URL = "https://www.gowork.pl"
private const  val PROVIDER_NAME = "gowork.pl"

private const val CLASS_AD = "offer"
private const val CLASS_AD_TITLE = "offer__title"
private const val `CLASS_AD_COMPANY` = "company-name"
private const val CLASS_AD_LOCATION = "offer__subtitle"
private const val CLASS_PAGINATION = "pagination"

private const val ATTRIBUTE_HREF = "href"

private const val LOCATION_SPLIT_STRING = " ) "

internal class GoWorkProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
        .getElementsByClass(CLASS_AD)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) =
        URL("$URL/praca/$keyword;st/$location;l/$pageNumber;pg")

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
        .document
        .getElementsByClass(CLASS_PAGINATION)
        .asSequence()
        .map(Element::text)
        .filter(StringUtils::isNumeric)
        .map(Integer::parseInt)
        .max() ?: 1

    override fun extractTitle(ad: Element) = ad
        .getElementsByClass(CLASS_AD_TITLE)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractCompany(ad: Element) = ad
        .getElementsByClass(`CLASS_AD_COMPANY`)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractLocation(ad: Element) = ad
        .getElementsByClass(CLASS_AD_LOCATION)
        .firstOrNull()
        ?.text()
        ?.substringAfter(LOCATION_SPLIT_STRING)
        .orEmpty()

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_AD_TITLE)
        .firstOrNull()
        ?.getElementsByTag("a")
        ?.firstOrNull()
        ?.attr(ATTRIBUTE_HREF)
        ?.let { URL + it }
        .orEmpty()
}
