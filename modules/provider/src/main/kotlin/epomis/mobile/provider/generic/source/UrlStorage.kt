package epomis.mobile.provider.generic.source

class UrlStorage(list: List<String> = listOf(
    "http://www.amundio.pl/praca/?q=java&l=warszawa&distance=0.06&lat=52.2296756&lng=21.0122287&p=%d",
    "https://bulldogjob.pl/companies/jobs/s/q,java warszawa?page=%d",
    "https://pl.indeed.com/praca?q=java&l=warszawa&start=%d",
    "http://www.infopraca.pl/praca?q=java&lc=warszawa&pg=%d",
    "http://praca.money.pl/oferty,pracy,wyszukaj,0-p1.html?slowo=java+warszawa&zawod=&wojewodztwo=&okres=",
    "http://www.praca.egospodarka.pl/oferty-pracy/m,warszawa_s,java-strona-%d.html",
    "https://www.adzuna.pl/search?loc=132658&p=%d&q=java",
    "http://jobfinder.pl/strefa-kandydata/wyszukiwarka-ofert-pracy/?fraza=java&kraj=polska&str=%d",
    "https://www.glassdoor.com/Job/bukowno-warszawa-developer-jobs-SRCH_IL.0,16_IC3058838_KO17,26_IP%d.htm?lst=-1",
    "https://www.olx.pl/praca/informatyka/q-java/?page=%d",
    "https://www.jobdesk.pl/employee/view_adverts.php?action=1&keyword=Java&category=0&job_type=0&region=0&city=Warszawa&page=%d"
)) : List<String> by list