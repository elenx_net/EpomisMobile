package epomis.mobile.provider.io.whoishiring

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.response.JsonResponse

import java.net.URL
import java.net.URLEncoder

private const val SEARCH_URL = "https://search.whoishiring.io/item/item/_search"
private const val SITE_AD_ENDPOINT = "https://whoishiring.io/s/"
private const val PROVIDER_NAME = "whoishiring.io"

internal class WhoIsHiringProvider : JsonAdProvider<WhoIsHiringResponse>
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int)
        = URL("$SEARCH_URL?q=title:%22${URLEncoder.encode(keyword, "UTF-8")}%22%20AND%20city:%22${URLEncoder.encode(location, "UTF-8")}%22&size=20")

    override fun extractAdsFrom(response: JsonResponse<WhoIsHiringResponse>): Set<Ad> = extractDataFrom(response)
        .asSequence()
        .map(WhoIsHiringAdData::ad)
        .map(this::extractAd)
        .toSet()

    override fun furthestKnownPageNumber(response: JsonResponse<WhoIsHiringResponse>): Int = 1

    override fun getJsonClass(): Class<WhoIsHiringResponse> = WhoIsHiringResponse::class.java

    private fun extractAd(ad: WhoIsHiringAd): Ad = Ad(
        href = SITE_AD_ENDPOINT + ad.id,
        title = ad.title,
        company = ad.company,
        location = ad.city,
        provider = PROVIDER_NAME
    )

    private fun extractDataFrom(response: JsonResponse<WhoIsHiringResponse>): List<WhoIsHiringAdData> = response
        .json
        .searchResult
        .data
}
