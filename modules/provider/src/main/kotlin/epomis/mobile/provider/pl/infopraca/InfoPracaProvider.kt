package epomis.mobile.provider.pl.infopraca

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.net.URL

private const val URL = "https://www.infopraca.pl/"
private const val PROVIDER_NAME = "infopraca.pl"

private const val CLASS_AD = "job-offer-content"
private const val CLASS_PAGINATION = "pagination"
private const val CLASS_TITLE = "job-offer"
private const val CLASS_LOCATION = "p-locality"
private const val CLASS_COMPANY = "company"

private const val ATTR_HREF = "href"

internal class InfoPracaProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document): Set<Element> = document
        .getElementsByClass(CLASS_AD)
        ?.toSet()
        .orEmpty()

    override fun extractHref(ad: Element): String = ad
        .getElementsByTag("a")
        .firstOrNull()
        ?.getElementsByAttribute(ATTR_HREF)
        ?.firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractTitle(ad: Element): String = ad
        .getElementsByClass(CLASS_TITLE)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractLocation(ad: Element): String = ad
        .getElementsByClass(CLASS_LOCATION)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractCompany(ad: Element): String = ad
        .getElementsByClass(CLASS_COMPANY)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL =
        URL("${URL}praca?q=$keyword&lc=$location&pg=$pageNumber")

    override fun furthestKnownPageNumber(response: HtmlResponse): Int = response
        .document
        .getElementsByClass(CLASS_PAGINATION)
        .firstOrNull()
        ?.getElementsByTag("li")
        ?.asSequence()
        ?.map(Element::text)
        ?.filter(StringUtils::isNumeric)
        ?.map(Integer::parseInt)
        ?.max() ?: 1

}