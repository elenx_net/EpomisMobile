package epomis.mobile.provider.pl.olx

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider

import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document

import org.jsoup.nodes.Element

import java.net.URL

import org.jsoup.select.Elements
import java.lang.StringBuilder

private const val CLASS_OFFER = "wrap"
private const val CLASS_OFFER_CITY = "breadcrumb x-normal"
private const val CLASS_OFFER_H3 = "h3"
private const val CLASS_PAGE = "item fleft"
private const val CLASS_PAGE_ATTR = "span"
private const val CLASS_OFFER_STRONG = "strong"
private const val PAGE = "page"
private const val ATTR_HREF = "href"

private const val URL = "https://www.olx.pl/praca/"
private const val PROVIDER_NAME = "olx.pl"

internal class OlxProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(CLASS_OFFER)
            .toSet()

    override fun extractHref(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER)
            .firstOrNull()
            ?.getElementsByTag(CLASS_OFFER_H3)
            ?.firstOrNull()
            ?.attr(ATTR_HREF)
            ?: throw ProviderException(PROVIDER_NAME, "Could not parse url")

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER)
            .firstOrNull()
            ?.getElementsByTag(CLASS_OFFER_STRONG)
            ?.text()
            ?: throw ProviderException(PROVIDER_NAME, "Could not parse title")

    override fun extractLocation(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER_CITY)
            .firstOrNull()
            ?.text()
            .orEmpty()

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL {

        val urlBuilder = StringBuilder()

        location.takeIf { it.isNotBlank() }
                ?.let { urlBuilder.append("$it/") }

        keyword.takeIf { it.isNotBlank() }
                ?.let { urlBuilder.append("q-$it/") }

        urlBuilder.append("?$PAGE=$pageNumber")


        return URL("$URL$urlBuilder")
    }

    override fun furthestKnownPageNumber(htmlResponse: HtmlResponse) = htmlResponse
            .document
            .getElementsByClass(CLASS_PAGE)
            ?.let { extractLastPageNumber(it) }
            ?: 1

    private fun extractLastPageNumber(elements: Elements) = elements
            .lastOrNull()
            ?.getElementsByTag(CLASS_PAGE_ATTR)
            ?.lastOrNull()
            ?.text()
            ?.toInt()
            ?: 1

}