package epomis.mobile.provider.generic.poc

import epomis.mobile.model.Ad
import epomis.mobile.provider.GenericAdProvider
import java.net.URL

/**
 * Own crawler PoC with reactive stream to present idea of Ad streaming and processing.
 * This code should be deleted after integrating better crawler with Epomis.
*/
class Cralwer
{
    private val parallelism: Int = 4
    private val cs: CS? = null

    /*fun publishJobAdsFrom(jobAdProviders: Set<AbstractAdProvider<Any>>, keyword: String, location: String) = Flux
        .fromIterable(jobAdProviders)
        .flatMap { contextOf(it, keyword, location) }
        .expand { loop(it) }
        .flatMapIterable { it.jobAds }
        .parallel(parallelism)

    private fun <RESPONSE> contextOf(jobAdProvider: AbstractAdProvider<RESPONSE>, keyword: String, location: String) = IntStream
        .rangeClosed(1, parallelism)
        .mapToObj { CrawlingContext(it, keyword, location, jobAdProvider, setOf()) }
        .toFlux()

    fun <RESPONSE> loop(crawlingContext: CrawlingContext<RESPONSE>) = Mono
        .fromSupplier { crawlingContext.step(parallelism, cs!!.request(crawlingContext.url())) }*/
}

data class CrawlingContext<RESPONSE>(val pageNumber: Int, val keyword: String, val location: String, private val jobAdProvider: GenericAdProvider<RESPONSE>, val jobAds: Set<Ad>)
{
    fun url() = jobAdProvider.urlFor(keyword, location, pageNumber)

    fun step(stepSize: Int, response: RESPONSE) = copy(pageNumber = pageNumber + stepSize, jobAds = jobAds + extractAdsFrom(response))

    private fun extractAdsFrom(response: RESPONSE) = jobAdProvider.extractAdsFrom(response)
}

interface CS
{
    fun <RESPONSE> request(url: URL): RESPONSE
}
