package epomis.mobile.provider.generic.selector

import java.util.regex.Pattern

import org.apache.commons.lang3.StringUtils

import org.jsoup.select.Selector
import org.jsoup.nodes.Element

private val idPattern = Pattern.compile("\\S* ?#\\S*")
private val childPattern = Pattern.compile("child\\(\\d*\\)")

class SelectorSanitizerImpl : SelectorSanitizer
{
    override fun toGenericSelector(cssSelector: String): String
    {
        var selectedCss = cssSelector
        selectedCss = idPattern.matcher(selectedCss).replaceAll("id")
        selectedCss = childPattern.matcher(selectedCss).replaceAll("child")

        return selectedCss
    }

    override fun cssSelectorFilter(element: Element, cssSelector: String): Boolean
        = try
        {
            StringUtils.equalsAnyIgnoreCase(cssSelector, toGenericSelector(element.cssSelector()))
        }
        catch (parseException: Selector.SelectorParseException)
        {
            false
        }
        catch (parseException: IllegalArgumentException)
        {
            false
        }
}