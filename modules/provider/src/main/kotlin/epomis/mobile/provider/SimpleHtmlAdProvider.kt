package epomis.mobile.provider

import epomis.mobile.model.Ad
import epomis.service.connection6.response.HtmlResponse
import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

internal interface SimpleHtmlAdProvider : HtmlAdProvider
{
    val providerName: String

    fun selectAdElements(document: Document): Set<Element>
    fun extractHref(ad: Element): String
    fun extractTitle(ad: Element): String
    fun extractLocation(ad: Element): String = StringUtils.EMPTY
    fun extractCompany(ad: Element): String = StringUtils.EMPTY

    override fun extractAdsFrom(response: HtmlResponse): Set<Ad> =
        adaptElementsToAds(selectAdElements(response.document))

    private fun adaptElementToAd(element: Element): Ad =
        Ad(
            title = extractTitle(element),
            location = extractLocation(element),
            company = extractCompany(element),
            href = extractHref(element),
            provider = providerName
        )

    private fun adaptElementsToAds(elements: Set<Element>): Set<Ad> = elements
        .map { adaptElementToAd(it) }
        .toSet()
}
