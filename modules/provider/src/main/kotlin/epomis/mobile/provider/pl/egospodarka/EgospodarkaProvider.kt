package epomis.mobile.provider.pl.egospodarka

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document

import org.jsoup.nodes.Element

import java.net.URL

import org.jsoup.select.Elements
import java.lang.StringBuilder

private const val CLASS_OFFER = "oferta"
private const val CLASS_OFFER_CITY = "opis"
private const val CLASS_OFFER_H3 = "h3"
private const val CLASS_OFFER_WORKPLACE = "miejsce pracy:"
private const val CLASS_OFFER_CATEGORY = "oferta w kategorii:"

private const val ATTR_HREF = "href"
private const val ATTR_TAG = "art-pag-2"
private const val ATTR_A = "A"
private const val TAG_A = "a"

private const val URL = "http://www.praca.egospodarka.pl/oferty-pracy/"
private const val URL_SHORT = "http://www.praca.egospodarka.pl"
private const val PROVIDER_NAME = "egospodarka.pl"
private const val PAGE = "strona"

internal class EgospodarkaProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(CLASS_OFFER)
            .toSet()

    override fun extractHref(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER)
            .firstOrNull()
            ?.getElementsByTag(TAG_A)
            ?.firstOrNull()
            ?.attr(ATTR_HREF)
            ?.let { URL_SHORT + it }
            ?: throw ProviderException(PROVIDER_NAME, "Could not parse url")

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER)
            .firstOrNull()
            ?.getElementsByTag(CLASS_OFFER_H3)
            ?.text()
            ?: throw ProviderException(PROVIDER_NAME, "Could not parse title")

    override fun extractLocation(ad: Element) = ad
            .getElementsByClass(CLASS_OFFER_CITY)
            .firstOrNull()
            ?.text()
            ?.substringAfter(CLASS_OFFER_WORKPLACE)
            ?.substringBefore(CLASS_OFFER_CATEGORY)
            .orEmpty()

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL {

        val urlBuilder = StringBuilder()

        location.takeIf { it.isNotBlank() }
                ?.let { urlBuilder.append("m,${it}_") }

        keyword.takeIf { it.isNotBlank() }
                ?.let { urlBuilder.append("s,$it") }

        if (keyword.isBlank() && location.isBlank())
            urlBuilder.append("$PAGE-$pageNumber")
        else
            urlBuilder.append("-$PAGE-$pageNumber")

        return URL("$URL$urlBuilder.html")
    }

    override fun furthestKnownPageNumber(htmlResponse: HtmlResponse) = htmlResponse
            .document
            .getElementsByClass(ATTR_TAG)
            ?.let { extractLastPageNumber(it) }
            ?: 1

    private fun extractLastPageNumber(elements: Elements) = elements
            .lastOrNull()
            ?.getElementsByTag(ATTR_A)
            ?.lastOrNull()
            ?.text()
            ?.toInt()
            ?: 1

}


