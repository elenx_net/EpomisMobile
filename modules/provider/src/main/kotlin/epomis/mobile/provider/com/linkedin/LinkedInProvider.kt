package epomis.mobile.provider.com.linkedin

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.net.URL

private const val URL = "https://www.linkedin.com/jobs/search/"
private const val PROVIDER_NAME = "linkedin.com"

private const val ADS_CLASS = "jobs-search-result-item"
private const val PAGE_CLASS = "pagination__button"
private const val HREF_CLASS = "listed-job-posting--is-link"
private const val HREF_ATTR = "href"
private const val TITLE_CLASS = "listed-job-posting__title"
private const val COMPANY_CLASS = "listed-job-posting__company"
private const val LOCATION_CLASS = "listed-job-posting__location"
private const val DESCRIPTION_CLASS = "listed-job-posting__description"

internal class LinkedInProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(ADS_CLASS)
            .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL?keywords=$keyword&location=$location&start=${pageNumber*25-25}")

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
            .document
            .getElementsByClass(PAGE_CLASS)
            .lastOrNull()
            ?.text()
            ?.toInt() ?: throw ProviderException(providerName, "Could not parse page number")

    override fun extractHref(ad: Element) = ad
            .getElementsByClass(HREF_CLASS)
            ?.attr(HREF_ATTR)
            ?.split('?')
            ?.firstOrNull() ?: throw ProviderException(providerName, "Could not parse url")

    override fun extractTitle(ad: Element) = ad
            .getElementsByClass(TITLE_CLASS)
            .firstOrNull()
            ?.text() ?: throw ProviderException(providerName, "Could not parse title")

    override fun extractCompany(ad: Element) = ad
            .getElementsByClass(COMPANY_CLASS)
            .firstOrNull()
            ?.text() ?: throw ProviderException(providerName, "Could not parse title")

    override fun extractLocation(ad: Element) = ad
            .getElementsByClass(LOCATION_CLASS)
            .firstOrNull()
            ?.text() ?: throw ProviderException(providerName, "Could not parse location ")
}
