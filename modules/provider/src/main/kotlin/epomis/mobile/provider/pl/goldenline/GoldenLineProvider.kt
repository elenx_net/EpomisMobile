package epomis.mobile.provider.pl.goldenline

import epomis.mobile.model.Ad
import epomis.mobile.provider.HtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils


import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.lang.StringBuilder

import java.net.URL

import java.util.Collections

private const val URL = "https://www.goldenline.pl/praca"

private const val PROVIDER = "pl.goldenline"
private const val SELECTOR = "div.offers-listing li.stats-data-offer"
private const val PAGER_CLASS = "total"
private const val CURRENT_CLASS = "current"

internal class GoldenLineProvider : HtmlAdProvider
{
    override fun extractAdsFrom(response: HtmlResponse): Set<Ad> = response
        .document
        .select(SELECTOR)
        .asSequence()
        .map(this::extractOffer)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL {

        val urlBuilder = StringBuilder()

        if (isPageNumberGreaterThanOne(pageNumber))
            urlBuilder.append("/szukaj$pageNumber?query=$keyword&locations=$location")
        else
            urlBuilder.append("/szukaj?query=$keyword&locations=$location")

        return URL("$URL$urlBuilder")
    }


    override fun furthestKnownPageNumber(response: HtmlResponse) = response
                   .document
                   .getElementsByClass(PAGER_CLASS)
                   .first()
                   ?.let {Collections.max(extractPageNumbers(it))} ?: 1


    private fun extractOffer(ad: Element): Ad
    {
        val jobTitle = ad.getElementsByClass("position")[0].text()
        val company = ad.getElementsByClass("position")[0].text()
        val city = ad.getElementsByTag("span")[2].text()
        val href = ad.getElementsByTag("a").attr("href")

        return Ad(
            href = href,
            title = jobTitle,
            company = company,
            location = city,
            provider = PROVIDER)
    }

    private fun extractPageNumbers(pageNumberList: Element): List<Int> = pageNumberList
        .getElementsByTag("li")
        .asSequence()
        .filter(this::hasPageNumber)
        .map { element -> element.getElementsByTag("a") }
        .map(Elements::first)
        .map(Element::text)
        .filter(StringUtils::isNumeric)
        .map(Integer::parseInt)
        .toList()

    private fun hasPageNumber(element: Element): Boolean = !element.hasClass(CURRENT_CLASS) && !element.hasClass("space")
    private fun isPageNumberGreaterThanOne(pageNumber: Int): Boolean = ((pageNumber) > 1)
}
