package epomis.mobile.provider.pl.lepszapraca

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.response.JsonResponse
import org.apache.commons.lang3.StringUtils
import java.net.URL

private const val BASE_URL = "https://www.lepszapraca.pl/"
private const val PROVIDER_NAME = "lepszapraca.pl"

internal class LepszaPracaPlProvider : JsonAdProvider<LepszaPracaJsonResponse>
{
    override fun getJsonClass(): Class<LepszaPracaJsonResponse> = LepszaPracaJsonResponse::class.java

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("${BASE_URL}ajaxsearch?q=${keyword}%20${location}&p=${pageNumber-1}&filters=e30%3D")

    override fun extractAdsFrom(response: JsonResponse<LepszaPracaJsonResponse>) =
        response
            .json
            .hits
            .asSequence()
            .map(this::extractAd)
            .toSet()

    override fun furthestKnownPageNumber(response: JsonResponse<LepszaPracaJsonResponse>) = 1

    private fun extractAd(ad: LepszaPracaAdJson): Ad
    {
        val href = ad.url
        val title = ad.title
        val location = ad.location.city
        return Ad(href, title, StringUtils.EMPTY, location, PROVIDER_NAME)
    }
}
