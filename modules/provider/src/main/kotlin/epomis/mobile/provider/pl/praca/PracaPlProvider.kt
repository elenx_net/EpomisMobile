package epomis.mobile.provider.pl.praca

import epomis.mobile.model.Ad
import epomis.mobile.provider.HtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils

import org.jsoup.nodes.Element

import java.net.URL

private const val URL = "http://www.praca.pl/"
private const val PROVIDER_NAME = "praca.pl"

private const val PAGINATION_CLASS = "pagination"
private const val PAGINATION_ITEM_CLASS = "pagination__item"
private const val OFFER_CLASS = "listing__item"
private const val TITLE_CLASS = "listing__offer-title"
private const val COMPANY_CLASS = "listing__employer-name"
private const val LOCATION_CLASS = "listing__location"
private const val MULTI_LOCATION_CLASS = "listing__multiregion-link"
private const val EMPTY_STRING = ""

internal class PracaPlProvider : HtmlAdProvider
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("${URL}s-$keyword,${location}_$pageNumber")
    override fun extractAdsFrom(response: HtmlResponse): Set<Ad> = response
            .document
            .getElementsByClass(OFFER_CLASS)
            .asSequence()
            .map(this::extractAd)
            .toSet()

    override fun furthestKnownPageNumber(response: HtmlResponse): Int = response
            .document
            .getElementsByClass(PAGINATION_CLASS)
            ?.first()
            ?.getElementsByClass(PAGINATION_ITEM_CLASS)
            ?.asSequence()
            ?.map(Element::text)
            ?.filter(StringUtils::isNumeric)
            ?.map(String::toInt)
            ?.toSet()
            ?.max() ?: 1

    private fun extractAd(ad: Element): Ad
    {
        val titleTag = ad
                .getElementsByClass(TITLE_CLASS)
                .first()

        return Ad(href = URL + (extractMultiLocationHref(ad) ?: extractHref(titleTag)),
                title =  extractTitle(titleTag),
                company = extractCompany(ad),
                location = extractLocation(ad) ?: extractMultiLocation(ad),
                provider = PROVIDER_NAME)
    }

    private fun extractTitle(element: Element?) = element?.attr("title") ?: EMPTY_STRING

    private fun extractHref(element: Element?) = element?.attr("href") ?: EMPTY_STRING

    private fun extractMultiLocationHref(element: Element) = element
            .getElementsByClass(MULTI_LOCATION_CLASS)
            .first()
            ?.attr("href")

    private fun extractCompany(element: Element) = element
            .getElementsByClass(COMPANY_CLASS)
            .first()
            ?.text() ?: EMPTY_STRING

    private fun extractLocation(element: Element) = element
            .getElementsByClass(LOCATION_CLASS)
            .first()
            ?.text()

    private fun extractMultiLocation(element: Element) = element
            .getElementsByClass(MULTI_LOCATION_CLASS)
            .asSequence()
            .map { it.text() }
            .filterNot { it.isEmpty() }
            .reduce { acc, location -> "$acc, $location" }
}
