package epomis.mobile.provider.pl.uget

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.response.JsonResponse

import org.apache.commons.lang3.StringUtils

import java.net.URL

private const val REST_URL = "http://jobs.u-get.pro/offers.json"
private const val PROVIDER_NAME = "uget.pl"

internal class UGetProvider : JsonAdProvider<Array<UGetData>>
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int): URL = URL(REST_URL)

    override fun extractAdsFrom(response: JsonResponse<Array<UGetData>>): Set<Ad> = response
            .json
            .asSequence()
            .map(this::extractAd)
            .toSet()

    override fun furthestKnownPageNumber(response: JsonResponse<Array<UGetData>>): Int = 1

    override fun getJsonClass(): Class<Array<UGetData>> = Array<UGetData>::class.java

    private fun extractAd(jsonAdData: UGetData): Ad
    {
        val title = jsonAdData.title
        val href = jsonAdData.id
        val location = if(jsonAdData.remote) "Remote" else jsonAdData.getAddressLabel()!!

        return Ad(
            href = REST_URL + href,
            title = title,
            company = StringUtils.EMPTY,
            location = location,
            provider = PROVIDER_NAME
        )
    }
}
