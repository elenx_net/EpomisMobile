package epomis.mobile.provider.generic.selector

import epomis.mobile.model.AdFilter
import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Element
import org.jsoup.select.Selector

class CssSelectorExtractor(private val selectorSanitizer: SelectorSanitizer) : SelectorExtractor
{
    override fun findMostFrequentCssSelector(body: Element, adFilter: AdFilter): String? = body
        .allElements
        .toList()
        .filter { StringUtils.containsIgnoreCase(it.text(), adFilter.location) }
        .filter { StringUtils.containsIgnoreCase(it.text(), adFilter.keyword) }
        .mapNotNull { cssSelector(it) }
        .filter { it.isNotEmpty() }
        .map { selectorSanitizer.toGenericSelector(it) }
        .groupingBy { it }
        .eachCount()
        .maxBy { it.value }
        ?.key

    private fun cssSelector(element: Element) = try
    {
        element.cssSelector()
    }
    catch (parseException: Selector.SelectorParseException)
    {
        null
    }
}

