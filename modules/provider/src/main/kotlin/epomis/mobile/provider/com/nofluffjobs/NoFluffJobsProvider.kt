package epomis.mobile.provider.com.nofluffjobs

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider

import epomis.service.connection6.response.HtmlResponse

import java.net.URL

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

private const val URL = "https://nofluffjobs.com"
private const val PROVIDER_NAME = "nofluffjobs.com"
private const val CLASS_AD = "posting-list-item"
private const val TAG_AD_LOCATION = "nfj-posting-item-city"
private const val LOCATION_PREFIX = ", "
private const val CLASS_AD_NAME = "posting-title__position"
private const val CLASS_AD_EMPLOYER = "posting-title__company"
private const val HREF = "href"

internal class NoFluffJobsProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
        .getElementsByClass(CLASS_AD)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL/jobs/$location/$keyword?page=$pageNumber")

    override fun furthestKnownPageNumber(response: HtmlResponse) = 1

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_AD)
        .firstOrNull()
        ?.attr(HREF) ?: throw ProviderException(providerName, "Could not parse url")

    override fun extractLocation(ad: Element) = ad
        .getElementsByTag(TAG_AD_LOCATION)
        .firstOrNull()
        ?.text()
        ?.replaceAfter(LOCATION_PREFIX, "")
        ?.replace(",", "")
        .orEmpty()

    override fun extractTitle(ad: Element) = ad
        .getElementsByClass(CLASS_AD_NAME)
        .firstOrNull()
        ?.text() ?: throw ProviderException(providerName, "Could not parse title")

    override fun extractCompany(ad: Element) = ad
        .getElementsByClass(CLASS_AD_EMPLOYER)
        .firstOrNull()
        ?.text()
        ?.replace("w ","")
        .orEmpty()
}
