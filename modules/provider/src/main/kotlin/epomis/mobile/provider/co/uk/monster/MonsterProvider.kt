package epomis.mobile.provider.co.uk.monster

import epomis.mobile.provider.ProviderException
import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import kotlin.math.ceil
import java.net.URL

private const val URL = "https://www.monster.co.uk/jobs/search/"
private const val PROVIDER_NAME = "monster.co.uk"

private const val CLASS_OFFERS = "mux-card mux-job-card"
private const val CLASS_OFFER_TITLE = "title"
private const val CLASS_OFFER_COMPANY = "company"
private const val CLASS_OFFER_LOCATION = "location"

private const val ATTR_OFFER = "data-jobid"
private const val ATTR_HREF = "href"

private const val OFFERS_PER_PAGE = "data-results-per-page"
private const val SEARCH_INFO = "mux-search-results"
private const val TOTAL_OFFERS_SIZE = "data-results-total"
private const val CURRENT_PAGE = "data-results-page"

private const val URL_KEYWORD_PARAM = "q"
private const val URL_LOCATION_PARAM = "where"
private const val URL_PAGE_PARAM = "page"

internal class MonsterProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
            .getElementsByClass(CLASS_OFFERS)
            .firstOrNull()
            ?.getElementsByAttribute(ATTR_OFFER)
            ?.let { extractOffersForCurrentPageOnly(it, document) }
            ?.toSet()
            .orEmpty()

    override fun extractTitle(ad: Element) = ad
        .getElementsByClass(CLASS_OFFER_TITLE)
        .firstOrNull()
        ?.text()
        ?: throw ProviderException(providerName, "Could not parse title")

    override fun extractCompany(ad: Element) = ad
        .getElementsByClass(CLASS_OFFER_COMPANY)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractLocation(ad: Element) = ad
        .getElementsByClass(CLASS_OFFER_LOCATION)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_OFFER_TITLE)
        .firstOrNull()
        ?.childNodes()
        ?.getOrNull(0)
        ?.attr(ATTR_HREF)
        ?: throw ProviderException(providerName, "Could not parse url")

    override fun furthestKnownPageNumber(response: HtmlResponse) = response
        .document
        .getElementsByClass(SEARCH_INFO)
        .firstOrNull()
        ?.attr(TOTAL_OFFERS_SIZE)
        ?.let { ceil(it.toDouble()/extractOffersPerPage(response.document)) }
        ?.toInt()
        ?: 1

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL?$URL_KEYWORD_PARAM=$keyword&$URL_LOCATION_PARAM=$location&$URL_PAGE_PARAM=$pageNumber")

    private fun extractOffersPerPage(document: Document): Int = document
        .getElementsByClass(SEARCH_INFO)
        .firstOrNull()
        ?.attr(OFFERS_PER_PAGE)
        ?.toInt()
        ?: 25

    private fun extractCurrentPage(document: Document): Int = document
        .getElementsByClass(SEARCH_INFO)
        .firstOrNull()
        ?.attr(CURRENT_PAGE)
        ?.toInt()
        ?: 1

    private fun extractOffersForCurrentPageOnly(offers: Elements, document: Document): List<Element>
    {
        val offersPerPage = extractOffersPerPage(document)
        val currentPage = extractCurrentPage(document)

        return offers.subList(offersPerPage * (currentPage - 1), offers.size)
    }
}