package epomis.mobile.provider.pl.jober

import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.net.URL

private const val URL = "https://www.jober.pl/?a=search"
private const val PROVIDER_NAME = "jober.pl"

private const val CLASS_PAGINATION = "row pagination"
private const val CLASS_ADS = "job-results"
private const val CLASS_LOCATION = "hidden-xs hidden-sm"
private const val TAG_TBODY = "tbody"
private const val TAG_TR = "tr"
private const val TAG_TD = "td"
private const val TAG_A = "a"
private const val TAG_SPAN = "span"
private const val ATTR_HREF = "href"
private const val CORRECT_NUMBER_OF_COLUMNS = 3

internal class JoberProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
        .getElementsByClass(CLASS_ADS)
        .firstOrNull()
        ?.getElementsByTag(TAG_TBODY)
        ?.firstOrNull()
        ?.getElementsByTag(TAG_TR)
        ?.filter { it.children().size == CORRECT_NUMBER_OF_COLUMNS }
        ?.toSet()
        .orEmpty()

    override fun extractHref(ad: Element) = ad
        .getElementsByTag(TAG_TD)
        .getOrNull(1)
        ?.getElementsByTag(TAG_A)
        ?.firstOrNull()
        ?.attr(ATTR_HREF)
        .orEmpty()

    override fun extractTitle(ad: Element) = ad
        .getElementsByTag(TAG_TD)
        .getOrNull(1)
        ?.getElementsByTag(TAG_A)
        ?.text()
        .orEmpty()

    override fun extractCompany(ad: Element) = ad
        .getElementsByTag(TAG_TD)
        .getOrNull(1)
        ?.child(2)
        ?.text()
        .orEmpty()

    override fun extractLocation(ad: Element) = ad
        .getElementsByTag(TAG_TD)
        .getOrNull(2)
        ?.getElementsByClass(CLASS_LOCATION)
        ?.firstOrNull()
        ?.getElementsByTag(TAG_SPAN)
        ?.text()
        .orEmpty()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) =
        URL("$URL&city=$location&position=$keyword&p=$pageNumber")


    override fun furthestKnownPageNumber(response: HtmlResponse) = response
        .document
        .getElementsByClass(CLASS_PAGINATION)
        .firstOrNull()
        ?.getElementsByTag(TAG_A)
        ?.asSequence()
        ?.map(Element::text)
        ?.map(Integer::parseInt)
        ?.max() ?: 1
}