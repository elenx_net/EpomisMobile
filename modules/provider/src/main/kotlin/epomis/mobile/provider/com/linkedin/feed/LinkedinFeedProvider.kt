package epomis.mobile.provider.com.linkedin.feed

import epomis.service.connection6.response.HtmlResponse
import mu.KotlinLogging
import org.jsoup.nodes.Element

private const val CONTENT = "feed-shared-text"
private const val AUTHOR = "feed-shared-actor__name"
private const val LINKEDIN_URL = "https://www.linkedin.com"
private const val ADS_LIMIT = 10

private val log = KotlinLogging.logger {}

internal class LinkedinFeedProvider {

    fun receiveFeeds(response: HtmlResponse) : Set<LinkedinFeedData> = response
        .document
        .getElementsByClass("relative ember-view")
        .asSequence()
        .map { extractFeed(it) }
        .take(ADS_LIMIT)
        .apply { this.forEach {printFeed(it)} }
        .toSet()

    private fun extractFeed(feedElement: Element): LinkedinFeedData
    {
        val author = feedElement.getElementsByClass(AUTHOR).tagName("span").text()
        val content = feedElement.getElementsByClass(CONTENT).tagName("span").text()
        val href = correctHref(feedElement.getElementsByTag("a").attr("href"))

        return LinkedinFeedData(
            author = author,
            content = content,
            authorHref = href
        )
    }

    private fun correctHref(href: String)
        = if (href.length >= 5 && href.substring(0, 5) == "https") href else LINKEDIN_URL + href

    private fun printFeed(linkedinFeedData: LinkedinFeedData) {
        log.info("\n\nFeed author: {} \nFeed author href: {} \nContent:\n{}", linkedinFeedData.author, linkedinFeedData.authorHref, linkedinFeedData.content)
    }
}

