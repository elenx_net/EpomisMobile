package epomis.mobile.provider.pl.pracuj

import epomis.mobile.provider.SimpleHtmlAdProvider
import epomis.service.connection6.response.HtmlResponse

import org.apache.commons.lang3.StringUtils
import org.jsoup.nodes.Document

import org.jsoup.nodes.Element

import java.net.URL

private const val URL = "https://www.pracuj.pl/"
private const val PROVIDER_NAME = "pracuj.pl"

private const val CLASS_AD = "offer"
private const val CLASS_AD_NAME = "offer-details__title-link"
private const val CLASS_AD_EMPLOYER = "offer-company__name"
private const val CLASS_LOCATION = "offer-labels__item--location"
private const val CLASS_PAGINATION = "pagination_element-page"

internal class PracujPlProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
        .getElementsByClass(CLASS_AD)
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("${URL}praca/$keyword;kw/$location;wp?pn=$pageNumber")

    override fun furthestKnownPageNumber(response: HtmlResponse): Int = response
        .document
        .getElementsByClass(CLASS_PAGINATION)
        .asSequence()
        .map(Element::text)
        .filter(StringUtils::isNumeric)
        .map(Integer::parseInt)
        .max() ?: 1

    override fun extractTitle(ad: Element) = ad
        .getElementsByClass(CLASS_AD_NAME)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractCompany(ad: Element) = ad
        .getElementsByClass(CLASS_AD_EMPLOYER)
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractHref(ad: Element) = ad
        .getElementsByClass(CLASS_AD_NAME)
        .firstOrNull()
        ?.attr("href")
        ?.let {URL + it}
        .orEmpty()

    override fun extractLocation(ad: Element): String
    {
        val location =  ad
            .getElementsByClass(CLASS_LOCATION)
            .firstOrNull()
            ?.text()
            .orEmpty()

        return if(canExtractLocationFrom(location))
             location
        else
            return StringUtils.EMPTY
    }

    private fun canExtractLocationFrom(text: String) =  !containsNumber(text)
    private fun containsNumber(text: String) = text.contains(Regex("[0-9]"))
}
