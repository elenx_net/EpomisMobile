package epomis.mobile.provider.pl.jobrapido

import epomis.mobile.provider.SimpleHtmlAdProvider

import epomis.service.connection6.response.HtmlResponse

import org.jsoup.nodes.Document

import org.jsoup.nodes.Element

import java.net.URL

private const val URL = "http://pl.jobrapido.com/"
private const val PROVIDER_NAME = "jobrapido.pl"

internal class JobrapidoProvider : SimpleHtmlAdProvider
{
    override val providerName = PROVIDER_NAME

    override fun selectAdElements(document: Document) = document
        .getElementsByClass("result-item")
        .toSet()

    override fun urlFor(keyword: String, location: String, pageNumber: Int) = URL("$URL?w=${considerKeyword(keyword)}&l=$location&p=$pageNumber")

    private fun considerKeyword(keyword: String): String =
        if(keyword.isBlank()){
            "developer"
        } else {
            keyword
        }

     override fun furthestKnownPageNumber(response: HtmlResponse): Int = response
        .document
        .getElementsByClass("pagination-item")
        .map { extractPageNumber(it) }
        .max() ?: 1

    private fun extractPageNumber(pagination: Element): Int = pagination
        .text()
        .toInt()

    override fun extractHref(ad: Element): String = ad
        .getElementsByClass("advert-link")
        .firstOrNull()
        ?.attr("href")
        .orEmpty()

    override fun extractTitle(ad: Element): String = ad
        .getElementsByClass("result-title")
        .firstOrNull()
        ?.text()
        .orEmpty()

    override fun extractCompany(ad: Element): String = ad
        .getElementsByClass("company-name")
        .asSequence()
        .map(Element::text)
        .firstOrNull()
        .orEmpty()

    override fun extractLocation(ad: Element): String = ad
        .getElementsByClass("job-info")
        .firstOrNull()
        ?.getElementsByClass("location")
        ?.firstOrNull()
        ?.text()
        .orEmpty()


}
