package epomis.mobile.provider.generic.recognizer

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter

import io.reactivex.Flowable

import org.jsoup.nodes.Document

interface AdRecognizer
{
    fun recognize(document: Document, adFilter: AdFilter): Flowable<Ad>
}
