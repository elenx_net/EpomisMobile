package epomis.mobile.provider.pl.jobviously

import epomis.mobile.model.Ad
import epomis.mobile.provider.JsonAdProvider
import epomis.service.connection6.request.ConnectionRequest
import epomis.service.connection6.response.JsonResponse
import java.net.URL

private const val GET_AS_JSON_BASE_URL = "https://jobviously.pl/api/offers?count=all&page="
private const val BASE_JSON_URL = "https://jobviously.pl/api/offers/"
private const val PROVIDER_NAME = "jobviously.pl"
private const val KEYWORD_COOKIE = "keyword"
private const val LOCATION_COOKIE = "location"
private const val DASH = "/"

internal class JobviouslyProvider : JsonAdProvider<JobviouslyJsonResponse>
{
    override fun urlFor(keyword: String, location: String, pageNumber: Int) =
            URL("$GET_AS_JSON_BASE_URL${pageNumber - 1}")

    override fun extractAdsFrom(response: JsonResponse<JobviouslyJsonResponse>) = response
            .json
            .offers
            .asSequence()
            .map(this::extractAd)
            .filter { it.location.contains(response.cookies.getOrDefault(LOCATION_COOKIE, ""), ignoreCase = true) }
            .filter { it.title.contains(response.cookies.getOrDefault(KEYWORD_COOKIE, ""), ignoreCase = true) }
            .toSet()

    override fun furthestKnownPageNumber(response: JsonResponse<JobviouslyJsonResponse>) =
            Math.ceil(response.json.totalCount / response.json.countPerPage.toDouble()).toInt()

    override fun requestFor(keyword: String, location: String, pageNumber: Int) = ConnectionRequest(
                url = urlFor(keyword, location, pageNumber).toString(),
                headers = headersFor(keyword, location, pageNumber),
                data = dataEntriesFor(keyword, location, pageNumber),
                cookies = prepareRequestCookies(keyword, location)
        )

    private fun prepareRequestCookies(keyword: String, location: String) = mapOf(
            KEYWORD_COOKIE to keyword,
            LOCATION_COOKIE to location
    )

    override fun getJsonClass(): Class<JobviouslyJsonResponse> = JobviouslyJsonResponse::class.java

    private fun extractAd(jsonAd: JobviouslyJsonAd) = Ad(
            href = "$BASE_JSON_URL${jsonAd.id}$DASH${jsonAd.slug}",
            title = jsonAd.position,
            company = jsonAd.name,
            location = jsonAd.address.city,
            provider = PROVIDER_NAME
    )
}
