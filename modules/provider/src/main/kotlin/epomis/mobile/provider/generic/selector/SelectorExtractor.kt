package epomis.mobile.provider.generic.selector

import epomis.mobile.model.AdFilter
import org.jsoup.nodes.Element

interface SelectorExtractor
{
    fun findMostFrequentCssSelector(body: Element, adFilter: AdFilter): String?
}