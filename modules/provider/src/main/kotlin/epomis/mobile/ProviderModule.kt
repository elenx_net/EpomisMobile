package epomis.mobile

import epomis.connectionModule
import epomis.mobile.manager.ProviderManager
import epomis.mobile.manager.RxProviderManager
import epomis.mobile.provider.GenericAdProvider
import epomis.mobile.provider.com.linkedin.feed.LinkedinFeedProvider
import epomis.mobile.provider.generic.genericProviderModule
import epomis.mobile.provider.pl.bulldogjob.BulldogJobProvider
import epomis.mobile.provider.pl.goldenline.GoldenLineProvider
import epomis.mobile.provider.pl.jobs.JobsProvider
import epomis.mobile.provider.pl.praca.PracaPlProvider
import epomis.utilsModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.inSet
import org.kodein.di.generic.instance
import org.kodein.di.generic.setBinding
import org.kodein.di.generic.singleton

val providerModule = Kodein.Module("provider")
{
    /**MODULES**/
    importOnce(utilsModule, allowOverride = true)
    importOnce(connectionModule, allowOverride = true)

    /**PROVIDERS**/
    bind() from setBinding<GenericAdProvider<*>>()


    //bind<GenericAdProvider<*>>().inSet() with singleton { PracaPlProvider() }
    //bind<GenericAdProvider<*>>().inSet() with singleton { JobsProvider() }
    bind<GenericAdProvider<*>>().inSet() with singleton { GoldenLineProvider() }

//    these providers applicants misbehave, may require minor fixes
//    bind<GenericAdProvider<*>>().inSet() with singleton { PracujPlProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { JobviouslyProvider() }

//    these providers applicants throw an exception during application
//    bind<GenericAdProvider<*>>().inSet() with singleton { OlxProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { JoberProvider() }

//    these providers don't have applicants
//    bind<GenericAdProvider<*>>().inSet() with singleton { LepszaPracaPlProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { JoobleProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { JobrapidoProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { IndeedProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { AmundioProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { GoWorkProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { GazetaPracaProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { AdzunaProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { PracaMoneyPlProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { EgospodarkaProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { PracaGratkaPlProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { InfoPracaProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { FunctionalWorksProvider() }
    bind<GenericAdProvider<*>>().inSet() with singleton { BulldogJobProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { GumtreeProvider(instance()) }
//    bind<GenericAdProvider<*>>().inSet() with singleton { StackOverflowProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { HaysProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { NoFluffJobsProvider() }

//
//    these providers don't work at all
//    bind<GenericAdProvider<*>>().inSet() with singleton { LinkedInProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { SkillHuntProviderJson() }

//    bind<GenericAdProvider<*>>().inSet() with singleton { WhoIsHiringProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { ForProgrammersProvider() }
//
//    these providers don't respect search parameters (location/keyword)
//    bind<GenericAdProvider<*>>().inSet() with singleton { UGetProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { JustJoinProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { MonsterProvider() }
//
//    these providers don't provide any ads
//    bind<GenericAdProvider<*>>().inSet() with singleton { PracaInteriaProvider() }
//    bind<GenericAdProvider<*>>().inSet() with singleton { GraftonProvider() }

    /** MANAGER **/
    bind<ProviderManager>() with singleton { RxProviderManager(instance(), instance()) }

    /**OTHER**/
    import(genericProviderModule)
    bind<LinkedinFeedProvider>() with singleton { LinkedinFeedProvider() }
}
