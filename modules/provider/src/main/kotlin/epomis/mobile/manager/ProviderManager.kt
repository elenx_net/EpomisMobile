package epomis.mobile.manager

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter
import org.reactivestreams.Publisher

interface ProviderManager
{
    /**
     * Returns an iterable of publishers where each publisher represents a stream of ads from a separate source (provider)
     *
     * @param adFilter used to specify the search params
     */
    fun acquireAds(adFilter: AdFilter): Iterable<Publisher<Ad>>
}