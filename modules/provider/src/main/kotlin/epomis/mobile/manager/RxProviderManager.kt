package epomis.mobile.manager

import epomis.mobile.model.Ad
import epomis.mobile.model.AdFilter
import epomis.mobile.provider.GenericAdProvider
import epomis.service.connection6.ConnectionService6
import io.reactivex.Flowable

private const val FROM_PAGE = 1
private const val AMOUNT_OF_PAGES_TO_CRAWL = 5

class RxProviderManager(
    private val providers: Set<GenericAdProvider<*>>,
    private val connectionService: ConnectionService6
) : ProviderManager
{
    @Suppress("UNCHECKED_CAST")
    override fun acquireAds(adFilter: AdFilter): Iterable<Flowable<Ad>> = providers
        .map { it as GenericAdProvider<Any?> }
        .map { acquireAdsFromProvider(adFilter, it) }

    private fun acquireAdsFromProvider(adFilter: AdFilter, provider: GenericAdProvider<Any?>): Flowable<Ad> = Flowable
        .range(FROM_PAGE, AMOUNT_OF_PAGES_TO_CRAWL)
        .map { prepareSendArgumentsFor(adFilter, it, provider) }
        .flatMapSingle { (request, httpMethod) -> provider.send(request, connectionService, httpMethod) }
        .flatMapIterable { provider.extractAdsFrom(it) }

    private fun prepareSendArgumentsFor(adFilter: AdFilter, pageNumber: Int, provider: GenericAdProvider<Any?>) = Pair(
        connectionRequestFor(adFilter, pageNumber, provider),
        httpMethodFor(adFilter, pageNumber, provider)
    )

    private fun httpMethodFor(adFilter: AdFilter, pageNumber: Int, provider: GenericAdProvider<Any?>) = provider
        .methodFor(adFilter.keyword, adFilter.location, pageNumber)

    private fun connectionRequestFor(adFilter: AdFilter, pageNumber: Int, provider: GenericAdProvider<Any?>) = provider
        .requestFor(adFilter.keyword, adFilter.location, pageNumber)
}