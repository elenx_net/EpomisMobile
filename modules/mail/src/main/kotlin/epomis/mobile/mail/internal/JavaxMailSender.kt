package epomis.mobile.mail.internal

import epomis.mobile.mail.Attachment
import epomis.mobile.mail.MailSender
import java.util.Date
import javax.activation.DataHandler
import javax.mail.Message
import javax.mail.Multipart
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import javax.mail.util.ByteArrayDataSource

internal class JavaxMailSender(private val session: Session) : MailSender
{
    override fun send(to: String, subject: String, message: String, attachment: Attachment)
    {
        val mimeMessage: Message = prepareMessage(to, subject, message, attachment)
        Transport.send(mimeMessage)
    }

    private fun prepareMessage(to: String, subject: String, message: String, attachment: Attachment): MimeMessage
    {
        val mailMessage = MimeMessage(session)

        mailMessage.setRecipient(Message.RecipientType.TO, InternetAddress(to))
        mailMessage.subject = subject
        mailMessage.sentDate = Date()

        val multipart: Multipart = MimeMultipart()
        multipart.addBodyPart(createMessageBodyPart(message))

        if (isAttachmentCorrect(attachment))
        {
            multipart.addBodyPart(createAttachmentBodyPart(attachment))
        }

        mailMessage.setContent(multipart)
        return mailMessage
    }

    private fun createMessageBodyPart(message: String): MimeBodyPart
    {
        val messageBodyPart = MimeBodyPart()
        messageBodyPart.setContent(message, "text/html")
        return messageBodyPart
    }

    private fun isAttachmentCorrect(attachment: Attachment) =
        !attachment.filename.isBlank() && !attachment.contentType.isBlank()

    private fun createAttachmentBodyPart(attachment: Attachment): MimeBodyPart
    {
        val attachmentBodyPart = MimeBodyPart()
        attachmentBodyPart.disposition = "attachment"
        attachmentBodyPart.fileName = attachment.filename

        val ds = ByteArrayDataSource(attachment.inputStream, attachment.contentType)
        attachmentBodyPart.dataHandler = DataHandler(ds)
        return attachmentBodyPart
    }
}