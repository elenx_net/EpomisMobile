package epomis.mobile.mail.internal

import javax.mail.Authenticator
import javax.mail.PasswordAuthentication

internal class UsernamePasswordAuthenticator(private val username: String, private val password: String) : Authenticator()
{
    override fun getPasswordAuthentication() = PasswordAuthentication(username, password)
}