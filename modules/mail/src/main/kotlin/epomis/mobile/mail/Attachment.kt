package epomis.mobile.mail

import java.io.InputStream

data class Attachment(
    val filename: String,
    val contentType: String,
    val inputStream: InputStream
)