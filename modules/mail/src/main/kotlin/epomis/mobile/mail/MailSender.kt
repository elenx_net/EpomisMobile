package epomis.mobile.mail

interface MailSender
{
    fun send(to: String, subject: String, message: String, attachment: Attachment)
}