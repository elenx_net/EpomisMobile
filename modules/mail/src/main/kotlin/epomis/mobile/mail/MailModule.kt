package epomis.mobile.mail

import epomis.mobile.mail.internal.MailConfiguration
import epomis.mobile.mail.internal.JavaxMailSender
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

val mailModule = Kodein.Module("mail")
{
    bind<MailSender>() with singleton { JavaxMailSender(MailConfiguration().session()) }
}