package epomis.mobile.mail.internal

import java.util.Properties
import javax.mail.Session

private const val CONFIGURATION_FILE_NAME = "epomis.mobile.mail.configuration/mail-configuration.yaml"

internal class MailConfiguration
{
    fun session(): Session
    {
        val properties = loadMailProperties()
        return Session.getInstance(
            properties,
            UsernamePasswordAuthenticator(
                properties.getProperty("mail.username"),
                properties.getProperty("mail.password")
            )
        )
    }

    private fun loadMailProperties(): Properties
    {
        val properties = Properties()
        properties.load(
            MailConfiguration::class.java.classLoader.getResourceAsStream(
                CONFIGURATION_FILE_NAME
            )
        )
        return properties
    }
}