plugins {
    kotlin
    kodein
    test
    rx
}

dependencies {
    api("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.5")
    api("org.apache.commons:commons-lang3:3.7")
    api("commons-validator:commons-validator:1.6")
}