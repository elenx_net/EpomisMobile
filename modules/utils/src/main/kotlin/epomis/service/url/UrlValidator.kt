package epomis.service.url

class UrlValidator
{
    fun makeUrl(baseUrl: String, href: String): String
    {
        if (href.startsWith("http"))
            return href

        val baseUrl = baseUrl
            .replace("https://", "")
            .replace("http://", "")
        val href = if (href.startsWith("/")) href.replaceFirst("/", "") else href

        return "https://$baseUrl/$href"
    }
}
