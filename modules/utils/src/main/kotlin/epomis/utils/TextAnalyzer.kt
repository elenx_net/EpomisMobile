package epomis.utils

import org.apache.commons.lang3.StringUtils

object TextAnalyzer
{
    private const val MIN_SIMILARITY = .39f

    @JvmStatic
    fun stringSimilarity(longer: String, shorter: String): Float =
        when
        {
            longer.length < shorter.length -> stringSimilarity(shorter, longer)
            longer.isEmpty() -> 1.0f
            else -> 1 - StringUtils.getLevenshteinDistance(longer, shorter) / longer.length.toFloat()
        }

    @JvmStatic
    fun isSimilarEnough(s1: String, s2: String, minSimilarity: Float): Boolean = stringSimilarity(s1, s2) > minSimilarity

    @JvmStatic
    fun isSimilarEnough(similarity: Float): Boolean = similarity > MIN_SIMILARITY
}
