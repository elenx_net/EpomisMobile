package epomis.utils

import hu.akarnokd.rxjava2.operators.FlowableTransformers
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.reactivestreams.Publisher

inline fun <E> Flowable<E>.expand(crossinline expander: (E) -> Flowable<E>) =
    compose(FlowableTransformers.expand { expander(it) })

fun <T> Publisher<T>.asSingle(): Single<T> = Single
    .fromPublisher(this)

fun <T> Publisher<T>.asFlowable(): Flowable<T> = Flowable
    .fromPublisher(this)

fun <T> T.toMaybe(): Maybe<T> = Maybe
    .just(this)

fun <T> T.toSingle(): Single<T> = Single
    .just(this)