package epomis.utils

/**
 * Adds the specified element to the list.
 *
 * If the element already exists, then it is being replaced by the new one.
 *
 * @return `true` if the element was replaced, false otherwise.
 */
fun <E> MutableCollection<E>.addOrReplace(item: E) = this
    .remove(item)
    .also { add(item) }

/**
 * Calls the specified function [block] if the collection is not empty.
 */
fun <E> Collection<E>.ifNotEmpty(block: () -> Unit)
{
    if (!this.isEmpty())
    {
        block()
    }
}