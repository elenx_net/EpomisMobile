package epomis.utils

fun String?.nullIfBlank() = this?.ifBlank { null }