package epomis.domainextractor

import org.apache.commons.validator.routines.UrlValidator

import java.net.URL

class DomainExtractor
{
    private val urlValidator: UrlValidator

    init
    {
        val schemes = arrayOf("http", "https")
        urlValidator = UrlValidator(schemes)
    }

    fun extractReverseDomain(url: String): String =
        if (urlValidator.isValid(url))
        {
            URL(url)
                .host
                .split(".")
                .reversed()
                .joinToString(separator = ".")
        }
        else
        {
            throw InvalidURLException("URL is invalid")
        }

}

class InvalidURLException(override var message: String) : Exception(message)
