package epomis

import com.fasterxml.jackson.databind.ObjectMapper
import epomis.configuration.jacksonObjectMapper
import epomis.domainextractor.DomainExtractor
import epomis.service.url.UrlValidator
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val utilsModule = Kodein.Module("utilsModule") {
    /**JACKSON**/
    bind<ObjectMapper>() with instance(jacksonObjectMapper)

    bind<UrlValidator>() with singleton { UrlValidator() }
    bind<DomainExtractor>() with singleton { DomainExtractor() }
}