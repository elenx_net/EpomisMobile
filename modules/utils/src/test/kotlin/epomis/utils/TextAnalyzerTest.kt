package epomis.utils

import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

class TextAnalyzerTest : Spek({
    data class SimilarityTest(val s1: String, val s2: String, val expectedSimilarity: Float)
    data class IsSimilarEnoughTest1(val s1: String, val s2: String, val minSimilarity: Float, val isSimilarEnough: Boolean)
    data class IsSimilarEnoughTest2(val similarity: Float, val isSimilarEnough: Boolean)

    listOf(
        SimilarityTest("aaaa", "aaaa", 1.0f),
        SimilarityTest("aaaa", "bbbb", 0.0f),
        SimilarityTest("aaaa", "aabb", 0.5f),
        SimilarityTest("", "aaaa", 0.0f)
    ).forEach { test ->
        describe("similarity test $test") {
            it("should return expected similarity") {
                assertEquals(test.expectedSimilarity, TextAnalyzer.stringSimilarity(test.s1, test.s2))
            }
        }
    }

    listOf(
        IsSimilarEnoughTest1("aaaa", "aaaa", 0.99f, true),
        IsSimilarEnoughTest1("aaaa", "bbbb", 0.00f, false),
        IsSimilarEnoughTest1("aaaa", "aabb", 0.49f, true)
    ).forEach { test ->
        describe("isSimilarEnough test 1 $test") {
            it("should check minimal similarity") {
                assertEquals(test.isSimilarEnough, TextAnalyzer.isSimilarEnough(test.s1, test.s2, test.minSimilarity))
            }
        }
    }

    listOf(
        IsSimilarEnoughTest2(0.4f, true),
        IsSimilarEnoughTest2(0.39f, false)
    ).forEach { test ->
        describe("isSimilarEnough test 2 $test") {
            it("should check minimal similarity") {
                assertEquals(test.isSimilarEnough, TextAnalyzer.isSimilarEnough(test.similarity))
            }
        }
    }
})