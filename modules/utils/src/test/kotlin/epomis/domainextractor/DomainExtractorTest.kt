package epomis.domainextractor

import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class DomainExtractorTest : Spek({

    val subject = DomainExtractor()

    describe("test domain extractor") {

        it("should return reverse domain name for valid url") {
            val validUrl = "http://example.domain.org/"
            val validReversedUrl = "org.domain.example"

            assertEquals(subject.extractReverseDomain(validUrl), validReversedUrl)
        }
        it("should throw exception when url is invalid") {
            val invalidUrls = setOf("htp://example.domain.org/", "", "htp://example.do/", "htpps://example.domainorg/")

            invalidUrls.forEach { url -> assertFailsWith<InvalidURLException> { subject.extractReverseDomain(url) } }
        }
    }
})
