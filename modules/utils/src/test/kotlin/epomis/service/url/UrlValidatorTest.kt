package epomis.service.url

import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

data class Test(val baseUrl: String, val href: String, val properFormat: String)

class UrlValidatorTest : Spek({

    listOf(
        Test("1", "/2", "https://1/2"),
        Test("http://1", "2", "https://1/2"),
        Test("https://1", "2", "https://1/2"),
        Test("1", "http://2", "http://2"),
        Test("1", "https://2", "https://2"),
        Test("http://1", "http://2", "http://2"),
        Test("https://1", "https://2", "https://2"),
        Test("http://1", "https://2", "https://2"),
        Test("https://1", "http://2", "http://2")
    ).forEach { test ->
        describe("an UrlValidator $test") {
            it("creates valid uri by concatenation of prefix, ${test.baseUrl} and ${test.href}")
            {
                assertEquals(test.properFormat, UrlValidator().makeUrl(test.baseUrl, test.href))
            }
            }
        }
})