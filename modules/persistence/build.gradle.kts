plugins {
    android
    kodein
    kotlin("kapt")
}

dependencies {
    implementation(project(":modules:model"))
    implementation(project(":modules:utils"))

    kapt("androidx.room:room-runtime:2.2.4")
    kapt("androidx.room:room-compiler:2.2.4")
    implementation("androidx.room:room-ktx:2.2.4")
    implementation("androidx.room:room-rxjava2:2.2.4")
}