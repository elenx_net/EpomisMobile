package epomis.mobile.persistence.ad.internal

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import epomis.mobile.model.ApplicationResult

@Entity
internal data class AdEntity(
    val href: String,
    val title: String,
    val company: String,
    val location: String,
    val provider: String,
    @Embedded val latestApplication: ApplicationResult?,

    @PrimaryKey
    val id: Long? = null
)