package epomis.mobile.persistence.ad.internal

import androidx.room.Entity

@Entity(primaryKeys = ["jobId", "adId"])
internal data class JobAdCrossRef(
    val jobId: Long,
    val adId: Long
)