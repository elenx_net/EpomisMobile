package epomis.mobile.persistence.ad.internal

import epomis.mobile.model.Ad

internal class AdMapper
{
    fun fromAd(ad: Ad): AdEntity =
        AdEntity(
            href = ad.href,
            title = ad.title,
            company = ad.company,
            location = ad.location,
            provider = ad.provider,
            id = ad.id,
            latestApplication = ad.latestApplication
        )

    fun toAd(adEntity: AdEntity): Ad = Ad(
        href = adEntity.href,
        title = adEntity.title,
        company = adEntity.company,
        location = adEntity.location,
        provider = adEntity.provider,
        id = adEntity.id,
        latestApplication = adEntity.latestApplication
    )
}