package epomis.mobile.persistence.ad

import epomis.mobile.model.Ad
import epomis.mobile.model.ApplicationResult
import epomis.mobile.model.Job
import io.reactivex.Flowable

interface AdStorageFacade
{
    fun observeAllByJobId(jobId: Long): Flowable<List<Ad>>
    fun observeSize(): Flowable<Int>

    fun insert(ad: Ad, job: Job): Ad
    fun findAllByJob(job: Job): List<Ad>
    fun updateApplicationResult(ad: Ad, applicationResult: ApplicationResult): Ad
    fun delete(jobIds: List<Long>)
}