package epomis.mobile.persistence.userdata.internal

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
internal interface UserDataDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userDataEntity: UserDataEntity): Completable

    @Query("SELECT * FROM UserDataEntity")
    fun findUserData(): UserDataEntity

    @Query("SELECT * FROM UserDataEntity")
    fun maybeFindUserData(): Maybe<UserDataEntity>

    @Query("SELECT EXISTS(SELECT * FROM UserDataEntity)")
    fun isUserDataPresent(): Single<Boolean>
}