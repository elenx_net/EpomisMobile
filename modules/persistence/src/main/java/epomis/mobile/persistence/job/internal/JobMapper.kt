package epomis.mobile.persistence.job.internal

import epomis.mobile.model.Job

internal class JobMapper
{
    fun fromJob(job: Job): JobEntity = JobEntity(
        type = job.type,
        status = job.status,
        adFilter = job.adFilter,
        started = job.started,
        finished = job.finished,
        id = job.id
    )

    fun toJob(jobEntity: JobEntity): Job = Job(
        type = jobEntity.type,
        status = jobEntity.status,
        adFilter = jobEntity.adFilter,
        started = jobEntity.started,
        finished = jobEntity.finished,
        id = jobEntity.id
    )

    fun toJobs(jobEntities: List<JobEntity>): List<Job> = jobEntities
        .map { toJob(it) }

}
