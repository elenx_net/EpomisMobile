package epomis.mobile.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import epomis.mobile.persistence.ad.internal.AdDao
import epomis.mobile.persistence.ad.internal.AdEntity
import epomis.mobile.persistence.ad.internal.JobAdCrossRef
import epomis.mobile.persistence.job.internal.JobDao
import epomis.mobile.persistence.job.internal.JobEntity
import epomis.mobile.persistence.userdata.internal.UserDataDao
import epomis.mobile.persistence.userdata.internal.UserDataEntity

@TypeConverters(Converters::class)
@Database(
    entities = [AdEntity::class, JobEntity::class, JobAdCrossRef::class, UserDataEntity::class],
    version = 1
)
internal abstract class EpomisMobileDatabase : RoomDatabase()
{
    abstract fun jobDao(): JobDao
    abstract fun adDao(): AdDao
    abstract fun userDataDao(): UserDataDao
}