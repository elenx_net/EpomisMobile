package epomis.mobile.persistence.userdata.internal

import android.util.Base64
import epomis.mobile.model.UserData

internal class UserDataMapper
{
    fun toUserData(userDataEntity: UserDataEntity): UserData =
        UserData(
            userDataEntity.firstName,
            userDataEntity.lastName,
            userDataEntity.email,
            userDataEntity.resumeFilename,
            decodeResumeContent(userDataEntity.base64EncodedResumeContent)
        )

    private fun decodeResumeContent(cvStringData: String) = Base64
        .decode(cvStringData, Base64.NO_WRAP)

    fun encodeResumeContent(content: ByteArray): String = Base64.encodeToString(content, Base64.NO_WRAP)
}