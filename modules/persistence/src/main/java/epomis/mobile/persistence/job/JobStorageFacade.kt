package epomis.mobile.persistence.job

import epomis.mobile.model.Job
import epomis.mobile.model.JobStatus
import io.reactivex.Flowable

interface JobStorageFacade
{
    fun observeAll(): Flowable<List<Job>>
    fun updateStatus(job: Job, status: JobStatus): Job
    fun insert(job: Job): Job
    fun delete(jobs: List<Job>)
}