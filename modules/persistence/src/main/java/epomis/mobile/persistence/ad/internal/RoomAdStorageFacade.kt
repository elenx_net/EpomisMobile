package epomis.mobile.persistence.ad.internal

import epomis.mobile.model.Ad
import epomis.mobile.model.ApplicationResult
import epomis.mobile.model.Job
import epomis.mobile.persistence.ad.AdStorageFacade
import epomis.mobile.persistence.job.internal.JobMapper
import io.reactivex.Flowable

internal class RoomAdStorageFacade(
    private val adDao: AdDao,
    private val adMapper: AdMapper,
    private val jobMapper: JobMapper
) : AdStorageFacade
{
    override fun observeAllByJobId(jobId: Long): Flowable<List<Ad>> = adDao
        .observeAllByJobId(jobId)
        .map { mapToAds(it) }

    private fun mapToAds(it: List<AdEntity>) = it
        .map { adMapper.toAd(it) }

    override fun observeSize(): Flowable<Int> = adDao.observeSize()

    override fun insert(ad: Ad, job: Job): Ad = adMapper
        .toAd(adDao.insert(adMapper.fromAd(ad), jobMapper.fromJob(job)))

    override fun findAllByJob(job: Job): List<Ad> = adDao
        .findAllByJobId(job.id!!)
        .map { adMapper.toAd(it) }

    override fun updateApplicationResult(ad: Ad, applicationResult: ApplicationResult): Ad = ad
        .copy(latestApplication = applicationResult)
        .also { adDao.update(adMapper.fromAd(it)) }

    override fun delete(jobIds: List<Long>) = adDao.delete(jobIds)
        .also { adDao.deleteRelations(jobIds) }

}