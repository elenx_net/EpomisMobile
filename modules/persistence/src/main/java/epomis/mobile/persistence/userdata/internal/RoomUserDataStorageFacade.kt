package epomis.mobile.persistence.userdata.internal

import epomis.mobile.model.UserData
import epomis.mobile.persistence.userdata.UserDataFacade
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

internal class RoomUserDataStorageFacade(
    private val userDataDao: UserDataDao,
    private val userDataMapper: UserDataMapper
) : UserDataFacade
{
    override fun findUserData(): UserData = userDataDao
        .findUserData()
        .let { userDataMapper.toUserData(it) }

    override fun isUserDataPresent(): Single<Boolean> = userDataDao
        .isUserDataPresent()

    override fun maybeFindUserData(): Maybe<UserData> = userDataDao
        .maybeFindUserData()
        .map { userDataMapper.toUserData(it) }

    override fun saveUserData(
        firstName: String,
        lastName: String,
        email: String,
        resumeFilename: String,
        resumeContent: ByteArray
    ): Completable =
        userDataDao.insert(
            UserDataEntity(
                firstName,
                lastName,
                email,
                resumeFilename,
                userDataMapper.encodeResumeContent(resumeContent)
            )
        )
}