package epomis.mobile.persistence.ad.internal

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import epomis.mobile.persistence.job.internal.JobEntity
import io.reactivex.Flowable

@Dao
internal interface AdDao
{
    @Query(
        """
        SELECT AdAlias.* FROM AdEntity AdAlias
        INNER JOIN JobAdCrossRef CrossRefAlias ON AdAlias.id = CrossRefAlias.adId
        WHERE jobId = :jobId
        """
    )
    @Transaction
    fun observeAllByJobId(jobId: Long): Flowable<List<AdEntity>>

    @Query("SELECT COUNT(*) FROM AdEntity")
    fun observeSize(): Flowable<Int>

    @Query("SELECT * FROM AdEntity WHERE id = :id")
    fun findById(id: Long): AdEntity

    @Query(
        """
        SELECT AdAlias.* FROM AdEntity AdAlias
        INNER JOIN JobAdCrossRef CrossRefAlias ON AdAlias.id = CrossRefAlias.adId
        WHERE jobId = :jobId
        """
    )
    fun findAllByJobId(jobId: Long): List<AdEntity>

    @Transaction
    fun insert(ad: AdEntity, job: JobEntity): AdEntity = insert(ad)
        .also { insertRelation(JobAdCrossRef(job.id!!, it)) }
        .let { findById(it) }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ad: AdEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRelation(jobAdCrossRef: JobAdCrossRef)

    @Update
    fun update(ad: AdEntity)

    @Query(
        """DELETE FROM AdEntity WHERE id IN
        (SELECT adId FROM JobAdCrossRef WHERE jobId IN (:jobIds))"""
    )
    fun delete(jobIds: List<Long>)

    @Query("DELETE FROM JobAdCrossRef WHERE jobId IN (:jobIds)")
    fun deleteRelations(jobIds: List<Long>)
}