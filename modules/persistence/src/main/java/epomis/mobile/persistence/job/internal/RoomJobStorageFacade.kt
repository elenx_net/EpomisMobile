package epomis.mobile.persistence.job.internal

import epomis.mobile.model.Job
import epomis.mobile.model.JobStatus
import epomis.mobile.persistence.job.JobStorageFacade
import io.reactivex.Flowable

internal class RoomJobStorageFacade(
    private val jobDao: JobDao,
    private val jobMapper: JobMapper
) : JobStorageFacade
{
    override fun observeAll(): Flowable<List<Job>> = jobDao
        .observeAll()
        .map { jobMapper.toJobs(it) }

    override fun updateStatus(job: Job, status: JobStatus): Job = job
        .copy(status = status)
        .also { jobDao.update(jobMapper.fromJob(it)) }

    override fun insert(job: Job): Job = jobMapper
        .fromJob(job)
        .let { jobDao.insert(it) }
        .let { jobDao.findById(it) }
        .let { jobMapper.toJob(it) }

    override fun delete(jobs: List<Job>) = jobDao.delete(jobs.map { job -> jobMapper.fromJob(job) })
}