package epomis.mobile.persistence

import android.content.Context
import androidx.room.Room
import epomis.mobile.persistence.ad.AdStorageFacade
import epomis.mobile.persistence.ad.internal.AdMapper
import epomis.mobile.persistence.ad.internal.RoomAdStorageFacade
import epomis.mobile.persistence.job.JobStorageFacade
import epomis.mobile.persistence.job.internal.JobMapper
import epomis.mobile.persistence.job.internal.RoomJobStorageFacade
import epomis.mobile.persistence.userdata.UserDataFacade
import epomis.mobile.persistence.userdata.internal.RoomUserDataStorageFacade
import epomis.mobile.persistence.userdata.internal.UserDataMapper
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton


private const val DATABASE_NAME = "EM"

fun persistenceModule(applicationContext: Context) = Kodein.Module("persistence")
{
    val database = Room.databaseBuilder(
        applicationContext,
        EpomisMobileDatabase::class.java,
        DATABASE_NAME
    ).build()

    val adMapper = AdMapper()
    val jobMapper = JobMapper()

    bind<UserDataMapper>() with singleton { UserDataMapper() }

    bind<AdStorageFacade>() with singleton {
        RoomAdStorageFacade(
            database.adDao(),
            adMapper,
            jobMapper
        )
    }
    bind<JobStorageFacade>() with singleton {
        RoomJobStorageFacade(database.jobDao(), jobMapper)
    }
    bind<UserDataFacade>() with singleton {
        RoomUserDataStorageFacade(database.userDataDao(), instance())
    }
}