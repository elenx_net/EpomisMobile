package epomis.mobile.persistence.job.internal


import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Flowable

@Dao
internal interface JobDao
{
    @Query("SELECT * FROM JobEntity ORDER BY id DESC")
    fun observeAll(): Flowable<List<JobEntity>>

    @Query("SELECT * FROM JobEntity WHERE id = :jobId")
    fun findById(jobId: Long): JobEntity

    @Update
    fun update(job: JobEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(job: JobEntity): Long

    @Delete
    fun delete(jobs: List<JobEntity>)
}