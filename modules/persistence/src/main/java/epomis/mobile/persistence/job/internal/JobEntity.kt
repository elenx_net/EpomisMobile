package epomis.mobile.persistence.job.internal

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import epomis.mobile.model.AdFilter
import epomis.mobile.model.JobStatus
import epomis.mobile.model.JobType
import org.joda.time.DateTime

@Entity
internal data class JobEntity(
    val type: JobType,
    val status: JobStatus,
    @Embedded val adFilter: AdFilter,
    val started: DateTime,
    val finished: DateTime,

    @PrimaryKey
    val id: Long? = null
)