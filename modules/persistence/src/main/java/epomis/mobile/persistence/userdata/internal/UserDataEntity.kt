package epomis.mobile.persistence.userdata.internal

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
internal data class UserDataEntity(
    val firstName: String,
    val lastName: String,
    val email: String,
    val resumeFilename: String,
    val base64EncodedResumeContent: String
)
{
    @PrimaryKey
    var id: Long = 1
}