package epomis.mobile.persistence

import androidx.room.TypeConverter
import epomis.mobile.model.JobStatus
import epomis.mobile.model.JobType
import org.joda.time.DateTime

internal class Converters
{
    @TypeConverter
    fun fromTimestamp(value: Long?): DateTime? = value?.let { DateTime(it) }

    @TypeConverter
    fun dateToTimestamp(date: DateTime?): Long? = date?.millis

    @TypeConverter
    fun getStatus(ordinal: Int): JobStatus = JobStatus.values()[ordinal]

    @TypeConverter
    fun getStatusInt(status: JobStatus): Int = JobStatus.values().indexOf(status)

    @TypeConverter
    fun getType(ordinal: Int): JobType = JobType.values()[ordinal]

    @TypeConverter
    fun getTypeInt(status: JobType): Int = JobType.values().indexOf(status)
}