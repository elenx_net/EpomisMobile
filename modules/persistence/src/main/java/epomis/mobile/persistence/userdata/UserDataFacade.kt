package epomis.mobile.persistence.userdata

import epomis.mobile.model.UserData
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface UserDataFacade
{
    fun findUserData(): UserData
    fun maybeFindUserData(): Maybe<UserData>
    fun isUserDataPresent(): Single<Boolean>
    fun saveUserData(firstName: String, lastName: String, email: String, resumeFilename: String, resumeContent: ByteArray): Completable
}