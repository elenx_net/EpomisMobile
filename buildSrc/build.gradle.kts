plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    jcenter()
    google()
}

val kotlin = "1.3.72"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin")
    implementation("com.android.tools.build:gradle:4.1.1")
    implementation("android.arch.navigation:navigation-safe-args-gradle-plugin:1.0.0")
    implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.8.0.1969")
}
