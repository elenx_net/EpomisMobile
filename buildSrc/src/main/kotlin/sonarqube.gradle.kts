plugins {
    id("org.sonarqube")
}

sonarqube {
    properties {
        property("sonar.projectKey", "elenx_net_EpomisMobile")
        property("sonar.organization", "elenx")
    }
}
