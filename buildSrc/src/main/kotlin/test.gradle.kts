plugins {
    `java-library`
}

val spek = "2.0.10"

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.spekframework.spek2:spek-dsl-jvm:$spek")
    testImplementation("org.mockito:mockito-core:2.23.4")
    testImplementation("org.spockframework:spock-core:2.0-M3-groovy-3.0")

    testRuntimeOnly("org.spekframework.spek2:spek-runner-junit5:$spek")
    testRuntimeOnly("org.jetbrains.kotlin:kotlin-reflect")
}

tasks.withType<Test> {
    useJUnitPlatform {
        includeEngines("spek2")
    }
}
