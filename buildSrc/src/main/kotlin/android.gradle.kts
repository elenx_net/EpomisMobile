project.name.apply {
    plugins {
        if (this@apply == "app")
        {
            id("com.android.application")
        } else
        {
            id("com.android.library")
        }
        id("org.jetbrains.kotlin.android")
        id("org.jetbrains.kotlin.android.extensions")
        id("androidx.navigation.safeargs.kotlin")
    }
}

apply(from = "${rootDir.path}/buildSrc/src/main/kotlin/android-config.gradle")

configurations.all {
    exclude(module = "httpclient")
    exclude(module = "xpp3")
}

val implementation by configurations

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}