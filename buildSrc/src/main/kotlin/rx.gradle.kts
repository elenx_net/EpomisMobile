val implementation by configurations

dependencies {
    implementation("io.reactivex.rxjava2:rxjava:2.2.19")
    implementation("io.reactivex.rxjava2:rxkotlin:2.4.0")
    implementation("com.github.akarnokd:rxjava2-extensions:0.20.10")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
}