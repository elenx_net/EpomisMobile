val implementation by configurations

dependencies {
    implementation("org.kodein.di:kodein-di-generic-jvm:6.5.4")
    implementation("org.kodein.di:kodein-di-framework-android-support:6.5.4")
}