Providery
=========

Providery to komponenty odpowiedzialne za dostarczanie ofert z poszczególnych stron, z uwzględnieniem wyszukiwania po słowach kluczowych i lokalizacji oraz numerze strony.

Ogólne informacje
-----------------

- Każdy provider ma swoją klasę w module `provider`, w pakiecie `epomis.mobile.provider`. Przykładowo - providerem praca.pl będzie klasa `epomis.mobile.provider.pl.praca.PracaPlCrawler`.
- Każdy provider implementuje interfejs `AbstractAdProvider<*>`. Przy implementacji nowych providerów nie powinno się rozszerzać bezpośrednio tego interfejsu, tylko odpowiedni subinterfejs: `HtmlAdProvider` lub `JsonAdProvider`
-  Providery nie wykonują requestów, tylko delegują je do ConnectionService i dostają od niego odpowiedź. Provider jest odpowiedzialny za:
    - dostarczenie odpowiedniego url'a (budowanego, w zależności od keyword, location oraz numeru strony, z której chcemy pobrać oferty) - metoda uriFor()
    - extractowanie ofert (do naszego modelu Ad) z response, który dostanie od ConnectionService po requeście na dany url (dostarczony przez metodę wyżej) - metoda extractOffersFrom()
    - dostarczenie numeru ostatniej strony (do której można pobierać oferty) - metoda extractFarthestKnownPageNumber()
- Pierwsze wywołanie `uriFor()` zawsze będzie na pierwszą stronę (`pageNumber = 1`). Po pierwszym requeście metoda `extractFarthestKnownPageNumber()` extractuje największy dostępny numer strony. Na tej podstawie `ProviderManager` może potem wywoływać `uriFor()` z kolejnymi dostępnymi numerami stron.
- Provider powinien więc wyciągać oferty z danej strony i w miarę możliwości uzupełnić wszystkie pola klasy Ad, która reprezentuje ofertę pracy / ogłoszenie. Jeśli danego pola klasy Ad nie da się uzupełnić, zostawiamy je puste.
- Provider może opcjonalnie implementować metodę `send()` która jest odpowiedzialna za wysłanie przygotowanego już requesta (na adres podany przez `uriFor()`) poprzez wywołanie odpowiedniej metody ConnectionService. Przydatne, gdy API providera wymaga na przykład requestu GET, a domyślna implementacja używa POST.
- Mamy do dyspozycji gotowy mechanizm generyczny - Generic Crawlera. Znajduje on najczęściej występujące selektory css, przyjmuje że to są oferty i próbuje wyciągnąć z nich jak najwięcej danych. Nie zawsze takie podejście działa i czasem po prostu trzeba zaimplementować dedykowany mechanizm. Generalnie, jeśli wyniki generica są zadowalające to używamy generica, jest mniej podatny na zmiany w kodzie strony.
- Jeśli pobranie ofert z danej strony wymaga uzupełnienia captchy lub innej walidacji to taki provider będzie uruchomiony później. Piszemy odpowiednią informację w komentarzu taska i dajemy do ON HOLD.
- Jeśli pobranie ofert wymaga przetworzenia JavaScriptu - tak samo dajemy do ON HOLD (na razie)
- Providery mają testy jednostkowe i integracyjne. Jednostkowe weryfikują czy provider został poprawnie zaimplementowany (czy działa ze stroną pobraną w momencie pisania providera) a integracyjne sprawdzają czy działa dla aktualnej wersji strony (symuluje prawdziwe użycie providera i sprawdza czy nie wysypuje się i zwraca jakiekolwiek oferty)

Schemat tworzenia nowego providera
----------------------------------

1. `uriFor()` ma zwracać adres na który zostanie wykonany request GET (providerzy nie obsługują requestów POST).
     W miarę możliwości na tym etapie filtrujemy oferty według `keyword` i `location`.
2. Sprawdzamy czy dla danej strony dostępne jest API z którego można pobrać oferty. Jeśli tak:
   - Tworzymy klasy modelu (np. `MyProviderResponse`) do której Jackson zdeserializuje nam odpowiedź z API.
   - Implementujemy `JsonAdProvider<MyProviderResponse>`.
   - `extractOffersFrom()` extractuje oferty z otrzymanej odpowiedzi. Uzupełniamy co się da w klasie `Ad`.
   - `extractFarthestKnownPageNumber()` - nie dotyczy, możemy zwrócić `1`.
2. Jeśli nie, musimy extractować dane bezpośrednio z dokumentu HTML - implementujemy `HtmlAdProvider`
3. Sprawdzamy czy generic crawler działa dla strony:
   - w konstruktorze deklarujemy `AdRecognizer`a, DI go nam dostarczy podczas tworzenia klasy
   - `extractOffersFrom()` -> `adRecognizer.recognize()`
4. Jeśli nie, implementujemy dedykowany mechanizm
5. `farthestKnownPageNumber()` - extractujemy największy znany numer strony z response
6. Dodajemy naszego providera do DI - `bind<AbstractAdProvider<*>>().inSet() with singleton { MyProvider() }` w `ProviderModule`
7. Dodajemy test case: pobieramy HTML/JSON aktualnej strony, wrzucamy go do folderu `src/test/resources/document`. Następnie w pliku `providersTestData.json` dodajemy nowy wpis z danymi naszego providera i plikiem testowym.
8. Sprawdzamy czy testy integracyjne dla naszego providera działają: Uruchamiamy w konsoli `gradlew :modules:provider:integrationTest --info`. Na wyjściu powinny się pojawić informacje o wadliwych providerach (sekcje `PROVIDER TEST RESULTS`)
Jeśli jednak chcielibyśmy odpalić testy integracyjne z poziomu IDE należy przenieść ProviderIntegrationTest tymczasowo do package test. Pamiętajmy jednak, aby przez pushem przywrócić go do stanu pierwotnego.
8. Pushujemy i czekamy na code review ;)

Przykładowy kod
---------------

```kotlin
// JSON
data class MyProviderOffer(val title: String, val href: String)
data class MyProviderOfferList(val data: List<MyProviderOffer>)
class MyJsonProvider : JsonAdProvider<MyProviderOfferList>
{
    override fun uriFor(keyword: String, location: String, pageNumber: Int) = URI(MY_API_ENDPOINT)
    override fun extractOffersFrom(response: JsonResponse<MyProviderOfferList>) = response.json.data.map { extractOffer(it) }.toSet()
    override fun farthestKnownPageNumber(response: JsonResponse<MyProviderOfferList>) = 1
    override fun getJsonClass() = MyProviderOfferList::class.java
}
// w providerModule
bind<AbstractAdProvider<*>>.inSet() with singleton { MyJsonProvider() }

// HTML (generic)
class MyHtmlGenericProvider(private val adRecognizer: AdRecognizer): HtmlAdProvider
{
    //...
    override fun extractOffersFrom(response: HtmlResponse) = adRecognizer.recognize(response.document, AdFilter("", "")).blockingIterable().toSet()
}
// w providerModule
bind<AbstractAdProvider<*>>.inSet() with singleton { MyHtmlGenericProvider(instance()) }

// HTML (dedykowany)
class MyHtmlProvider : HtmlAdProvider
{
    override fun uriFor(keyword: String, location: String, pageNumber: Int) = URI(MY_HTML_ENDPOINT)
    override fun extractOffersFrom(response: HtmlResponse) = response.document.getElementsByClass("offer").map { extractOffer(it) }.toSet()
    override fun farthestKnownPageNumber(response: HtmlResponse) = response.document.getElementById("pagin").text().toInt()
}
// w providerModule
bind<AbstractAdProvider<*>>.inSet() with singleton { MyJsonProvider() }
```



