WD4A
=========
Jest to moduł, który ma za zadanie sterować przeglądarką za pomocą androidowego WebView [(snippet)](https://elenx.net/blank/EpomisMobile/snippets/8). Jest naszą własną implementacją Selenium z drobnymi ulepszeniami oraz jest w pełni asynchroniczny. 

Rola w projekcie
----
W Epomisie będzie używany przez Applicantów i Providerów, aby umożliwić im sterowanie przeglądarką w celu obsługi skomplikowanych stron

Struktura
========
Moduł jest podzielony na część JavaScriptową i Kotlinową.

`Module` posiada nazwę, nazwę pliku .js, który odpowiada danemu modułowi i listę innych modułów na których polega

`ModuleManager` załadowuje `Module`, które odpowiadają za poszczególne funkcjonalności np. CookiesModule, HeadersModule

`JsExecutionContext` odpowiada za bezpośrednią interakcję z JSem (wywoływanie funkcji, tworzenie funkcji, tworzenie callbacka) w kontekście _aktualnie_ załadowanej strony

`JsExecutionContextProvider` jest odpowiedzialny za zapewnienie, że `JsExecutionContext` na którym operujemy jest zawsze aktualny, poprawny i ma załadowane wszystkie moduły które nas interesują

Diagram części opartej o Selenium
-----
![webdriver](uploads/webdriver.png)
