Applicanci
=========
Applicanci to komponenty odpowiedzialne za zaaplikowanie na ofertę pracy.

Sposób działania
------
Każdy applicant odpowiada za jeden krok w aplikacji. Zbiór applicantów dla danego providera odpowiada za cały proces aplikacji.

Przykład:

1. Pobranie z oferty lokalizacji i przekazanie jej dalej
2. Załadowanie CV i przekazanie dalej identyfikatora oraz lokalizacji
2. Wysłanie formularza wraz z lokalizacją i identyfikatorem cv wziętym z poprzedniego kroku
