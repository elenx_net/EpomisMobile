In order to render the diagrams that are in this folder, either:
* use one of the online tools: https://www.google.com/search?hl=pl&q=plant%20uml%20online
* render the diagrams locally, if you want to do it in IDEA, install a [plugin](https://plugins.jetbrains.com/plugin/7017-plantuml-integration),
you will probably also have to install [Graphviz](https://graphviz.org/) which performs the rendering