plugins {
    sonarqube
}

group = "net.elenx"
version = "1.0-SNAPSHOT"

subprojects {
    repositories {
        mavenCentral()
        jcenter()
        google()
    }
}