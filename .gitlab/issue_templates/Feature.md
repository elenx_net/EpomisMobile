*  **WHAT**  
Zadanie polega na implementacji interface'u Provider dla implementacji XXX.pl  

*  **HOW**  
Każda implementacja w obrębie jednego providera reprezentuje 1 krok (request) .... Należy wzorować się na już istniejących implementacjach Providerów .....  
Trzea pamiętać o tym, żeby poprawnie napisać `isAppopraiteFor` -> tutaj wyjaśniamy co to znaczy `poprawnie` (tak żeby jedno wykluczało drugie)
* **ROADMAP**  
Wykonujący powinien jedynie dodać nowe implementacje interfeace'u Provider, nic więcej nie zmieniamy, np:  
`elenx.net.epomis.provider.xxx.Step1FetchSessionCookie.kt`   
`elenx.net.epomis.provider.xxx.Step2UploadCV.kt`  
*  **PROPOSALS**  
Do parsowania HTML'a można użyć biblioteki `jsoup`
*  **RELATED RESOURCES**   
 ~~ TUTAJ NP LINK DO WIKI OPISUJACY CZYM JEST W OGOLE TEN PROVIDER (biznesowo, górnolotnie)  ~~
*  **DEFINITION OF DONE**  
Provider poprawnie ropoznaje stronę która obsługuje oraz wyciąganie z niej oferty pracy
* **STEPS**  
* [ ]  Zidentyfikowanie ilu conajmniej kroków (requestow / implementacji) wymaga przejście całego flow
* [ ] Implementacja poszczególnych kroków  

###### ***PROJECT'S WIKI***  
https://elenx.net/blank/EpomisMobile/wikis/home
  
  *Opis ma na celu jedynie wyjaśnienie mniej więcej na czym polega zadanie - o wszelkie szczegóły lub jeśli czegoś nie jesteś pewny pytaj na `#general`. Jeśli opis nie jest  
  wystarczająco dobry, poproś o doszczegółowienie osoby wystawiającej task*